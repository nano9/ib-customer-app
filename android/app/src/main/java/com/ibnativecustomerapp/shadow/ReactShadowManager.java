package com.ibnativecustomerapp.shadow;

import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.annotations.ReactProp;

public class ReactShadowManager extends ViewGroupManager<ShadowView> {
    public static final String REACT_CLASS = "RTCShadowView";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected ShadowView createViewInstance(ThemedReactContext context) {
        return new ShadowView(context);
    }

    @ReactProp(name = "color", defaultInt = 0)
    public void setColor(ShadowView view, int color) {
        view.setShadowColor(color);
    }

    @ReactProp(name = "colorAlpha", defaultInt = 0)
    public void setColorAlpha(ShadowView view, int colorAlpha) {
        view.setColorAlpha(colorAlpha);
    }

    @ReactProp(name = "bgColor", defaultInt = 0)
    public void setBgColor(ShadowView view, int color) {
        view.setBgColor(color);
    }

    @ReactProp(name = "bgColorAlpha", defaultInt = 0)
    public void setBgColorAlpha(ShadowView view, int colorAlpha) {
        view.setBgColorAlpha(colorAlpha);
    }

    @ReactProp(name = "shadowRadius", defaultFloat = 0)
    public void setShadowRadius(ShadowView view, float radius) {
        view.setShadowRadius(radius);
    }

    @ReactProp(name = "cornerRadius", defaultFloat = 0)
    public void setCornerRadius(ShadowView view, float radius) {
        view.setCornerRadius(radius);
    }

    @ReactProp(name = "offsetX", defaultFloat = 0)
    public void setOffsetX(ShadowView view, float x) {
        view.setOffsetX(x);
    }

    @ReactProp(name = "offsetY", defaultFloat = 0)
    public void setOffsetY(ShadowView view, float y) {
        view.setOffsetY(y);
    }

    public boolean needsCustomLayoutForChildren() {
        return true;
    }
}