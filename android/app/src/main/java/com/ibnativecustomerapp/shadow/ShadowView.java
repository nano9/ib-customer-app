package com.ibnativecustomerapp.shadow;

import android.content.Context;
import android.graphics.*;
import android.support.v4.graphics.ColorUtils;
import android.util.TypedValue;
import android.widget.FrameLayout;

public class ShadowView extends FrameLayout {
    private int shadowColor, bgColor;
    private Paint paint = new Paint();
    private Bitmap bitmap;
    private Path path = new Path();
    private Canvas canvas = new Canvas();
    private boolean needsToPaint = false;
    private float offsetX, offsetY, shadowRadius, cornerRadius;

    public ShadowView(Context context) {
        super(context);
        paint.setStyle(Paint.Style.FILL);
        update();
    }

    @Override
    public void onSizeChanged(int w, int h, int ow, int oh) {
        super.onSizeChanged(w, h, ow, oh);
        update();
    }

    public void setShadowColor(int shadowColor) {
        this.shadowColor = (this.shadowColor & 0xFF000000) | shadowColor;
        update();
    }

    public void setColorAlpha(int colorAlpha) {
        shadowColor = ColorUtils.setAlphaComponent(shadowColor, colorAlpha);
        update();
    }

    public void setBgColor(int color) {
        this.bgColor = (this.bgColor & 0xFF000000) | color;
        update();
    }

    public void setBgColorAlpha(int colorAlpha) {
        bgColor = ColorUtils.setAlphaComponent(bgColor, colorAlpha);
        update();
    }

    public void setShadowRadius(float shadowRadius) {
        this.shadowRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, shadowRadius, this.getContext().getResources().getDisplayMetrics());
        update();
    }

    public void setCornerRadius(float cornerRadius) {
        this.cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, cornerRadius, this.getContext().getResources().getDisplayMetrics());
        update();
    }

    public void setOffsetX(float offsetX) {
        this.offsetX = offsetX;
    }

    public void setOffsetY(float offsetY) {
        this.offsetY = offsetY;
    }

    private void update() {
        if (this.getWidth() <= 0 || this.getHeight() <= 0) return;
        if (bitmap == null || bitmap.getWidth() != getWidth() || bitmap.getHeight() != getHeight()) {
            bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.ARGB_8888);
            canvas.setBitmap(bitmap);
            needsToPaint = true;
        }
        invalidate();
    }

    @Override
    protected void dispatchDraw(Canvas viewCanvas) {
        if (needsToPaint) {
            paint.setShadowLayer(shadowRadius, offsetX, offsetY, shadowColor);
            paint.setColor(bgColor);

            if (!path.isEmpty()) path.rewind();

            float innerWidth = getWidth() - (shadowRadius + cornerRadius) * 2;
            float innerHeight = getHeight() - (shadowRadius + cornerRadius) * 2;

            path.setLastPoint(cornerRadius + shadowRadius, shadowRadius);

            path.rLineTo(innerWidth, 0);
            path.rQuadTo(
                    cornerRadius, 0,
                    cornerRadius, cornerRadius
            );

            path.rLineTo(0, innerHeight);
            path.rQuadTo(
                    0, cornerRadius,
                    -cornerRadius, cornerRadius
            );

            path.rLineTo(-innerWidth, 0);
            path.rQuadTo(
                    -cornerRadius, 0,
                    -cornerRadius, -cornerRadius
            );

            path.rLineTo(0, -innerHeight);
            path.rQuadTo(
                    0, -cornerRadius,
                    cornerRadius, -cornerRadius
            );

            path.close();

            canvas.drawPath(path, paint);

            needsToPaint = false;
        }

        viewCanvas.drawBitmap(bitmap, 0, 0, null);
        super.dispatchDraw(viewCanvas);
    }
}