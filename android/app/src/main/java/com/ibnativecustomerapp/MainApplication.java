package com.ibnativecustomerapp;

import android.app.Application;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.views.image.ReactImageManager;
import com.facebook.soloader.SoLoader;
import com.horcrux.svg.SvgPackage;
import com.ibnativecustomerapp.nestedScrollView.ReactNestedScrollViewManager;
import com.ibnativecustomerapp.shadow.ReactShadowManager;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.asList(
                new MainReactPackage(),
                new SvgPackage(),
                new ReactPackage() {
                    @Override
                    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
                        return Arrays.<NativeModule>asList(
                        );
                    }

                    @Override
                    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
                        return Arrays.<ViewManager>asList(
                            new ReactShadowManager(),
                            new ReactNestedScrollViewManager()
                        );
                    }
                }
            );
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
