import { getInstanceFromNode } from 'react-native/Libraries/Renderer/shims/ReactNativeComponentTree';

export function getComponentFromNativeID(id) {
  return getInstanceFromNode(id);
}
