import React from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';

import ShadowView from 'components/ShadowView';

export const Image = styled.Image`
  height: ${({ height }) => height || 152}px;
  border-radius: ${({ borderRadius }) => borderRadius || 0}px;
`;

const ShadowContainer = styled(ShadowView)`
  width: ${({ width }) => width || '100%'};
  position: ${({ absolute }) => (absolute ? 'absolute' : 'relative')};
  top: ${({ top }) => (top !== null ? top : 'auto')};
  left: ${({ left }) => (left !== null ? left : 'auto')};
  right: ${({ right }) => (right !== null ? right : 'auto')};
  bottom: ${({ bottom }) => (bottom !== null ? bottom : 'auto')};
  padding: 0px;
  margin: 0px;
  z-index: 100;
`;

export default function ImageWithShadow(props) {
  const {
    uri,
    shadowRadius,
    borderRadius,
    color,
    colorAlpha,
    height,
    width,
    absolutePosition,
    left,
    right,
    top,
    bottom,
  } = props;
  return (
    <ShadowContainer
      shadowRadius={shadowRadius}
      cornerRadius={borderRadius}
      color={color}
      colorAlpha={colorAlpha}
      backgroundColor={0xFF0000}
      backgroundColorAlpha={0}
      absolute={absolutePosition}
      left={left}
      right={right}
      top={top}
      bottom={bottom}
      width={width}
    >
      {/* <View style={{ width: 100, height: 144, backgroundColor: 'white' }} /> */}
      <Image
        borderRadius={borderRadius}
        height={height}
        source={{ uri }}
      />
    </ShadowContainer>
  );
}

ImageWithShadow.propTypes = {
  uri: PropTypes.string.isRequired,
  shadowRadius: PropTypes.number,
  borderRadius: PropTypes.number,
  color: PropTypes.number,
  colorAlpha: PropTypes.number,
  absolutePosition: PropTypes.bool,
  height: PropTypes.number,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number, null]),
  left: PropTypes.oneOfType([PropTypes.number, null]),
  right: PropTypes.oneOfType([PropTypes.number, null]),
  top: PropTypes.oneOfType([PropTypes.number, null]),
  bottom: PropTypes.oneOfType([PropTypes.number, null]),
};

ImageWithShadow.defaultProps = {
  shadowRadius: 0,
  borderRadius: 0,
  color: 0x000000,
  colorAlpha: 0xff,
  absolutePosition: false,
  height: 152,
  width: '100%',
  top: null,
  left: null,
  right: null,
  bottom: null,
};

