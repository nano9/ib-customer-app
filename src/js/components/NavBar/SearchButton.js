import React, { PureComponent } from 'react';
import { TouchableWithoutFeedback, View } from 'react-native';
import Svg, {
  Path,
  Defs,
  LinearGradient,
  Stop,
  RadialGradient,
  Circle,
} from 'react-native-svg';
import { Link } from 'react-router-native';

// eslint-disable-next-line react/prefer-stateless-function
export default class SidebarButton extends PureComponent {
  render() {
    return (
      <Link to='/search' component={TouchableWithoutFeedback}>
        <View>
          <Svg width="80" height="80" viewBox="-10 -14 80 84">
            <Defs>
              <RadialGradient id="grad" cx="50%" cy="50%" rx="50%" ry="50%" fx="50%" fy="50%">
                <Stop
                  offset="0%"
                  stopColor="#000"
                  stopOpacity={0.6}
                />
                <Stop
                  offset="100%"
                  stopColor="#000"
                  stopOpacity={0}
                />
              </RadialGradient>
              <LinearGradient id="paint0_linear" x1="0%" y1="0%" x2="100%" y2="0%">
                <Stop offset="0%" stopColor="#006DF0" />
                <Stop offset="100%" stopColor="#328DFA" />
              </LinearGradient>
            </Defs>

            <Circle
              cx="30"
              cy="30"
              r="40"
              fill="url(#grad)"
            />

            <Path
              fill="url(#paint0_linear)"
              fillRule="evenodd"
              d="M 30 60C 46.5685 60 60 46.5685 60 30C 60 13.4315 46.5685 0 30 0C 13.4315 0 0 13.4315 0 30C 0 46.5685 13.4315 60 30 60Z"
            />
            <Path
              fill="#FFFFFF"
              d="M 30 62C 47.6731 62 62 47.6731 62 30L 58 30C 58 45.464 45.464 58 30 58L 30 62ZM 62 30C 62 12.3269 47.6731 -2 30 -2L 30 2C 45.464 2 58 14.536 58 30L 62 30ZM 30 -2C 12.3269 -2 -2 12.3269 -2 30L 2 30C 2 14.536 14.536 2 30 2L 30 -2ZM -2 30C -2 47.6731 12.3269 62 30 62L 30 58C 14.536 58 2 45.464 2 30L -2 30Z"
            />
            <Path
              fill="#FFFFFF"
              transform="translate(19, 19)"
              d="M 21.8097 20.878L 16.4622 15.5317C 17.8967 13.8829 18.7651 11.7317 18.7651 9.38049C 18.7651 4.20488 14.5544 0 9.38257 0C 4.20581 0 3.63524e-11 4.20976 3.63524e-11 9.38049C 3.63524e-11 14.5512 4.21069 18.761 9.38257 18.761C 11.7343 18.761 13.886 17.8927 15.5352 16.4585L 20.8827 21.8049C 21.0095 21.9317 21.1803 22 21.3462 22C 21.5121 22 21.6829 21.9366 21.8097 21.8049C 22.0634 21.5512 22.0634 21.1317 21.8097 20.878ZM 1.31249 9.38049C 1.31249 4.93171 4.9328 1.31707 9.37769 1.31707C 13.8275 1.31707 17.4429 4.93659 17.4429 9.38049C 17.4429 13.8244 13.8275 17.4488 9.37769 17.4488C 4.9328 17.4488 1.31249 13.8293 1.31249 9.38049Z"
            />
          </Svg>
        </View>
      </Link>
    );
  }
}
