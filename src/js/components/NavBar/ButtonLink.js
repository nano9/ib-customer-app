import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Link } from 'react-router-native';

import HomeButton from './HomeButton';
import TicketButton from './TicketButton';
import FavoriteButton from './FavoriteButton';

const ButtonLinkWrapper = styled(Link)`
  align-items: center;
  justify-content: center;
  flex: 1;
  height: 54px;
`;

export const ButtonContainer = styled.View`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const ButtonUnderline = styled.View`
  width: 78%;
  height: 3px;
  background: #197DF5;
  position: absolute;
  bottom: 0px;
`;

export const Notices = styled.View`
  width: 17px;
  height: 17px;
  border-radius: 8px;
  background: #006DF0;
  position: absolute;
  right: 0px;
  top: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const NoticeLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  color: #F2F2F2;
`;

export const IconContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 33px;
`;

export default class ButtonLink extends PureComponent {
  static propTypes = {
    notices: PropTypes.number,
    isActive: PropTypes.bool.isRequired,
    to: PropTypes.string.isRequired,
    button: PropTypes.oneOfType([HomeButton, TicketButton, FavoriteButton]).isRequired,
    onPress: PropTypes.func,
  };

  static defaultProps = {
    notices: 0,
    onPress: undefined,
  };

  onPress = event => this.props.onPress(this.props, event);

  render() {
    const { to, button: Button, isActive, notices } = this.props;
    return (
      <ButtonLinkWrapper underlayColor="rgba(0,0,0,0.07)" to={to} onPress={this.onPress}>
        <ButtonContainer>
          <IconContainer>
            <Button isActive={isActive} />
            {notices > 0 && !isActive &&
            <Notices>
              <NoticeLabel>{notices}</NoticeLabel>
            </Notices>
            }
          </IconContainer>
          {isActive && <ButtonUnderline />}
        </ButtonContainer>
      </ButtonLinkWrapper>
    );
  }
}