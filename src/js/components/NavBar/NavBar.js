import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { withRouter } from 'react-router-native';

import ViewWithShadow from 'components/ViewWithShadow';
import ButtonLink, { ButtonContainer, IconContainer, NoticeLabel, Notices, ButtonUnderline } from './ButtonLink';
import SearchButtonLink from './SearchButtonLink';
import HomeButton from './HomeButton';
import TicketButton from './TicketButton';
import FavoriteButton from './FavoriteButton';
import SidebarButton from './SidebarButton';

const MenuContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  background-color: transparent;
  height: 75px;
  overflow: visible;
`;

const ShadowContent = styled.View`
  height: 54px;
  width: 100%;
  background-color: #fff;
`;

// TODO should reuse ButtonLink?
export const SidebarToggle = styled.TouchableHighlight`
  display: flex;
  height: 54px;
  align-items: center;
  justify-content: center;
  flex: 1;
`;

@withRouter
export default class NavBar extends PureComponent {
  static propTypes = {
    setSideBarState: PropTypes.func.isRequired,
    ticketsNotices: PropTypes.number,
    favoritesNotices: PropTypes.number,
    accountNotices: PropTypes.number,
    isSidebarOpen: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    ticketsNotices: 0,
    favoritesNotices: 0,
    accountNotices: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      activeButton: 0,
    };
  }

  toggleSideBar = () => {
    this.props.setSideBarState(!this.props.isSidebarOpen);
  };

  buttonPress = ({ to }) => this.setState({ activeButton: to });

  render() {
    const {
      ticketsNotices,
      favoritesNotices,
      accountNotices,
      isSidebarOpen,
    } = this.props;
    const { activeButton } = this.state;

    return (
      <MenuContainer>
        <ViewWithShadow
          shadowRadius={7}
          borderRadius={0}
          color={0x000000}
          colorAlpha={70}
          bgColor={0xffffff}
          bgColorAlpha={255}
          style={{
            position: 'absolute',
            left: -9,
            right: -9,
            bottom: -9,
          }}
        >
          <ShadowContent />
        </ViewWithShadow>

        <ButtonLink
          to="/main"
          onPress={this.buttonPress}
          isActive={activeButton === '/main'}
          button={HomeButton}
        />

        <ButtonLink
          to="/pc/tickets"
          onPress={this.buttonPress}
          isActive={activeButton === '/pc/tickets'}
          button={TicketButton}
          notices={ticketsNotices}
        />

        <SearchButtonLink
          to="/search"
          onPress={this.buttonPress}
          isActive={activeButton === '/search'}
        />

        <ButtonLink
          to="/pc/favorites"
          onPress={this.buttonPress}
          isActive={activeButton === '/pc/favorites'}
          button={FavoriteButton}
          notices={favoritesNotices}
        />

        <SidebarToggle underlayColor="rgba(0,0,0,0.07)" onPress={this.toggleSideBar}>
          <ButtonContainer>
            <IconContainer>
              <SidebarButton isActive={isSidebarOpen} />
              {accountNotices > 0 && !isSidebarOpen &&
                <Notices>
                  <NoticeLabel>{accountNotices}</NoticeLabel>
                </Notices>
              }
            </IconContainer>
            {isSidebarOpen && <ButtonUnderline />}
          </ButtonContainer>
        </SidebarToggle>
      </MenuContainer>
    );
  }
}
