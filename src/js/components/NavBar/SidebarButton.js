import React from 'react';
import Svg, {
  Path, LinearGradient, Stop, Defs,
} from 'react-native-svg';
import PropTypes from 'prop-types';

export default function SidebarButton({ isActive }) {
  const pathProps = isActive
    ? {
      fill: 'transparent',
      stroke: 'url(#active)',
    } : {
      fillOpacity: 0.4,
    };

  return (
    <Svg width="16" height="13" viewBox="-1 -1 17 12">
      <Defs>
        <LinearGradient id="active" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse">
          <Stop offset="0%" stopColor="#006DF0" />
          <Stop offset="100%" stopColor="#328DFA" />
        </LinearGradient>
      </Defs>
      <Path {...pathProps} d="M 0 5L 16 5L 16 6L 0 6L 0 5Z" />
      <Path {...pathProps} d="M 0 10L 16 10L 16 11L 0 11L 0 10Z" />
      <Path {...pathProps} d="M 0 0L 16 0L 16 1L 0 1L 0 0Z" />
      <Path {...pathProps} d="M 0 5L 0 4.7L -0.3 4.7L -0.3 5L 0 5ZM 16 5L 16.3 5L 16.3 4.7L 16 4.7L 16 5ZM 16 6L 16 6.3L 16.3 6.3L 16.3 6L 16 6ZM 0 6L -0.3 6L -0.3 6.3L 0 6.3L 0 6ZM 0 10L 0 9.7L -0.3 9.7L -0.3 10L 0 10ZM 16 10L 16.3 10L 16.3 9.7L 16 9.7L 16 10ZM 16 11L 16 11.3L 16.3 11.3L 16.3 11L 16 11ZM 0 11L -0.3 11L -0.3 11.3L 0 11.3L 0 11ZM 0 0L 0 -0.3L -0.3 -0.3L -0.3 0L 0 0ZM 16 0L 16.3 0L 16.3 -0.3L 16 -0.3L 16 0ZM 16 1L 16 1.3L 16.3 1.3L 16.3 1L 16 1ZM 0 1L -0.3 1L -0.3 1.3L 0 1.3L 0 1ZM 0 5.3L 16 5.3L 16 4.7L 0 4.7L 0 5.3ZM 15.7 5L 15.7 6L 16.3 6L 16.3 5L 15.7 5ZM 16 5.7L 0 5.7L 0 6.3L 16 6.3L 16 5.7ZM 0.3 6L 0.3 5L -0.3 5L -0.3 6L 0.3 6ZM 0 10.3L 16 10.3L 16 9.7L 0 9.7L 0 10.3ZM 15.7 10L 15.7 11L 16.3 11L 16.3 10L 15.7 10ZM 16 10.7L 0 10.7L 0 11.3L 16 11.3L 16 10.7ZM 0.3 11L 0.3 10L -0.3 10L -0.3 11L 0.3 11ZM 0 0.3L 16 0.3L 16 -0.3L 0 -0.3L 0 0.3ZM 15.7 0L 15.7 1L 16.3 1L 16.3 0L 15.7 0ZM 16 0.7L 0 0.7L 0 1.3L 16 1.3L 16 0.7ZM 0.3 1L 0.3 0L -0.3 0L -0.3 1L 0.3 1Z" />
    </Svg>
  );
}

SidebarButton.propTypes = {
  isActive: PropTypes.bool,
};

SidebarButton.defaultProps = {
  isActive: false,
};
