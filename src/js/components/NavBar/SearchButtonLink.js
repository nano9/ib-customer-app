import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';
import styled from 'styled-components/native';
import { Link } from 'react-router-native';
import Svg, {
  Path, LinearGradient, Stop, Defs, Circle,
} from 'react-native-svg';
import ViewWithShadow from 'components/ViewWithShadow';

const ButtonLinkWrapper = styled(Link)`
  align-items: center;
  justify-content: center;
  flex: 1;
  height: 80px;
`;

export const ButtonContainer = styled.View`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  opacity: 1;
`;

export const ButtonUnderline = styled.View`
  width: 78%;
  height: 3px;
  background: #197DF5;
  position: absolute;
  bottom: 0px;
`;

export const Notices = styled.View`
  width: 17px;
  height: 17px;
  border-radius: 8px;
  background: #006DF0;
  position: absolute;
  right: 0px;
  top: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const NoticeLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  color: #F2F2F2;
`;

export const IconContainer = styled.View`
  position: absolute;
  top: 34;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 33px;
`;

function SearchIcon({ isActive }) {
  const pathProps = isActive
    ? {
      fill: 'url(#active)',
      stroke: 'url(#active)',
    } : {
      fill: '#FFFFFF',
    };

  return (
    <Svg width="22" height="22" viewBox="-1 -1 23 23">
      <Defs>
        <LinearGradient id="active" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse">
          <Stop offset="0%" stopColor="#006DF0" />
          <Stop offset="100%" stopColor="#328DFA" />
        </LinearGradient>
      </Defs>
      <Path {...pathProps} d="M 21.8097 20.878L 16.4622 15.5317C 17.8967 13.8829 18.7651 11.7317 18.7651 9.38049C 18.7651 4.20488 14.5544 0 9.38257 0C 4.20581 0 3.63524e-11 4.20976 3.63524e-11 9.38049C 3.63524e-11 14.5512 4.21069 18.761 9.38257 18.761C 11.7343 18.761 13.886 17.8927 15.5352 16.4585L 20.8827 21.8049C 21.0095 21.9317 21.1803 22 21.3462 22C 21.5121 22 21.6829 21.9366 21.8097 21.8049C 22.0634 21.5512 22.0634 21.1317 21.8097 20.878ZM 1.31249 9.38049C 1.31249 4.93171 4.9328 1.31707 9.37769 1.31707C 13.8275 1.31707 17.4429 4.93659 17.4429 9.38049C 17.4429 13.8244 13.8275 17.4488 9.37769 17.4488C 4.9328 17.4488 1.31249 13.8293 1.31249 9.38049Z" />
    </Svg>
  );
}

function InactiveButtonBg() {
  return (
    <Svg width="56" height="56" viewBox="-2 -2 63 63">
      <Defs>
        <LinearGradient id="search_btn_bg" x1="0%" y1="0%" x2="100%" y2="0%">
          <Stop offset="0%" stopColor="#006DF0" />
          <Stop offset="100%" stopColor="#328DFA" />
        </LinearGradient>
      </Defs>
      <Circle
        cx="30"
        cy="30"
        r="30"
        fill="url(#search_btn_bg)"
        fillOpacity={1}
        stroke="#fff"
        strokeWidth={2}
      />
    </Svg>
  );
}

export default class ButtonLink extends PureComponent {
  static propTypes = {
    isActive: PropTypes.bool.isRequired,
    to: PropTypes.string.isRequired,
    onPress: PropTypes.func,
  };

  static defaultProps = {
    notices: 0,
    onPress: undefined,
  };

  state = {
    iconOffsetY: new Animated.Value(this.props.isActive ? 0 : -9),
    iconBgScaleAndOpacity: new Animated.Value(this.props.isActive ? 0 : 1),
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.isActive !== this.props.isActive) {
      // RUN ANIMATON
      const ANIMATION_DURATION = 150;
      const { iconOffsetY, iconBgScaleAndOpacity } = this.state;

      iconOffsetY.setValue(this.props.isActive ? 0 : -9);
      iconBgScaleAndOpacity.setValue(this.props.isActive ? 0 : 1);

      Animated.parallel([
        Animated.spring(
          iconOffsetY,
          {
            toValue: this.props.isActive ? -9 : 0,
            duration: ANIMATION_DURATION,
            useNativeDriver: true,
          },
        ),
        Animated.spring(
          iconBgScaleAndOpacity,
          {
            toValue: this.props.isActive ? 1 : 0,
            duration: ANIMATION_DURATION,
            useNativeDriver: true,
          },
        ),
      ]).start();
    }
  }

  onPress = event => this.props.onPress(this.props, event);

  render() {
    const { to, isActive } = this.props;
    return (
      <ButtonLinkWrapper underlayColor="rgba(0,0,0,0.07)" to={to} onPress={this.onPress}>
        <ButtonContainer>
          <Animated.View style={{
              width: '100%',
              height: 70,
              position: 'absolute',
              top: 11,
              display: 'flex',
              justifyContent: 'flex-start',
              alignItems: 'center',
              transform: [
                { scaleY: this.state.iconBgScaleAndOpacity },
                { scaleX: this.state.iconBgScaleAndOpacity },
              ],
              opacity: this.state.iconBgScaleAndOpacity,
            }}
          >
            <ViewWithShadow
              shadowRadius={4}
              borderRadius={30}
              color={0x000000}
              colorAlpha={50}
              bgColor={0xffffff}
              bgColorAlpha={0xff}
            >
              <InactiveButtonBg />
            </ViewWithShadow>
          </Animated.View>
          <Animated.View style={{
              position: 'absolute',
              top: 34,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              width: 40,
              height: 33,
              transform: [
                { translateY: this.state.iconOffsetY },
              ],
            }}
          >
            <SearchIcon isActive={this.props.isActive} />
          </Animated.View>
          {isActive && <ButtonUnderline />}
        </ButtonContainer>
      </ButtonLinkWrapper>
    );
  }
}
