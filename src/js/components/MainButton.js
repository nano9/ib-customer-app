import React, { Component } from 'react';
import styled from 'styled-components/native';
import { TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types';
import Svg, {
  Path,
  Defs,
  LinearGradient,
  Stop,
} from 'react-native-svg';

const Touchable = styled(TouchableHighlight)`
  flex: 1;
  z-index: 902;
`;

const ButtonWrapper = styled.View`
  flex: 1;
`;

const ButtonContainer = styled.View`
  flex: 1;
  width: 100%;
  height: 49px;
  display: flex;
  border-radius: 3px;
`;

const ButtonLabelContainer = styled.View`
  flex: 1;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const ButtonLabel = styled.Text`
  flex: 1;
  height: 24px;
  width: 100%;
  text-align: center;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 24px;
  font-size: 20px;
  color: #4F4F4F;
`;

const ArrowIcon = styled.View`
  width: 12px;
  height: 100%;
  position: absolute;
  right: 61px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  transform: rotate(${props => (props.arrowStyle !== 'down') && '-'}90deg);
`;

function Button({ arrowStyle, children, disabled }) {
  const opacity = disabled ? 0.4 : 1;
  return (
    <ButtonContainer>
      <Svg width="100%" height="49" viewBox="-1 0 361 49" preserveAspectRatio="none">
        <Defs>
          <LinearGradient id="bg" x1="0%" y1="0%" x2="100%" y2="0%">
            <Stop offset="0%" stopColor="#E7A90A" />
            <Stop offset="100%" stopColor="#FAE101" />
          </LinearGradient>
          <LinearGradient id="stroke" x1="0%" y1="0%" x2="100%" y2="0%">
            <Stop offset="0%" stopColor="#DEB923" />
            <Stop offset="100%" stopColor="#E7D31F" />
          </LinearGradient>
        </Defs>
        <Path fill="url(#bg)" fillOpacity={opacity} d="M 0 3C 0 1.34314 1.34315 0 3 0L 356 0C 357.657 0 359 1.34315 359 3L 359 46C 359 47.6569 357.657 49 356 49L 3.00001 49C 1.34315 49 0 47.6569 0 46L 0 3Z" />
        <Path fill="url(#stroke)" fillOpacity={opacity} d="M 3 1L 356 1L 356 -1L 3 -1L 3 1ZM 358 3L 358 46L 360 46L 360 3L 358 3ZM 356 48L 3.00001 48L 3.00001 50L 356 50L 356 48ZM 1 46L 1 3L -1 3L -1 46L 1 46ZM 3.00001 48C 1.89544 48 1 47.1046 1 46L -1 46C -1 48.2091 0.790874 50 3.00001 50L 3.00001 48ZM 358 46C 358 47.1046 357.105 48 356 48L 356 50C 358.209 50 360 48.2091 360 46L 358 46ZM 356 1C 357.105 1 358 1.89543 358 3L 360 3C 360 0.790857 358.209 -1 356 -1L 356 1ZM 3 -1C 0.790862 -1 -1 0.790859 -1 3L 1 3C 1 1.89543 1.89543 1 3 1L 3 -1Z" />
      </Svg>
      {children}
      {arrowStyle &&
        <ArrowIcon arrowStyle={arrowStyle}>
          <Svg width="12" height="20" viewBox="0 0 12 20">
            <Path fill="#4F4F4F" d="M 0 9.70448L 1.61663 8.19027L 12 18.5522L 10.3605 20L 0 9.70448Z" />
            <Path fill="#4F4F4F" d="M 12 1.53567L 10.3605 0L 0 9.70448L 1.74877 11.3425L 12 1.53567Z" />
          </Svg>
        </ArrowIcon>
      }
    </ButtonContainer>
  );
}

Button.propTypes = {
  arrowStyle: PropTypes.string,
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  arrowStyle: 'down',
  disabled: false,
};

export default class MainButton extends Component {
  constructor(props) {
    super(props);
    this.buttonRef = null;
  }

  onPress = () => {
    if (this.props.onPressGetRef) {
      this.props.onPressGetRef(this.buttonRef);
    }
  };

  render() {
    return (
      <Touchable onPress={this.onPress} underlayColor="rgba(0,0,0,0.2)" disabled={this.props.disabled}>
        <ButtonWrapper innerRef={(comp) => { this.buttonRef = comp; }}>
          <Button arrowStyle={this.props.arrowStyle} disabled={this.props.disabled}>
            <ButtonLabelContainer>
              <ButtonLabel>{this.props.label}</ButtonLabel>
            </ButtonLabelContainer>
          </Button>
        </ButtonWrapper>
      </Touchable>
    );
  }
}

MainButton.propTypes = {
  label: PropTypes.string.isRequired,
  arrowStyle: PropTypes.string,
  onPressGetRef: PropTypes.func,
  disabled: PropTypes.bool,
};

MainButton.defaultProps = {
  arrowStyle: null,
  onPressGetRef: null,
  disabled: false,
};
