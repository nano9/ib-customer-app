import React from 'react';
import PropTypes from 'prop-types';
import { Platform, Image } from 'react-native';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import SideShadowSource from 'components/SideShadowSource';

import MarkerIcon from './Icons/MarkerIcon';
import PlaceIcon from './Icons/PlaceIcon';
import DateIcon from './Icons/DateIcon';

const Wrapper = styled.View`
  background-color: #fff;
  width: 100%;
  padding: 20px;
  padding-top: 10px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
`;

const ImageContainer = styled(ViewWithShadow)`
  max-width: 116px;
  width: 100%;
  ${Platform.OS === 'ios' ? `
  box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.3137); 
  margin: 3px; 
  border-radius: 3px;
  background-color: transparent;
  ` : ''}
`;

const InfoContainer = styled.View`
  flex: 1;
  margin-left: 11px;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #006DF0;
`;

const RegularLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #4F4F4F;
  margin-left: 10px;
`;

const Row = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  margin-top: 5px;
`;

export default function TicketInfo({ image, title, city, place, date }) {
  return [
    <Wrapper key="info">
      <ImageContainer
        shadowRadius={3}
        borderRadius={3}
        color={0x000000}
        colorAlpha={80}
        bgColor={0xffffff}
        bgColorAlpha={0xff}
      >
        <Image
          source={{ uri: image.size1 }}
          style={{ height: 160, borderRadius: 3 }}
        />
      </ImageContainer>
      <InfoContainer>
        <TitleLabel>{title}</TitleLabel>
        <Row>
          <MarkerIcon />
          <RegularLabel style={{ marginLeft: 12 }}>{city}</RegularLabel>
        </Row>
        <Row>
          <PlaceIcon />
          <RegularLabel>{place}</RegularLabel>
        </Row>
        <Row>
          <DateIcon />
          <RegularLabel>{date}</RegularLabel>
        </Row>
      </InfoContainer>
    </Wrapper>,
    <SideShadowSource key="info-sh" goes="from-top" bgColor="#fff" />,
  ];
}

TicketInfo.propTypes = {
  image: PropTypes.shape({
    size1: PropTypes.string.isRequired,
    size2: PropTypes.string.isRequired,
  }).isRequired,
  title: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  place: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
};
