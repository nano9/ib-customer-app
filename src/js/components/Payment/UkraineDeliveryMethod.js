import React, { PureComponent } from 'react';
import { Platform } from 'react-native';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import {
  InputError,
  InputLabel,
  StyledTextInput,
  InputContainer,
} from 'components/FormComponents';

import {
  Wrapper,
  ShadowContainer,
  Container,
  TitleLabel,
  MethodContainer,
  MethodLabelContainer,
  FooterRegularLabel,
  FooterRegularLabelB,
  FooterRedLabel,
  FooterBlueLabel,
  FooterBlueLabelB,
} from './CityDeliveryMethod';

import NovaPoshtaIcon from './Icons/NovaPoshtaIcon';

const InputInfoContainer = styled.View`
  padding-left: 20px;
  padding-right: 20px;
  margin-bottom: 22px;
`;

const LinkLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  text-align: center;
  text-decoration-line: underline;
  color: #006DF0;
`;

export default class UkraineDeliveryMethod extends PureComponent {
  state = {
  };

  render() {
    const amount = 100;
    const totalCost = 400;
    return (
      <Wrapper>
        <InputInfoContainer>
          <InputLabel style={{ textAlign: 'left', marginLeft: 0 }}>
            Информация о доставке
          </InputLabel>
          <InputContainer style={{ marginTop: 6, marginBottom: 0 }}>
            <StyledTextInput placeholder="Область и регион" />
          </InputContainer>
          <InputContainer style={{ marginTop: 11, marginBottom: 0 }}>
            <StyledTextInput placeholder="№ Склада" />
          </InputContainer>
        </InputInfoContainer>

        <ShadowContainer
          shadowRadius={7}
          borderRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
          style={{ marginBottom: 4 }}
        >
          <Container>
            <LinkLabel>
              “Доставка осуществляется наложенным платежом по тарифам и правилам почтового сервиса «Новая Почта».”
            </LinkLabel>
          </Container>
        </ShadowContainer>

        <ShadowContainer
          shadowRadius={7}
          borderRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
        >
          <Container>
            <TitleLabel numberOfLines={1}>Cпособ оплаты:</TitleLabel>
            <ShadowContainer
              key="other-card-shadow"
              shadowRadius={7}
              borderRadius={3}
              color={0x000000}
              colorAlpha={50}
              bgColor={0xffffff}
              bgColorAlpha={0xff}
            >
              <MethodContainer>
                <NovaPoshtaIcon />
                <MethodLabelContainer>Новая Почта</MethodLabelContainer>
              </MethodContainer>
            </ShadowContainer>

            <FooterRegularLabel>
              Всего билетов: <FooterRegularLabelB> {amount} шт.</FooterRegularLabelB>
            </FooterRegularLabel>
            <FooterBlueLabel>
              Итого к оплате: <FooterBlueLabelB> {totalCost} грн</FooterBlueLabelB>
            </FooterBlueLabel>
          </Container>
        </ShadowContainer>
      </Wrapper>
    );
  }
}

UkraineDeliveryMethod.propTypes = {

};
