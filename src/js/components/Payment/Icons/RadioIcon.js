import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.View`
  width: 16px;
  height: 16px;
  border: 1px solid #C1C1C1;
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Circle = styled.View`
  width: 10px;
  height: 10px;
  border-radius: 5px;
  background-color: ${({ active }) => (active ? '#197DF5' : '#EDEDED')};
`;

export default function RadioIcon({ active }) {
  return (
    <Wrapper>
      <Circle active={active} />
    </Wrapper>
  );
}

RadioIcon.propTypes = {
  active: PropTypes.bool,
};

RadioIcon.defaultProps = {
  active: false,
};
