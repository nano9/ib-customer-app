import React from 'react';
import { TouchableOpacity, Platform, View } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import {
  InputLabel,
  StyledTextInput,
  InputContainer,
} from 'components/FormComponents';

import ButtonMain from 'components/ButtonMain';
import EIcon from 'components/Icons/EIcon';

import ViewWithShadow from 'components/ViewWithShadow';
import CheckBoxIcon from 'components/Icons/CheckBoxIcon';

const Wrapper = styled.View`
  width: 100%;
  padding-left: 13px;
  padding-right: 13px;
  margin-bottom: 13px;
  display: flex;
  align-items: center;
`;

const ShadowContainer = styled(ViewWithShadow)`
  width: 100%;
  ${Platform.OS === 'ios' ? `
  box-shadow: 0px 0px 7px rgba(0, 0, 0, 0.3137); 
  margin: 7px; 
  border-radius: 3px;
  background-color: transparent;
  ` : ''}
`;

const ButtonShadowContainer = styled(ViewWithShadow)`
  width: 100%;
  ${Platform.OS === 'ios' ? `
  box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.3137); 
  margin: 3px; 
  border-radius: 26px;
  background-color: transparent;
  ` : ''}
`;

const Container = styled.View`
  background-color: #fff;
  border-radius: 3px;
  padding: 15px 13px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const MidContainer = styled.View`
  flex: 1;
  flex-direction: column;
  padding-left: 18px;
`;

const RegularLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  text-align: center;
  color: #767676;
`;

const ButtonRow = styled.View`
  width: 100%;
  height: 49px;
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
  align-items: center;
`;

const CommentContainer = styled.View`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  padding-left: 7px;
  padding-right: 7px;
  padding-bottom: 0px;
  margin-top: 13px;
  margin-bottom: 13px;
`;

export default function Footer({ rulesAccepted, onToggleRulesCheckbox }) {
  return [
    <Wrapper key="footer-rules">
      <CommentContainer>
        <InputLabel>Доп. информация по заказу</InputLabel>
        <InputContainer style={{ marginTop: 6, marginBottom: 0 }}>
          <StyledTextInput placeholder="Комментарий" />
        </InputContainer>
      </CommentContainer>
      <ShadowContainer
        shadowRadius={7}
        borderRadius={3}
        color={0x000000}
        colorAlpha={80}
        bgColor={0xffffff}
        bgColorAlpha={0xff}
      >
        <Container>
          <TouchableOpacity onPress={onToggleRulesCheckbox} activeOpacity={0.6}>
            <CheckBoxIcon checked={rulesAccepted} />
          </TouchableOpacity>
          <MidContainer>
            <RegularLabel>Я подтверждаю что прочёл и согласен с условиями</RegularLabel>
            <TouchableOpacity onPress={onToggleRulesCheckbox} activeOpacity={0.6}>
              <RegularLabel style={{ color: '#006DF0' }}>Публичного договора-оферты</RegularLabel>
            </TouchableOpacity>
          </MidContainer>
        </Container>
      </ShadowContainer>
    </Wrapper>,
    <View
      style={{ width: '100%', maxWidth: 270 }}
      key="footer-btn"
    >
      <ButtonShadowContainer
        shadowRadius={3}
        borderRadius={26}
        color={0x000000}
        colorAlpha={80}
        bgColor={0xffffff}
        bgColorAlpha={0xff}
      >
        <ButtonRow>
          <ButtonMain
            label="Оформить билет"
            icon={<EIcon />}
            fontScale="big"
            height={49}
            borderRadius={26}
            onPressGetRef={this.onRequestClose}
          />
        </ButtonRow>
      </ButtonShadowContainer>
    </View>,
  ];
}

Footer.propTypes = {
  rulesAccepted: PropTypes.bool.isRequired,
  onToggleRulesCheckbox: PropTypes.func.isRequired,
};
