import React, { PureComponent } from 'react';
import { TouchableHighlight, TouchableOpacity, Platform, ActivityIndicator } from 'react-native';
import styled from 'styled-components/native';
import Collapsible from 'react-native-collapsible';

import SideShadowSource from 'components/SideShadowSource';
import ViewWithShadow from 'components/ViewWithShadow';
import ArrowIcon from 'components/Icons/ArrowIcon';
import {
  CardItem,
  MasterCardImage,
  VisaImage,
  CardTextContainer,
  CardText,
} from 'components/PersonalCabinet/BankCard';

import visaImg from 'img/visa.png';
import mcImg from 'img/mastercard.png';

import RadioIcon from './Icons/RadioIcon';

const Wrapper = styled.View`
  background-color: #fff;
  width: 100%;
  padding: 3px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

const ShadowContainer = styled(ViewWithShadow)`
  width: 100%;
  ${Platform.OS === 'ios' ? `
  box-shadow: 0px 0px 7px rgba(0, 0, 0, 0.3137); 
  margin: 7px; 
  border-radius: 3px;
  background-color: transparent;
  ` : ''}
`;

const Container = styled.View`
  background-color: #fff;
  border-radius: 3px;
  padding: 15px 13px 20px 13px;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #4F4F4F;
`;

const RadioTitleContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  margin-top: 20px;
  margin-bottom: 10px;
`;

const SecondTitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #4F4F4F;
  margin-left: 20px;
`;

const CardItemContainer = CardItem.extend`
  border: 1px solid #C1C1C1;
  border-radius: 3px;
`;

const CardDropdownContainer = styled.View`
  width: 50px;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const OtherCardsContainer = styled.View`
  width: 100%;
  background-color: #fff;
  border-radius: 3px;
`;

const OtherCardsHeaderContainer = styled.View`
  width: 100%;
  height: 67px;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding-left: 20px;
  padding-right: 20px;
`;

const OtherCardsLabel = styled.Text`
  flex: 1;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  text-align: center;
  color: #767676;
`;

const OtherCardsPlatonFrameContainer = styled.View`
  height: 480px;
  margin-left: 15px;
  margin-right: 15px
  border: 1px solid grey;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 20px;
`;

const FooterRegularLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-size: 18px;
  color: #767676;
  margin-top: 10px;
  margin-left: 7px;
  margin-right: 7px;
`;

const FooterRegularLabelB = FooterRegularLabel.extend`
  font-weight: 500;
  color: #292929;
`;

const FooterRedLabel = FooterRegularLabel.extend`
  color: #EB5757;
`;

const FooterBlueLabel = FooterRegularLabel.extend`
  color: #006DF0;
`;

const FooterBlueLabelB = FooterBlueLabel.extend`
  font-weight: 500;
`;

const CommentContainer = styled.View`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: 0px;
  margin-top: 20px;
  margin-bottom: 13px;
`;

const PAYMENT_METHOD_SAVED_CARD = 1;
const PAYMENT_METHOD_OTHER_CARD = 2;

export default class ETicketMethod extends PureComponent {
  state = {
    otherCardCollapsed: false,
    paymentMethod: PAYMENT_METHOD_OTHER_CARD,
  };

  setPaymentMethod = (paymentMethod) => {
    if (this.state.paymentMethod !== paymentMethod) {
      if (this.state.paymentMethod === PAYMENT_METHOD_OTHER_CARD) {
        this.setState({ paymentMethod, otherCardCollapsed: true });
      } else if (this.state.paymentMethod === PAYMENT_METHOD_SAVED_CARD) {
        this.setState({ paymentMethod, otherCardCollapsed: false });
      } else {
        this.setState({ paymentMethod });
      }
    }
  }

  toggleOtherCardContent = () =>
    this.setState({ otherCardCollapsed: !this.state.otherCardCollapsed });

  renderSavedCardsMethod = () => {
    const cards = [
      { id: '1', type: 'visa', cardLastNumber: '1234' },
      { id: '2', type: 'mc', cardLastNumber: '1234' },
    ];
    if (cards.length > 0) {
      const { type, cardLastNumber } = cards[0];
      return [
        <TouchableHighlight
          onPress={() => this.setPaymentMethod(PAYMENT_METHOD_SAVED_CARD)}
          underlayColor="rgba(0,0,0,0.07)"
          key="card-header"
        >
          <RadioTitleContainer>
            <RadioIcon active={this.state.paymentMethod === PAYMENT_METHOD_SAVED_CARD} />
            <SecondTitleLabel>Прикрепленные карты:</SecondTitleLabel>
          </RadioTitleContainer>
        </TouchableHighlight>,
        <TouchableHighlight
          onPress={() => this.setPaymentMethod(PAYMENT_METHOD_SAVED_CARD)}
          style={{ borderRadius: 3, marginTop: 6 }}
          underlayColor="rgba(0,0,0,0.07)"
          key="card-body"
        >
          <CardItemContainer>
            { type === 'visa' && <VisaImage source={visaImg} /> }
            { type === 'mc' && <MasterCardImage source={mcImg} /> }
            <CardTextContainer>
              <CardText numberOfLines={1}>**** **** **** {cardLastNumber}</CardText>
            </CardTextContainer>
            <CardDropdownContainer>
              <ArrowIcon />
            </CardDropdownContainer>
          </CardItemContainer>
        </TouchableHighlight>,
      ];
    }
    return null;
  }

  renderOtherCardMethod = () => [
    <TouchableHighlight
      onPress={() => this.setPaymentMethod(PAYMENT_METHOD_OTHER_CARD)}
      style={{ borderTopLeftRadius: 3, borderTopRightRadius: 3 }}
      underlayColor="rgba(0,0,0,0.07)"
      key="other-card-header"
    >
      <RadioTitleContainer style={{ marginTop: 29 }}>
        <RadioIcon active={this.state.paymentMethod === PAYMENT_METHOD_OTHER_CARD} />
        <SecondTitleLabel>Другая карта:</SecondTitleLabel>
      </RadioTitleContainer>
    </TouchableHighlight>,
    <ShadowContainer
      key="other-card-shadow"
      shadowRadius={7}
      borderRadius={3}
      color={0x000000}
      colorAlpha={80}
      bgColor={0xffffff}
      bgColorAlpha={0xff}
    >
      <OtherCardsContainer>
        <TouchableHighlight
          onPress={() => this.setPaymentMethod(PAYMENT_METHOD_OTHER_CARD)}
          style={{ borderTopLeftRadius: 3, borderTopRightRadius: 3 }}
          underlayColor="rgba(0,0,0,0.07)"
          key="card-body"
        >
          <OtherCardsHeaderContainer>
            <VisaImage source={visaImg} style={{ marginRight: 15 }} />
            <MasterCardImage source={mcImg} />
            <OtherCardsLabel numberOfLines={1}>Другая Карта </OtherCardsLabel>
            <ArrowIcon />
          </OtherCardsHeaderContainer>
        </TouchableHighlight>
        <Collapsible collapsed={this.state.otherCardCollapsed}>
          <OtherCardsPlatonFrameContainer>
            <ActivityIndicator size="large" color="#006DF0" />
          </OtherCardsPlatonFrameContainer>
        </Collapsible>
      </OtherCardsContainer>
    </ShadowContainer>,
  ];

  render() {
    const titleLabel = 'Выберите способ оплаты';
    const amount = 10;
    const serviceCost = 45;
    const totalCost = 450;
    return (
      <Wrapper>
        <ShadowContainer
          shadowRadius={7}
          borderRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
        >
          <Container>
            <TitleLabel numberOfLines={1}>{titleLabel.toUpperCase()}:</TitleLabel>
            { this.renderSavedCardsMethod() }
            { this.renderOtherCardMethod() }
            <FooterRegularLabel>
              Всего билетов:
              <FooterRegularLabelB> {amount} шт.</FooterRegularLabelB>
            </FooterRegularLabel>
            { (serviceCost > 0) &&
              <FooterRedLabel>Сервисные услуги: {serviceCost} грн.*</FooterRedLabel>
            }
            <FooterBlueLabel>
              Итого к оплате:
              <FooterBlueLabelB> {totalCost} грн</FooterBlueLabelB>
            </FooterBlueLabel>
          </Container>
        </ShadowContainer>
      </Wrapper>
    );
  }
}

ETicketMethod.propTypes = {

};
