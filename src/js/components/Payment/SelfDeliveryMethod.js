import React, { PureComponent } from 'react';
import { Platform, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import {
  InputError,
  StyledTextInput,
  InputContainer,
} from 'components/FormComponents';

import {
  Wrapper,
  ShadowContainer,
  Container,
  TitleLabel,
  MethodContainer,
  MethodLabelContainer,
  FooterRegularLabel,
  FooterRegularLabelB,
  FooterRedLabel,
  FooterBlueLabel,
  FooterBlueLabelB,
} from './CityDeliveryMethod';

import PurseIcon from './Icons/PurseIcon';
import MarkerIcon from './Icons/MarkerIcon';

const InfoTitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  color: #4F4F4F;
  margin-left: 20px;
  margin-right: 20px;
`;

const CashBoxesContainer = styled.View`
  margin-bottom: 7px;
`;

const CashBoxWrapper = styled.View`
  padding-left: 18px;
  padding-right: 18px;
`;

const CashBoxContainer = styled.View`
  border: 1px solid #B6B6B6;
  border-radius: 3px;
  padding: 14px;
  margin-top: 5px;
  margin-bottom: 5px;
  margin-left: 5px;
  margin-right: 5px;
`;

const CashBoxSelectedContainer = CashBoxContainer.extend`
  border-color: #fff;
  background-color: #fff;
  margin: 0;
`;

const CashBoxAddressLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #848484;
`;

const CashBoxAdressRow = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 12px;
`;

const CashBoxAddressLinkLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  text-decoration-line: underline;
  color: #006DF0;
  margin-left: 12px;
`;

export default class SelfDeliveryMethod extends PureComponent {
  state = {
  };

  renderCashBoxes = () => {
    const casboxes = [
      {
        id: 123,
        address: 'ТЦ "Комильфо" (ул.Сумская, 13) - САМОВЫВОЗ -Харьков, Касса в ТЦ "Комильфо"',
      },
      {
        id: 132,
        address: 'ТЦ "Комильфо" (ул.Сумская, 13) - САМОВЫВОЗ -Харьков, Касса в ТЦ "Комильфо"',
      },
      {
        id: 153,
        address: 'ТЦ "Комильфо" (ул.Сумская, 13) - САМОВЫВОЗ -Харьков, Касса в ТЦ "Комильфо"',
      },
    ];

    const rows = [];
    casboxes.forEach((cashbox) => {
      rows.push(
        <CashBoxWrapper key={`cashbox${cashbox.id}`}>
          { this.props.selectedCashBoxId === cashbox.id &&
            <ShadowContainer
              shadowRadius={5}
              borderRadius={3}
              color={0x000000}
              colorAlpha={80}
              bgColor={0xffffff}
              bgColorAlpha={0xff}
            >
              <CashBoxSelectedContainer>
                <CashBoxAddressLabel>{cashbox.address}</CashBoxAddressLabel>
                <CashBoxAdressRow>
                  <MarkerIcon color="#006DF0" />
                  <TouchableOpacity
                    activeOpacity={0.6}
                    onPress={() => {}}
                  >
                    <CashBoxAddressLinkLabel>Как нас найти</CashBoxAddressLinkLabel>
                  </TouchableOpacity>
                </CashBoxAdressRow>
              </CashBoxSelectedContainer>
            </ShadowContainer>
          }
          { this.props.selectedCashBoxId !== cashbox.id &&
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => {}}
            >
              <CashBoxContainer>
                <CashBoxAddressLabel>{cashbox.address}</CashBoxAddressLabel>
                <CashBoxAdressRow>
                  <MarkerIcon color="#006DF0" />
                  <TouchableOpacity
                    activeOpacity={0.6}
                    onPress={() => {}}
                  >
                    <CashBoxAddressLinkLabel>Как нас найти</CashBoxAddressLinkLabel>
                  </TouchableOpacity>
                </CashBoxAdressRow>
              </CashBoxContainer>
            </TouchableOpacity>
          }
        </CashBoxWrapper>,
      );
    });

    return rows;
  }

  render() {
    const amount = 100;
    const deliveryCost = 50;
    const totalCost = 400;
    return (
      <Wrapper>
        <InfoTitleLabel>Выберите кассу города:</InfoTitleLabel>
        <CashBoxesContainer>
          { this.renderCashBoxes() }
        </CashBoxesContainer>
        <ShadowContainer
          shadowRadius={7}
          borderRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
        >
          <Container>
            <TitleLabel numberOfLines={1}>Cпособ оплаты:</TitleLabel>
            <ShadowContainer
              key="other-card-shadow"
              shadowRadius={7}
              borderRadius={3}
              color={0x000000}
              colorAlpha={50}
              bgColor={0xffffff}
              bgColorAlpha={0xff}
            >
              <MethodContainer>
                <PurseIcon />
                <MethodLabelContainer>Наличный расчет</MethodLabelContainer>
              </MethodContainer>
            </ShadowContainer>

            <FooterRegularLabel>
              Всего билетов: <FooterRegularLabelB> {amount} шт.</FooterRegularLabelB>
            </FooterRegularLabel>
            <FooterBlueLabel>
              Итого к оплате: <FooterBlueLabelB> {totalCost} грн</FooterBlueLabelB>
            </FooterBlueLabel>
          </Container>
        </ShadowContainer>
      </Wrapper>
    );
  }
}

SelfDeliveryMethod.propTypes = {

};
