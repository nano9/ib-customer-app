import React, { PureComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import SideShadowSource from 'components/SideShadowSource';

import CashBoxIcon from './Icons/CashBoxIcon';
import CourierIcon from './Icons/CourierIcon';
import PackageIcon from './Icons/PackageIcon';
import ETicketIcon from './Icons/ETicketIcon';

const Wrapper = styled.View`
  width: 100%;
  padding: 0px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

const ItemTouchableWrapper = styled(TouchableOpacity)`
  width: 100%;
  height: 54px;
`;

const ItemContainer = styled.View`
  width: 100%;
  height: 54px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  background-color: #fff;
  border-bottom-width: 1px;
  border-bottom-color: #E2E2E2;
  padding-left: 18px;
`;

const ItemLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: ${({ active }) => (active ? '#006DF0' : '#A7A7A7')};
  margin-left: 14px;
`;

const ShadowWrapperBottom = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
`;

const ShadowWrapperTop = ShadowWrapperBottom.extend`
  bottom: auto;
  top: 0;
`;

const InfoTouchableWrapper = styled(TouchableOpacity)`
  width: 32px;
  height: 32px;
  position: absolute;
  right: 16px;
  top: 11;
  background-color: #4F4F4F;
  border-radius: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const InfoLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 22px;
  color: #F2F2F2;
`;

export const METHOD_SELF_DELIVERY = 'METHOD_SELF_DELIVERY';
export const METHOD_CITY_DELIVERY = 'METHOD_CITY_DELIVERY';
export const METHOD_ETICKET = 'METHOD_ETICKET';
export const METHOD_UA_DELIVERY = 'METHOD_UA_DELIVERY';

export default class PaymentMenu extends PureComponent {
  showInfoTooltip = () => {}

  renderNormalItem = (itemData, isActive) => {
    const {
      id,
      Icon,
      label,
      extraLabel,
    } = itemData;
    return (
      <ItemTouchableWrapper onPress={() => this.props.onDeliveryMethodChange(id)} activeOpacity={1}>
        <ItemContainer>
          <Icon active={isActive} />
          <ItemLabel numberOfLines={1} active={isActive}>
            {label}
            <ItemLabel numberOfLines={1}> {extraLabel}</ItemLabel>
          </ItemLabel>
          {id === METHOD_ETICKET &&
            <InfoTouchableWrapper onPress={this.showInfoTooltip} activeOpacity={0.6}>
              <InfoLabel>?</InfoLabel>
            </InfoTouchableWrapper>
          }
        </ItemContainer>
      </ItemTouchableWrapper>
    );
  }

  renderItems = (itemsData) => {
    const { activeDeliveryMethod } = this.props;
    const items = [];
    if (activeDeliveryMethod === METHOD_SELF_DELIVERY) {
      const { Icon } = itemsData[1];
      items.push(this.renderNormalItem(itemsData[0], true));
      items.push(
        <ItemTouchableWrapper
          onPress={() => this.props.onDeliveryMethodChange(itemsData[1].id)}
          activeOpacity={1}
        >
          <ItemContainer>
            <Icon />
            <ItemLabel numberOfLines={1}>{itemsData[1].label}</ItemLabel>
            <ShadowWrapperTop>
              <SideShadowSource goes="from-top" bgColor="#fff" />
            </ShadowWrapperTop>
          </ItemContainer>
        </ItemTouchableWrapper>,
      );
      items.push(this.renderNormalItem(itemsData[2]));
    } else if (activeDeliveryMethod === METHOD_CITY_DELIVERY || activeDeliveryMethod === METHOD_UA_DELIVERY) {
      const { Icon: Icon1 } = itemsData[0];
      const { Icon: Icon3 } = itemsData[2];
      items.push(
        <ItemTouchableWrapper onPress={() => this.props.onDeliveryMethodChange(itemsData[0].id)} activeOpacity={1}>
          <ItemContainer>
            <Icon1 />
            <ItemLabel numberOfLines={1}>{itemsData[0].label}</ItemLabel>
            <ShadowWrapperBottom>
              <SideShadowSource goes="from-bottom" bgColor="#fff" />
            </ShadowWrapperBottom>
          </ItemContainer>
        </ItemTouchableWrapper>,
      );
      items.push(this.renderNormalItem(itemsData[1], true));
      items.push(
        <ItemTouchableWrapper onPress={() => this.props.onDeliveryMethodChange(itemsData[2].id)} activeOpacity={1}>
          <ItemContainer>
            <Icon3 />
            <ItemLabel numberOfLines={1}>
              {itemsData[2].label}
              <ItemLabel numberOfLines={1}> {itemsData[2].extraLabel}</ItemLabel>
            </ItemLabel>
            <ShadowWrapperTop>
              <SideShadowSource goes="from-top" bgColor="#fff" />
            </ShadowWrapperTop>
            <InfoTouchableWrapper onPress={this.showInfoTooltip} activeOpacity={0.6}>
              <InfoLabel>?</InfoLabel>
            </InfoTouchableWrapper>
          </ItemContainer>
        </ItemTouchableWrapper>,
      );
    } else if (activeDeliveryMethod === METHOD_ETICKET) {
      const { Icon } = itemsData[1];
      items.push(this.renderNormalItem(itemsData[0]));
      items.push(
        <ItemTouchableWrapper onPress={() => this.props.onDeliveryMethodChange(itemsData[1].id)} activeOpacity={1}>
          <ItemContainer>
            <Icon />
            <ItemLabel numberOfLines={1}>{itemsData[1].label}</ItemLabel>
            <ShadowWrapperBottom>
              <SideShadowSource goes="from-bottom" bgColor="#fff" />
            </ShadowWrapperBottom>
          </ItemContainer>
        </ItemTouchableWrapper>,
      );
      items.push(this.renderNormalItem(itemsData[2], true));
      items.push(<SideShadowSource goes="from-top" bgColor="#fff" />);
    }
    return items;
  }

  render() {
    const items = [
      {
        id: METHOD_SELF_DELIVERY,
        Icon: CashBoxIcon,
        label: 'Cамовывоз из кассы',
        extraLabel: '',
      },
      // {
      //   id: METHOD_CITY_DELIVERY,
      //   Icon: CourierIcon,
      //   label: 'Доставка курьером',
      //   extraLabel: '',
      // },
      {
        id: METHOD_UA_DELIVERY,
        Icon: CourierIcon,
        label: 'Доставка по Украине',
        extraLabel: '',
      },
      {
        id: METHOD_ETICKET,
        Icon: ETicketIcon,
        label: 'Электронный билет',
        extraLabel: '(0818563)',
      },
    ];

    return (
      <Wrapper style={{ paddingBottom: this.props.activeDeliveryMethod !== METHOD_ETICKET ? 10 : 0 }}>
        { this.renderItems(items) }
      </Wrapper>
    );
  }
}

PaymentMenu.propTypes = {
  onDeliveryMethodChange: PropTypes.func.isRequired,
  activeDeliveryMethod: PropTypes.string.isRequired,
};
