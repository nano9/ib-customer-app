import React, { PureComponent } from 'react';
import { Platform } from 'react-native';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import {
  InputError,
  StyledTextInput,
  InputContainer,
} from 'components/FormComponents';

import PurseIcon from './Icons/PurseIcon';

export const Wrapper = styled.View`
  background-color: #fff;
  width: 100%;
  padding: 3px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  padding-top: 10px;
`;

export const InputInfoContainer = styled.View`
  padding-left: 20px;
  padding-right: 20px;
  margin-bottom: 13px;
`;

export const ShadowContainer = styled(ViewWithShadow)`
  width: 100%;
  ${Platform.OS === 'ios' ? `
  box-shadow: 0px 0px 7px rgba(0, 0, 0, 0.3137); 
  margin: 7px; 
  border-radius: 3px;
  background-color: transparent;
  ` : ''}
`;

export const Container = styled.View`
  background-color: #fff;
  border-radius: 3px;
  padding: 15px 13px 20px 13px;
`;

export const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #4F4F4F;
  margin-bottom: 13px;
`;

export const MethodContainer = Container.extend`
  height: 60px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding: 0;
  padding-left: 17px;
`;

export const MethodLabelContainer = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #4F4F4F;
  margin-left: 14px;
`;

export const FooterRegularLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-size: 18px;
  color: #767676;
  margin-top: 10px;
  margin-left: 7px;
  margin-right: 7px;
`;

export const FooterRegularLabelB = FooterRegularLabel.extend`
  font-weight: 500;
  color: #292929;
`;

export const FooterRedLabel = FooterRegularLabel.extend`
  color: #EB5757;
`;

export const FooterBlueLabel = FooterRegularLabel.extend`
  color: #006DF0;
`;

export const FooterBlueLabelB = FooterBlueLabel.extend`
  font-weight: 500;
`;

export default class CityDeliveryMethod extends PureComponent {
  state = {
  };

  render() {
    const amount = 100;
    const deliveryCost = 50;
    const totalCost = 400;
    return (
      <Wrapper>
        <InputInfoContainer>
          <InputError style={{ textAlign: 'left', marginLeft: 0 }}>
            по городу стоимость доставки составляет 50 грн. Срок доставки 1-2 рабочих дня.
          </InputError>
          <InputContainer style={{ marginTop: 6, marginBottom: 0 }}>
            <StyledTextInput placeholder="Улица" />
          </InputContainer>
          <InputContainer style={{ marginTop: 11, marginBottom: 0 }}>
            <StyledTextInput placeholder="№ Дома" />
          </InputContainer>
          <InputContainer style={{ marginTop: 11, marginBottom: 0 }}>
            <StyledTextInput placeholder="№ Квартиры" />
          </InputContainer>
        </InputInfoContainer>

        <ShadowContainer
          shadowRadius={7}
          borderRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
        >
          <Container>
            <TitleLabel numberOfLines={1}>Cпособ оплаты:</TitleLabel>
            <ShadowContainer
              key="other-card-shadow"
              shadowRadius={7}
              borderRadius={3}
              color={0x000000}
              colorAlpha={50}
              bgColor={0xffffff}
              bgColorAlpha={0xff}
            >
              <MethodContainer>
                <PurseIcon />
                <MethodLabelContainer>Наличный расчет</MethodLabelContainer>
              </MethodContainer>
            </ShadowContainer>

            <FooterRegularLabel>
              Всего билетов: <FooterRegularLabelB> {amount} шт.</FooterRegularLabelB>
            </FooterRegularLabel>
            <FooterRedLabel>Услуги курьера: {deliveryCost} грн.*</FooterRedLabel>
            <FooterBlueLabel>
              Итого к оплате: <FooterBlueLabelB> {totalCost} грн</FooterBlueLabelB>
            </FooterBlueLabel>
          </Container>
        </ShadowContainer>
      </Wrapper>
    );
  }
}

CityDeliveryMethod.propTypes = {

};
