import React  from 'react';
import { requireNativeComponent, ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';

const RTCNestedScrollView = requireNativeComponent('RTCNestedScrollView', {
  name: 'RTCNestedScrollView',
  propTypes: {
    ...ScrollView.propTypes,
    sendMomentumEvents: PropTypes.bool.isRequired,
  },
  defaultProps: ScrollView.defaultProps,
});

export default function NestedScrollView({ children, contentContainerStyle, ...otherProps }) {
  return (
    <RTCNestedScrollView
      {...otherProps}
      sendMomentumEvents={otherProps.onMomentumScrollBegin || otherProps.onMomentumScrollEnd}
    >
      <View collapsable={false} style={contentContainerStyle}>
        {children}
      </View>
    </RTCNestedScrollView>
  );
}

NestedScrollView.propTypes = ScrollView.propTypes;
