import React from 'react';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';
import Svg, { Path, LinearGradient, Stop, Defs, Rect } from 'react-native-svg';

const Container = styled.View`
  width: 26px;
  height: 26px;
  border: ${({ checked }) => (checked ? '0' : '1px solid #73787D')};
  border-radius: 3px;
  margin-right: 8px;
  padding-bottom: 2px;
`;

const IconWrapper = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default function CheckBoxIcon({ checked }) {
  return (
    <Container checked={checked}>
      {checked &&
        <Svg width="26" height="26" viewBox="0 0 26 26">
          <Defs>
            <LinearGradient id="bg" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse">
              <Stop offset="0.0276243%" stopColor="#006DF0" />
              <Stop offset="100%" stopColor="#328DFA" />
            </LinearGradient>
          </Defs>
          <Rect fill="url(#bg)" width="26" height="26" rx="3" ry="3" />
        </Svg>
      }
      {checked &&
        <IconWrapper>
          <Svg width="20" height="15" viewBox="0 0 20 15">
            <Path fill="#FFFFFF" d="M 2.02542 5.22843C 2.35305 4.902 2.88425 4.902 3.21188 5.22843L 8.58138 10.5783C 8.90902 10.9047 8.90902 11.434 8.58138 11.7604L 6.80169 13.5336C 6.47406 13.8601 5.94286 13.8601 5.61523 13.5336L 0.245724 8.18374C -0.0819081 7.85731 -0.081908 7.32805 0.245724 7.00162L 2.02542 5.22843Z"/>
            <Path fill="#FFFFFF" d="M 19.7543 2.01801C 20.0819 2.34445 20.0819 2.8737 19.7543 3.20014L 8.15685 14.7552C 7.82921 15.0816 7.29802 15.0816 6.97039 14.7552L 5.19069 12.982C 4.86306 12.6556 4.86306 12.1263 5.19069 11.7999L 16.7881 0.244826C 17.1158 -0.0816087 17.647 -0.0816086 17.9746 0.244826L 19.7543 2.01801Z"/>
          </Svg>
        </IconWrapper>
      }
    </Container>
  );
}

CheckBoxIcon.propTypes = {
  checked: PropTypes.bool,
};

CheckBoxIcon.defaultProps = {
  checked: false,
};

