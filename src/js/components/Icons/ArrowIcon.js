import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Svg, { Path } from 'react-native-svg';

const Container = styled.View`
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: rotate(${({ rotate }) => rotate}deg);
`;

function getRotation(direction) {
  switch (direction) {
    case 'right': return '180';
    case 'down': return '-90';
    case 'up': return '90';
    default: return '0';
  }
}

export default function ArrowIcon({ direction, color }) {
  const rotate = getRotation(direction);
  return (
    <Container rotate={rotate}>
      <Svg width="12" height="16" viewBox="0 0 12 20">
        <Path fill={color} d="M 0 9.70448L 1.61663 8.19027L 12 18.5522L 10.3605 20L 0 9.70448Z" />
        <Path fill={color} d="M 12 1.53567L 10.3605 0L 0 9.70448L 1.74877 11.3425L 12 1.53567Z" />
      </Svg>
    </Container>
  );
}

ArrowIcon.propTypes = {
  direction: PropTypes.string,
  color: PropTypes.string,
};

ArrowIcon.defaultProps = {
  direction: 'down',
  color: '#4F4F4F',
};
