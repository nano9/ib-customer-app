import React from 'react';
import Svg, { Path, G, Use, Defs, LinearGradient, Stop } from 'react-native-svg';

export default function CloseBtnIcon() {
  return (
    <Svg width="19" height="18" viewBox="0 0 19 18">
      <G transform="translate(-3545 3167)">
        <Use href="#path0_fill" transform="translate(3545 -3167)" fill="#828282" />
        <Use href="#path1_fill" transform="translate(3545 -3167)" fill="#828282" />
      </G>
      <Defs>
        <Path id="path0_fill" d="M 0 1.63636L 1.72727 0L 18.9999 16.3636L 17.2727 18L 0 1.63636Z"/>
        <Path id="path1_fill" d="M 19 1.63636L 17.2727 0L 5.80935e-05 16.3636L 1.72733 18L 19 1.63636Z"/>
      </Defs>
    </Svg>
  );
}
