import React, { Component } from 'react';
import styled from 'styled-components/native';
import { TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types';
import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Rect,
} from 'react-native-svg';

import ArrowIcon from 'components/Icons/ArrowIcon';

const Touchable = styled(TouchableHighlight)`
  flex: 1;
`;

const ButtonWrapper = styled.View`
  position: relative;
  width: 100%;
  border-radius: 4px;
  height: ${({ height }) => height}px;
`;

const ButtonContainer = styled.View`
  flex: 1;
  width: 100%;
  height: ${({ height }) => height}px;
  display: flex;
  border-radius: 4px;
`;

const ButtonLabelContainer = styled.View`
  flex: 1;
  height: 100%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding-left: 2px;
  padding-right: 2px;
`;

const ButtonLabel = styled.Text`
  text-align: center;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: ${({ fontScale }) => fontScale || 18}px;
  color: ${({ styleType }) => (styleType === 'blue' ? '#fff' : '#4F4F4F')};
`;

function Button(props) {
  const {
    children,
    disabled,
    height,
    borderRadius,
    styleType,
  } = props;

  const opacity = disabled ? 0.4 : 1;
  const bgStartColor = styleType === 'blue' ? '#006DF1' : '#E7A90A';
  const bgEndColor = styleType === 'blue' ? '#328DFB' : '#FAE101';
  const strokeStartColor = styleType === 'blue' ? '#0267E0' : '#DEB923';
  const strokeEndColor = styleType === 'blue' ? '#3088F2' : '#E7D31F';

  return (
    <ButtonContainer height={height}>
      <Svg width="100%" height={height} preserveAspectRatio="none">
        <Defs>
          <LinearGradient id="bg" x1="0%" y1="0%" x2="100%" y2="0%">
            <Stop offset="0%" stopColor={bgStartColor} />
            <Stop offset="100%" stopColor={bgEndColor} />
          </LinearGradient>
          <LinearGradient id="stroke" x1="0%" y1="0%" x2="100%" y2="0%">
            <Stop offset="0%" stopColor={strokeStartColor} />
            <Stop offset="100%" stopColor={strokeEndColor} />
          </LinearGradient>
        </Defs>
        <Rect
          width="100%"
          height="100%"
          fill="url(#bg)"
          stroke="url(#stroke)"
          rx={borderRadius}
          ry={borderRadius}
          strokeWidth={1}
          fillOpacity={opacity}
        />
      </Svg>
      {children}
    </ButtonContainer>
  );
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool.isRequired,
  height: PropTypes.number.isRequired,
  borderRadius: PropTypes.number.isRequired,
};

export default class ButtonMain extends Component {
  constructor(props) {
    super(props);
    this.buttonRef = null;
  }

  onPress = () => {
    if (this.props.onPressGetRef) {
      this.props.onPressGetRef(this.buttonRef);
    }
  }

  render() {
    const {
      arrowDirection,
      disabled,
      label,
      fontScale,
      styleType,
      icon,
      borderRadius,
    } = this.props;
    const calculatedFontScale = (fontScale === 'small') ? 14 : (fontScale === 'big') ? 20 : 18;
    return (
      <Touchable onPress={this.onPress} underlayColor="rgba(0,0,0,0.2)" disabled={disabled} style={{ borderRadius }}>
        <ButtonWrapper innerRef={(comp) => { this.buttonRef = comp; }} height={this.props.height}>
          <Button {...this.props}>
            <ButtonLabelContainer>
              <ButtonLabel
                fontScale={calculatedFontScale}
                styleType={styleType}
                numberOfLines={1}
                style={icon ? { marginRight: 19 } : {}}
              >
                {label}
              </ButtonLabel>
              {icon}
              {arrowDirection &&
                <ArrowIcon direction={arrowDirection} />
              }
            </ButtonLabelContainer>
          </Button>
        </ButtonWrapper>
      </Touchable>
    );
  }
}

ButtonMain.propTypes = {
  label: PropTypes.string.isRequired,
  icon: PropTypes.node,
  arrowDirection: PropTypes.string,
  onPressGetRef: PropTypes.func,
  disabled: PropTypes.bool,
  height: PropTypes.number,
  borderRadius: PropTypes.number,
  fontScale: PropTypes.oneOf(['small', 'normal', 'big']),
  styleType: PropTypes.oneOf(['blue', 'default', undefined]),
};

ButtonMain.defaultProps = {
  icon: null,
  arrowDirection: null,
  onPressGetRef: null,
  disabled: false,
  height: 49,
  borderRadius: 4,
  fontScale: 'normal',
  styleType: 'default',
};
