import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Platform, Animated, TouchableHighlight, Dimensions } from 'react-native';
import { Link } from 'react-router-native';
import styled from 'styled-components/native';
import { Svg, Path } from 'react-native-svg';

import ShadowView from 'components/ShadowView';
import { StyledTextInput } from 'components/FormComponents';
import Row from 'components/Layouts/Row';


export const HEADER_TYPE_NORMAL = 'normal';
export const HEADER_TYPE_SMALL = 'small';
export const INPUT_SEARCH_ROW_TYPE_EMPTY = 'empty';
export const INPUT_SEARCH_ROW_TYPE_FILLED = 'filled';

const Wrapper = styled.View`
  width: 100%;
  height: ${({ height }) => height || 120}px;
  background-color: transparent;
`;

const Shadow = styled(ShadowView)`
  ${Platform.OS === 'ios' ? 'box-shadow: 0px 0px 7px rgba(126, 151, 168, 0.25); margin: 7px;' : ''}
  position: absolute;
  top: -8px;
  left: -7px;
  right: -7px;
  background-color: transparent;
`;

const ContentOverlay = styled.View`
  width: 100%;
  height: 106px;
  background: #fff;
`;

const Content = styled.View`
  height: 106px;
  position: absolute;
  left: 0;
  right: 0;
  top: 0px;
  padding-top: 15px;
  padding-bottom: 20px;
  background-color: transparent;
  display: flex;
  justify-content: flex-start;
  align-items: stretch;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 21px;
  font-size: 20px;
  text-align: center;
  color: rgba(79, 79, 79, 0.8);
  margin-bottom: 3px;
`;

const InputContainer = styled.View`
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  border-radius: 3px;
  height: 40px;
  border: 1px solid #B2B2B2;
  margin-top: 9px;
`;

const SearchIconContainer = styled.View`
  width: 22px;
  height: 22px;
  position: absolute;
  right: 9px;
  top: 9px;
`;

function SearchIcon() {
  return (
    <Svg width="22" height="22" viewBox="0 0 22 22">
      <Path fill="#A7A7A7" d="M 21.8097 20.878L 16.4622 15.5317C 17.8967 13.8829 18.7651 11.7317 18.7651 9.38049C 18.7651 4.20488 14.5544 0 9.38257 0C 4.20581 0 0 4.20976 0 9.38049C 0 14.5512 4.21069 18.761 9.38257 18.761C 11.7343 18.761 13.886 17.8927 15.5352 16.4585L 20.8827 21.8049C 21.0095 21.9317 21.1803 22 21.3462 22C 21.5121 22 21.6829 21.9366 21.8097 21.8049C 22.0634 21.5512 22.0634 21.1317 21.8097 20.878ZM 1.31249 9.38049C 1.31249 4.93171 4.9328 1.31707 9.37769 1.31707C 13.8275 1.31707 17.4429 4.93659 17.4429 9.38049C 17.4429 13.8244 13.8275 17.4488 9.37769 17.4488C 4.9328 17.4488 1.31249 13.8293 1.31249 9.38049Z" />
    </Svg>
  );
}

const BackButtonContainer = styled.View`
  width: 40px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 9px;
`;

function ArrowIcon() {
  return (
    <Svg width="12" height="20" viewBox="0 0 12 20">
      <Path fill="#4F4F4F" d="M 0 9.70448L 1.61663 8.19027L 12 18.5522L 10.3605 20L 0 9.70448Z" />
      <Path fill="#4F4F4F" d="M 12 1.53567L 10.3605 0L 0 9.70448L 1.74877 11.3425L 12 1.53567Z" />
    </Svg>
  );
}

const ResultButtonWrapper = styled.View`
  width: 100%;
  height: 100%;
  border-radius: 3px;
  position: absolute;
  right: 0px;
  top: 0px;
  bottom: 0px;
  background-color: #CBCBCB;
`;

const ResultButtonContainer = styled.View`
  height: 100%;
  width: 100%;
  background-color: transparent;
  display: flex;
  flex-direction: row;
  padding-left: 1px;
`;

const ResultButtonLabel = styled.Text`
  flex: 1;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  text-align: right;
  color: #4F4F4F;
  margin-top: 3px;
`;

const CloseButtonContainer = styled.View`
  height: 100%;
  width: 28px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 10px;
`;

function CloseIcon() {
  return (
    <Svg width="14" height="14" viewBox="0 0 14 14">
      <Path fill="#4F4F4F" d="M 12.7273 0L 14 1.27273L 1.27273 14L 0 12.7273L 12.7273 0Z" />
      <Path fill="#4F4F4F" d="M 14 12.7273L 12.7273 14L 6.06884e-07 1.27273L 1.27273 0L 14 12.7273Z" />
    </Svg>
  );
}

export default class SearchTopNavBar extends PureComponent {
  static propTypes = {
    headerType: PropTypes.oneOf([HEADER_TYPE_NORMAL, HEADER_TYPE_SMALL]),
    inputSearchRowType: PropTypes.oneOf([INPUT_SEARCH_ROW_TYPE_EMPTY, INPUT_SEARCH_ROW_TYPE_FILLED]),
    onFocusInputSearchRow: PropTypes.func,
    onSubmitInputSearchRow: PropTypes.func,
    headerHeight: PropTypes.number,
  };

  static defaultProps = {
    headerType: HEADER_TYPE_NORMAL,
    inputSearchRowType: INPUT_SEARCH_ROW_TYPE_EMPTY,
    onFocusInputSearchRow: undefined,
    onSubmitInputSearchRow: undefined,
    headerHeight: 120,
  };

  constructor(props) {
    super(props);
    this.inputRowMaxOffsetX = 600;
  }

  state = {
    containerScaleY: new Animated.Value(this.props.headerType === HEADER_TYPE_NORMAL ? 1 : 0.7924),
    containerOffsetY: new Animated.Value(this.props.headerType === HEADER_TYPE_NORMAL ? 0 : -11),
    inputRowOffsetX: new Animated.Value(this.props.inputSearchRowType === INPUT_SEARCH_ROW_TYPE_EMPTY ? 600 : 0),
  };

  componentDidMount() {
    this.inputRowMaxOffsetX = (Dimensions.get('window').width / 2) || 300;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.headerType !== this.props.headerType) {
      const ANIMATION_DURATION = 150;
      const IS_CURRENT_HEADER_TYPE_NORMAL = this.props.headerType === HEADER_TYPE_NORMAL;
      const { containerScaleY, containerOffsetY } = this.state;

      containerScaleY.setValue(IS_CURRENT_HEADER_TYPE_NORMAL ? 1 : 0.7924);
      containerOffsetY.setValue(IS_CURRENT_HEADER_TYPE_NORMAL ? 0 : -11);

      Animated.parallel([
        Animated.spring(
          containerScaleY,
          {
            toValue: IS_CURRENT_HEADER_TYPE_NORMAL ? 0.7924 : 1,
            duration: ANIMATION_DURATION,
            useNativeDriver: true,
          },
        ),
        Animated.spring(
          containerOffsetY,
          {
            toValue: IS_CURRENT_HEADER_TYPE_NORMAL ? -11 : 0,
            duration: ANIMATION_DURATION,
            useNativeDriver: true,
          },
        ),
      ]).start();
    }

    if (nextProps.inputSearchRowType !== this.props.inputSearchRowType) {
      const ANIMATION_DURATION = 300;
      const IS_CURRENT_INPUT_SEARCH_ROW_EMPTY = this.props.inputSearchRowType === INPUT_SEARCH_ROW_TYPE_EMPTY;
      const { inputRowOffsetX } = this.state;

      inputRowOffsetX.setValue(IS_CURRENT_INPUT_SEARCH_ROW_EMPTY ? this.inputRowMaxOffsetX : 0);
      Animated.timing(
        inputRowOffsetX,
        {
          toValue: IS_CURRENT_INPUT_SEARCH_ROW_EMPTY ? 0 : this.inputRowMaxOffsetX,
          duration: ANIMATION_DURATION,
          useNativeDriver: true,
        },
      ).start();
    }
  }

  render() {
    const { headerType, inputSearchRowType, headerHeight } = this.props;
    return (
      <Animated.View style={{ transform: [{ translateY: this.state.containerOffsetY }] }}>
        <Animated.View style={{ transform: [{ scaleY: this.state.containerScaleY }] }}>
          <Wrapper height={headerHeight}>
            <Shadow shadowRadius={7} colorAlpha={64} bgColor={0xffffff} bgColorAlpha={1}>
              <ContentOverlay />
            </Shadow>
          </Wrapper>
        </Animated.View>
        <Content>
          { headerType === HEADER_TYPE_NORMAL && <TitleLabel>Что-то ищете?</TitleLabel> }
          <Row style={{ paddingLeft: 22, paddingRight: 8 }}>
            <InputContainer>
              <StyledTextInput
                textMaxWidth={inputSearchRowType === INPUT_SEARCH_ROW_TYPE_EMPTY ? '100%' : '55%'}
                placeholder="Найти мероприятие, артиста"
                onFocus={this.props.onFocusInputSearchRow}
                onSubmitEditing={this.props.onSubmitInputSearchRow}
              />
              <SearchIconContainer>
                <SearchIcon />
              </SearchIconContainer>
              <Animated.View
                style={{
                  position: 'absolute',
                  right: 0,
                  height: '100%',
                  width: '50%',
                  transform: [{ translateX: this.state.inputRowOffsetX }],
                }}
              >
                <TouchableHighlight
                  onPress={() => {}}
                  style={{
                    borderRadius: 3,
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'rgba(255,255,255,0)',
                  }}
                >
                  <ResultButtonWrapper>
                    <ShadowView
                      shadowRadius={6}
                      cornerRadius={6}
                      color={0xffffff}
                      colorAlpha={200}
                      bgColor={0xffffff}
                      bgColorAlpha={0x1}
                      style={{ flex: 1 }}
                    >
                      <ResultButtonContainer>
                        <ResultButtonLabel numberOfLines={1}>Здесь выбранные фильтры</ResultButtonLabel>
                        <TouchableHighlight onPress={() => {}} underlayColor="rgba(0,0,0,0.1)">
                          <CloseButtonContainer>
                            <CloseIcon />
                          </CloseButtonContainer>
                        </TouchableHighlight>
                      </ResultButtonContainer>
                    </ShadowView>
                  </ResultButtonWrapper>
                </TouchableHighlight>
              </Animated.View>
            </InputContainer>
            <Link to="/" underlayColor="rgba(0,0,0,0.1)" style={{ marginLeft: 6 }}>
              <BackButtonContainer>
                <ArrowIcon />
              </BackButtonContainer>
            </Link>
          </Row>
        </Content>
      </Animated.View>
    );
  }
}
