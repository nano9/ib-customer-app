import React from 'react';
import { View, Picker } from 'react-native';
import styled from 'styled-components/native';

import MainButton from 'components/MainButton';

import {
  InputLabel,
  StyledTextInput,
  StyledPicker,
  InputError,
  InputLabelContainer,
  InputContainer,
  InputShadow,
} from 'components/FormComponents';

const InputRow = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin: -15px;
`;

const RowInputWrapper = styled.View`
  flex: 1;
  align-items: stretch;
  margin: 15px;
`;

const PickerDropdownButton = styled.View`
  position: absolute;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
  border: 1px solid rgba(79, 79, 79, 0.4);
  right: -1px;
  bottom: -1px;
  top: -1px;
  width: 50px;
`;

export default function PersonalInfoFormComponent() {
  return (
    <View>
      <InputLabelContainer>
        <InputLabel>ФИО</InputLabel><InputError>Поле не может быть пустым</InputError>
      </InputLabelContainer>
      <InputContainer>
        <StyledTextInput placeholder="ФИО" />
      </InputContainer>

      <InputLabelContainer>
        <InputLabel>E-mail</InputLabel><InputError>Этот E-mail уже используется</InputError>
      </InputLabelContainer>
      <InputContainer>
        <StyledTextInput keyboardType="email-address" placeholder="E-mail" />
      </InputContainer>

      <InputLabelContainer>
        <InputLabel>Телефон</InputLabel><InputError>Некорректный телефон</InputError>
      </InputLabelContainer>

      <InputRow>
        <RowInputWrapper>
          <InputContainer>
            <StyledPicker
              items={[
                { label: '+38 Украина', value: '1' },
                { label: '+123', value: '2' },
              ]}
            />
          </InputContainer>
        </RowInputWrapper>
        <RowInputWrapper>
          <InputContainer>
            <StyledTextInput keyboardType="phone-pad" placeholder="(098) 000-00-00" />
          </InputContainer>
        </RowInputWrapper>
      </InputRow>

      <InputLabelContainer>
        <InputLabel>Страна</InputLabel>
      </InputLabelContainer>
      <InputContainer>
        <StyledPicker
          onValueChange={(itemValue, itemIndex) => {
          }}
          items={[
            { label: 'Украина', value: '1' },
            { label: 'США', value: '2' },
          ]}
        />
      </InputContainer>

      <InputLabelContainer>
        <InputLabel>Город</InputLabel>
      </InputLabelContainer>
      <InputContainer>
        <StyledPicker
          onValueChange={(itemValue, itemIndex) => {
          }}
          items={[
            { label: 'Харьков', value: '1' },
            { label: 'Киев', value: '2' },
          ]}
        />
      </InputContainer>
    </View>
  );
}
