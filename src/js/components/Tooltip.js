import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { TouchableWithoutFeedback, Modal, View } from 'react-native';
import Svg, { Path } from 'react-native-svg';

const TooltipWrapper = styled.View`
  position: absolute;
  right: ${({ right }) => right};
  bottom: ${({ bottom }) => bottom};
  padding-bottom: 10px;
`;

const Container = styled.View`
  max-width: 230px;
  border-radius: 3px;
  background-color: rgba(0,0,0,0.73);
  padding: 13px 18px;
  margin-bottom: -1px;
`;

const Label = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 15px;
  text-align: center;
  color: #DEDEDE;
`;

const TriangleWrapper = styled(Svg)`
  position: absolute;
  right: 13px;
  bottom: 0px;
  margin-top: -1px;
`;

export default function Tooltip(props) {
  const {
    right,
    bottom,
    text,
    visible,
    onToggle,
  } = props;
  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      onRequestClose={() => {}}
    >
      <TouchableWithoutFeedback
        style={{ width: '100%', height: '100%' }}
        onPress={onToggle}
      >
        <View style={{ flex: 1 }}>
          <TooltipWrapper
            right={right}
            bottom={bottom}
          >
            <Container>
              <Label>{text}</Label>
            </Container>
            <TriangleWrapper width="15" height="9" viewBox="0 1 15 9">
              <Path d="M 7 9L 0 0L 15 0L 7 9Z" fill="#000000" fillOpacity="0.73" />
            </TriangleWrapper>
          </TooltipWrapper>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
}

Tooltip.propTypes = {
  right: PropTypes.number,
  bottom: PropTypes.number,
  text: PropTypes.string.isRequired,
  visible: PropTypes.bool,
  onToggle: PropTypes.func,
};

Tooltip.defaultProps = {
  visible: true,
  onToggle: undefined,
  right: 'auto',
  bottom: 'auto',
};
