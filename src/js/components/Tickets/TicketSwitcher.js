import React, { PureComponent } from 'react';
import { TouchableHighlight, View, Animated } from 'react-native';
import Svg, { Circle, Defs, LinearGradient, Stop } from 'react-native-svg';
import styled from 'styled-components/native';

const Container = styled.View`
  width: 60px;
  height: 32px;
  border: 1px solid #D2D2D2;
  border-radius: 16px;
  padding-top: 1px;
`;

function Switch() {
  return (
    <Svg width="28" height="28" viewBox="0 0 28 28">
      <Defs>
        <LinearGradient id="bg" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse">
          <Stop offset="0.0276243%" stopColor="#006DF0" />
          <Stop offset="100%" stopColor="#328DFA" />
        </LinearGradient>
      </Defs>
      <Circle fill="url(#bg)" cx={14} cy={14} r={14} />
    </Svg>
  );
}


export default class TicketSwitcher extends PureComponent {
  constructor(props) {
    super(props);
    const isActivatedInit = true;
    this.state = {
      isActivated: isActivatedInit,
      switchOffsetLeft: new Animated.Value(isActivatedInit ? 1 : 29),
    };
  }

  toggleState = () => {
    const isNowActivated = !this.state.isActivated;
    this.setState({ isActivated: isNowActivated });
    Animated.timing(
      this.state.switchOffsetLeft,
      {
        toValue: isNowActivated ? 1 : 29,
        duration: 150,
        useNativeDriver: true,
      }
    ).start();
    if (this.props.onChangeState) {
      this.props.onChangeState(isNowActivated);
    }
  }

  render() {
    return(
      <TouchableHighlight
        onPress={this.toggleState}
        style={{ borderRadius: 16 }}
        underlayColor='rgba(0,0,0,0.1)'
      >
        <Container>
        <Animated.View
          style={{ transform: [ { translateX: this.state.switchOffsetLeft } ] }}>
          <Switch/>
        </Animated.View>
        </Container>
      </TouchableHighlight>
    );
  }
}
