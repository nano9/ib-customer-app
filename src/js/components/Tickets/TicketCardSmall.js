import React, { PureComponent } from 'react';
import { View, Text, Image } from 'react-native';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import ButtonMain from 'components/ButtonMain';
import Row from 'components/Layouts/Row';

import TicketCardSeparator from './TicketCardSeparator';

export const Container = styled.View`
  height: 154px;
  background-color: transparent;
  border-radius: 3px;
  display: flex;
  flex-direction: row;
`;

export const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  text-align: center;
  color: #767676;
  margin-bottom: 10px;
`;

export const LefColContainer = styled.View`
  width: 68px;
  height: 100%;
  background-color: #fff;
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-top: 7px;
`;

export const ImageContainer = styled.View`
  width: 40px;
  height: 45px;
  border-radius: 2px;
`;

export const DateNumberLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  line-height: 25px;
  font-size: 32px;
  text-align: center;
  color: #4F4F4F;
  margin-top: 4px;
  margin-bottom: 0px;
`;

export const DateMonthLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 13px;
  font-size: 10px;
  text-align: center;
  color: #767676;
  margin-top: 1px;
`;

export const SeparatorColContainer = styled.View`
  width: 12px;
  height: 100%;
`;

export const RightColContainer = styled.View`
  flex: 1;
  height: 100%;
  flex-direction: row;
  background-color: #fff;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
`;

export const ContentContainer = styled.View`
  flex: 1;
  padding-top: 10px;
  padding-left: 10px;
`;

export const EventTitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 15px;
  color: #4F4F4F;
`;

export const EventPlaceLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 24px;
  font-size: 13px;
  color: #767676;
`;

export const EventRegularLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 15px;
  font-size: 14px;
  color: #767676;
  margin: 0px;
  margin-left: ${({ marginLeft }) => marginLeft || 0}px;
`;

export const B = EventRegularLabel.extend`
  font-weight: 500;
`;

export const OrderNumberLabel = EventRegularLabel.extend`
  color: #006DF0;
  font-weight: 500;
`;

export default class TicketCardSmall extends PureComponent {
  renderLeftColumn = (image, date) => {
    return (
      <LefColContainer>
        <ViewWithShadow
          key={23}
          color={0x000000}
          colorAlpha={0x53}
          shadowRadius={7}
          offsetY={4}
          cornerRadius={2}
        >
          <ImageContainer>
            <Image source={{ uri: image.size1, width: 40, height: 45 }} />
          </ImageContainer>
        </ViewWithShadow>
        <DateNumberLabel>{date.number}</DateNumberLabel>
        <DateMonthLabel>{date.month.toUpperCase()}</DateMonthLabel>
      </LefColContainer>
    );
  }

  renderRightColumn = (title, place, time, price, orderData) => {
    return (
      <RightColContainer>
        <ContentContainer>
          <EventTitleLabel numberOfLines={1}>{title}</EventTitleLabel>
          <EventPlaceLabel numberOfLines={1}>{place}</EventPlaceLabel>
          <Row marginTop={14} marginBottom={-1}>
            <EventRegularLabel>{time}</EventRegularLabel>
            <EventRegularLabel marginLeft={15}><B>Цена: {price}</B> грн</EventRegularLabel>
          </Row>
          <Row marginTop={10} marginBottom={-1}>
            <EventRegularLabel numberOfLines={1}>Балкон: ряд: 2, место: 16, 17, 18, 19, 20</EventRegularLabel>
          </Row>
          <Row marginTop={19} >
            <EventRegularLabel numberOfLines={1}><B>№ заказа: <OrderNumberLabel>{orderData.number}</OrderNumberLabel></B></EventRegularLabel>
          </Row>
        </ContentContainer>
        <View style={{ paddingTop: 36, width: 92 }}>
          <ViewWithShadow
            key={23}
            color={0x000000}
            colorAlpha={0x33}
            shadowRadius={7}
            cornerRadius={3}
          >
            <View style={{ height: 34 }}>
              <ButtonMain
                label={orderData.status === 'finished' ? 'Открыть' : orderData.status === 'pending-payment' ? 'Завершить' : 'Оформить'}
                height={34}
                fontScale='small'
                onPressGetRef={this.onRequestClose}
              />
            </View>
          </ViewWithShadow>
          { orderData.canBeCanceled &&
            <ViewWithShadow
              key={23}
              color={0x000000}
              colorAlpha={0x33}
              shadowRadius={7}
              cornerRadius={3}
            >
              <View style={{ height: 34 }}>
                <ButtonMain
                  label='Отменить'
                  height={34}
                  fontScale='small'
                  styleType='blue'
                  onPressGetRef={this.onRequestClose}
                />
              </View>
            </ViewWithShadow>
          }
        </View>
      </RightColContainer>
    );
  }

  render() {
    const {
      image,
      title,
      place,
      time,
      date,
      price,
      orderData,
    } = this.props.ticketData;
    return (
      <ViewWithShadow
        key={23}
        color={0x000000}
        colorAlpha={0x33}
        shadowRadius={7}
        offsetY={4}
        bgColor={0xFFFFFF}
        bgColorAlpha={0xFF}
        cornerRadius={3}
      >
        <Container>
          { this.renderLeftColumn(image, date) }
          <SeparatorColContainer>
            <TicketCardSeparator />
          </SeparatorColContainer>
          { this.renderRightColumn(title, place, time, price, orderData) }
        </Container>
      </ViewWithShadow>
    );
  }
}
