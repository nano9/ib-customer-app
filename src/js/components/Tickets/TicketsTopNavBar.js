import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Platform } from 'react-native';
import { Link } from 'react-router-native';
import styled from 'styled-components/native';
import { Svg, Use, Defs, Path } from 'react-native-svg';

import ShadowView from 'components/ShadowView';

const TopNavBarWrapper = styled.View`
  height: 81px;
  width: 100%;
`;

const Shadow = styled(ShadowView)`
  ${Platform.OS === 'ios' ? 'box-shadow: 0px 0px 7px rgba(126, 151, 168, 0.25); margin: 7px;' : ''}
  position: absolute;
  top: -8px;
  left: -7px;
  right: -7px;
`;

const TopNavBarContext = styled.View`
  flex-direction: row;
  align-items: center;
  height: 80px;
  width: 100%;
  padding: 0;
`;

const LabelContainer = styled.View`
  flex: 1;
  height: 100%;
  align-items: flex-start;
  justify-content: center;
  padding-left: 20px;
`;

const ButtonContainer = styled.View`
  flex: 1;
`;

const Label = styled.Text`
  font-family: Roboto;
  font-size: 20px;
  color: #4F4F4F;
`;

const BackButton = styled(Svg)`
  margin-left: 20px;
`;

const BackButtonContainer = styled.View`
  height: 100%;
  width: 54px;
  display: flex;
  justify-content: center;
  align-items: flex-end;
  padding-right: 13px;
`;

const ButtonWrapper = styled.View`
  display: flex;
  align-items: flex-end;
  padding-right: 17px;
  padding-top: 9px;
`;

const ButtonContent = styled.View`
  border: 2px solid #767676;
  border-radius: 3px;
  height: 28px;
  width: 140px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ButtonLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  line-height: 16px;
  font-size: 14px;
  text-align: center;
  color: #767676;
`;

const ButtonNotificationLabelContainer = styled.View`
  width: 26px;
  height: 26px;
  border-radius: 13px;
  background-color: #EB5757;
  position: absolute;
  right: 9px;
  top: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ButtonNotificationLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #FFFFFF;
  margin-top: -1px;
  line-height: 15px;
`;

function Button({ label, link }) {
  return (
    <ButtonWrapper>
      <Link to={link} underlayColor='rgba(0,0,0,0.1)'>
        <ButtonContent>
          <ButtonLabel>{label}</ButtonLabel>
        </ButtonContent>
      </Link>
      <ButtonNotificationLabelContainer>
        <ButtonNotificationLabel>3</ButtonNotificationLabel>
      </ButtonNotificationLabelContainer>
    </ButtonWrapper>
  );
}

export default class TicketsTopNavBar extends PureComponent {
  static propTypes = {
    type: PropTypes.oneOf('paid', 'not-paid'),
  };

  static defaultProps = {
    type: 'not-paid',
  };

  render() {
    const { type } = this.props;
    return (
      <TopNavBarWrapper>
        <Shadow shadowRadius={7} colorAlpha={64} bgColor={0xffffff} bgColorAlpha={255}>
          <TopNavBarContext>
            <LabelContainer>
              <Label>Мои билеты</Label>
            </LabelContainer>
            <ButtonContainer>
              <Button
                label={type === 'paid' ? 'Неоплаченные' : 'Оплаченные'}
                link={type === 'paid' ? '/pc/tickets' : '/pc/tickets/paid'}
              />
            </ButtonContainer>
          </TopNavBarContext>
        </Shadow>
      </TopNavBarWrapper>
    );
  }
}
