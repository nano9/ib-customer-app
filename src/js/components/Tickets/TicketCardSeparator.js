import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Svg, { Path, Circle } from 'react-native-svg';

const EdgeContainer = styled.View`
  width: 12px;
  height: 12px;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: rotate(${({ rotate }) => rotate}deg);
`;

function getRotation(rotation) {
  switch (rotation) {
    case 'right': return '90';
    case 'bottom': return '180';
    case 'left': return '-90';
    default: return '0';
  }
}

function SeparatorEdgeBg({ rotation }) {
  const rotate = getRotation(rotation);
  return (
    <EdgeContainer rotate={rotate}>
      <Svg width="12" height="12" viewBox="0 0 12 12">
        <Path fill="#FFFFFF" d="M 1.5 0C 1.5 1.33333 2 4 6 4C 10 4 10.5 1.33333 10.5 0L 12 0L 12 12L 0 12L 0 0L 1.5 0Z" />
      </Svg>
    </EdgeContainer>
  );
}

SeparatorEdgeBg.propTypes = {
  rotation: PropTypes.oneOf(['left', 'right', 'top', 'bottom']),
};

SeparatorEdgeBg.defaultProps = {
  rotation: 'top',
};

const DotBgContainer = styled.View`
  transform: rotate(${({ rotate }) => rotate}deg);
`;

function renderDots(dotsCount) {
  const dots = [];
  for (let i = 0; i < dotsCount; i++) {
    dots.push(
      <Circle key={`key${i}`} fill="#CDCDCD" cx={2} cy={(i * 8) + 2} r={2} />
    );
  }
  return dots;
}

function DotBg({ dotsCount, direction }) {
  const height = (dotsCount * 8) - 4;
  return (
    <DotBgContainer rotate={direction === 'row' ? 90 : 0}>
      <Svg width="4" height={height} viewBox={`0 0 4 ${height}`}>
        { renderDots(dotsCount) }
      </Svg>
    </DotBgContainer>
  );
}

DotBg.propTypes = {
  dotsCount: PropTypes.number,
  direction: PropTypes.oneOf(['row', 'column']),
};
  
DotBg.defaultProps = {
  dotsCount: 1,
  direction: 'row',
};

const Container = styled.View`
  height: ${({ direction }) => direction === 'row' ? '12px' : '100%'}
  width: ${({ direction }) => direction === 'row' ? '100%' : '12px'}
  display: flex;
  flex-direction: ${({ direction }) => direction || 'column'};
  justify-content: center;
  align-items: center;
  padding-left: -1px;
`;

const DotsContainer = styled.View`
  flex: 1;
  width: 100%;
  height: 100%;
  background-color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default function TicketCardSeparator({ direction, dots }) {
  return (
    <Container direction={direction}>
      <SeparatorEdgeBg rotation={direction === 'row' ? 'left' : 'top'}/>
      <DotsContainer>
        <DotBg dotsCount={dots} direction={direction} />
      </DotsContainer>
      <SeparatorEdgeBg rotation={direction === 'row' ? 'right' : 'bottom'}/>
    </Container>
  );
}

TicketCardSeparator.propTypes = {
  direction: PropTypes.oneOf(['row', 'column']),
  dots: PropTypes.number,
};

TicketCardSeparator.defaultProps = {
  direction: 'column',
  dots: 19,
};
