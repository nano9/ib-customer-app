import React, { PureComponent } from 'react';
import { View, Text, Image } from 'react-native';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import Row from 'components/Layouts/Row';
import TicketCardSeparator from 'components/Tickets/TicketCardSeparator';
import TicketSwitcher from 'components/Tickets/TicketSwitcher';

import barCodeImg from 'img/bar-code.png';
import qrCodeImg from 'img/qr-code.png';
import ibLogoImg from 'img/ticket-ib-logo.png';

const Container = styled.View`
  width: 300px;
  display: flex;
  justify-content: flex-start;
  align-items: stretch;
`;

const HeaderRowContainer = styled.View`
  width: 100%;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  background-color: #fff;
  margin-bottom: -1px;
`;

const HeaderTopContainer = styled.View`
  width: 100%;
  height: 56px;
  margin-bottom: 2px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const HeaderTopLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  text-align: right;
  color: #4F4F4F;
  margin-right: 16px;
  margin-left: 16px;
`;

const CodeNumberLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  line-height: 18px;
  font-size: 15px;
  text-align: center;
  color: #4F4F4F;
  margin-top: 9px;
`;

const SeparatorRowContainer = styled.View`
  width: 100%;
  height: 12px;
`;

const BodyRowContainer = styled.View`
  width: 100%;
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
  background-color: #fff;
  margin-top: -1px;
  padding-top: 19px;
  padding-left: 12px;
  padding-right: 12px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-bottom: 9px;
`;

const BodyRegularLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #767676;
`;

const BodyRow = Row.extend`
  align-items: flex-start;
`;

const BodyCell = styled.View`
  flex: 1;
`;

const BlueLabel = BodyRegularLabel.extend`
  color: #006DF0;
`;

const IBLogoImgContainer = styled.View`
  max-width: 250px;
  margin-top: 23px;
`;

const CODE_TYPE_BARCODE = 1;
const CODE_TYPE_QRCODE = 2;

export default class TicketCardBig extends PureComponent {
  state = {
    showCodeType: CODE_TYPE_BARCODE,
  };

  renderTicketHeader = (codeData) => {
    const { isBarCode, img, number } = codeData;
    const isShowBarCode = this.state.showCodeType === CODE_TYPE_BARCODE;
    return (
      <HeaderRowContainer>
        <HeaderTopContainer>
          <HeaderTopLabel>Штрих-код</HeaderTopLabel>
          <TicketSwitcher
            onChangeState={
              (isActivated) => this.setState({
                showCodeType: isActivated ? CODE_TYPE_BARCODE : CODE_TYPE_QRCODE
              })
            }
          />
          <HeaderTopLabel>QR-Код</HeaderTopLabel>
        </HeaderTopContainer>
        <Image source={ isShowBarCode ? barCodeImg : qrCodeImg } />
        <CodeNumberLabel>238912749016</CodeNumberLabel>
      </HeaderRowContainer>
    );
  }

  renderTicketBody = () => {
    return (
      <BodyRowContainer>
        <BodyRow marginTop={1} marginBottom={1}>
          <BodyCell style={{ marginRight: 10 }}>
            <BodyRegularLabel><BlueLabel>Днепропетровская филармония имени Л. Б. Когана</BlueLabel></BodyRegularLabel>
          </BodyCell>
          <BodyCell style={{ marginLeft: 10 }}>
            <BodyRegularLabel>Балкон:</BodyRegularLabel>
            <BodyRegularLabel>Ряд: 2, Место: 16</BodyRegularLabel>
          </BodyCell>
        </BodyRow>
        <BodyRow marginTop={8} marginBottom={9}>
          <BodyCell style={{ marginRight: 10 }}>
            <BodyRegularLabel>Стоимость:</BodyRegularLabel>
            <BodyRegularLabel>100 грн</BodyRegularLabel>
          </BodyCell>
          <BodyCell style={{ marginLeft: 10 }}>
            <BodyRegularLabel>Покупатель:</BodyRegularLabel>
            <BodyRegularLabel>Авторег Разробник</BodyRegularLabel>
          </BodyCell>
        </BodyRow>
        <CodeNumberLabel>Номер билета 1-23891274901612-321</CodeNumberLabel>
        <IBLogoImgContainer>
          <Image source={ibLogoImg} />
        </IBLogoImgContainer>
      </BodyRowContainer>
    );
  }

  render() {
    return(
      <ViewWithShadow
        key={23}
        color={0x000000}
        colorAlpha={0x43}
        shadowRadius={10}
        offsetY={4}
        cornerRadius={2}
        style={{ marginBottom: 40 }}
      >
        <Container>
          { this.renderTicketHeader({ isBarCode: true, img: '', number: 123455 }) }
          <SeparatorRowContainer>
            <TicketCardSeparator direction='row' dots={35} />
          </SeparatorRowContainer>
          { this.renderTicketBody() }
        </Container>
      </ViewWithShadow>
    );
  }
}
