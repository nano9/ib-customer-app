import React from 'react';
import { ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
`;

export default function LoadingSpinner({ loading, marginTop, marginBottom }) {
  return (
    <Container marginTop={marginTop} marginBottom={marginBottom}>
      <ActivityIndicator size="large" color="#177CF5" animating={loading} />
    </Container>
  );
}

LoadingSpinner.propTypes = {
  loading: PropTypes.bool,
  marginTop: PropTypes.number,
  marginBottom: PropTypes.number,
};

LoadingSpinner.defaultProps = {
  loading: true,
  marginTop: 0,
  marginBottom: 0,
};
