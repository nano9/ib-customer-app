import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, View } from 'react-native';
import Svg, {
  Path, LinearGradient, Stop, Defs,
} from 'react-native-svg';
import styled from 'styled-components/native';
import { screenDimensions, TOUR_CARD_MARGIN_BOTTOM } from 'constants';
import ViewWithShadow from 'components/ViewWithShadow';
import ImageWithShadow from 'components/ImageWithShadow';

const Container = styled.View`
  margin: 0px;
  margin-bottom: ${TOUR_CARD_MARGIN_BOTTOM}px;
  padding: 10px;
  border-radius: 3px;
  background: #FFF;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
`;

const LeftColumn = styled.View`
  width: ${({ width }) => width || 100}px;
`;

const AuthorLabel = styled.Text`
  margin-top: 135px;
  height: 18px;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 18px;
  font-size: 13px;
  text-align: center;
  color: #006DF0;
`;

const RightColumn = styled.View`
  flex: 1;
  margin-left: 10px;
`;

const DateLabel = styled.Text`
  margin-left: 0px;
  margin-right: 10px;
  margin-top: 10px;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #767676;
  margin-top: 0px;
  margin-bottom: 0px;
`;

const Separator = styled.View`
  height: 1px;
  background-color: #E0E0E0;
  margin-bottom: 10px;
  margin-top: 7px;
`;

const DateRowsContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
`;

const RightLeftColumn = styled.View`
  flex: 62;
`;

const RightRightColumn = styled.View`
  flex: 38;
`;

const DateBoldLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  line-height: 12px;
  font-size: 12px;
  color: #767676;
  margin-top: 0px;
  margin-right: 10px;
  margin-bottom: 8px;
`;

const PlaceLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: ${props => (props.big ? '14' : '12')}px;
  color: #${props => (props.big ? '4F4F4F' : '767676')};
  margin-top: 0px;
  margin-right: 10px;
`;

const PriceLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: ${props => (props.bold ? 'bold' : 'normal')};
  font-size: 12px;
  text-align: center;
  color: #${props => (props.bold ? '4F4F4F' : '767676')};
  line-height: 14px;
`;

const OrderButtonContainer = styled.View`
  width: 100%;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 8px;
  border-radius: 4px;
  padding: 0px;
`;

const OrderButtonBg = styled(Svg)`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;

const OrderButtonLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  line-height: 14px;
  font-size: 12px;
  text-align: center;
  color: #4F4F4F;
  border-radius: 3px;
`;

// const ImageContainer = styled(Image)`
//   width: 100px;
//   height: ${({ height }) => height || 144}px;
//   position: absolute;
//   left: 10px;
//   top: 10px;
//   border-radius: 2px;
// `;
const Image = styled.Image`
  height: ${({ height }) => height || 144}px;
  width: 100px;
  border-radius: 2px;
  position: absolute;
  top: 0px;
  left: 10px;
`;

function OrderButton(props) {
  return (
    <TouchableHighlight>
      <OrderButtonContainer>
        <OrderButtonBg width="121" height="58" viewBox="-1 0 132 59" preserveAspectRatio="none">
          <Defs>
            <LinearGradient id="bg" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse">
              <Stop offset="0%" stopColor="#FAE101" />
              <Stop offset="100%" stopColor="#E7A90A" />
            </LinearGradient>
            <LinearGradient id="stroke" x1="0%" y1="0%" x2="100%" y2="0%">
              <Stop offset="0%" stopColor="#E7D31F" />
              <Stop offset="100%" stopColor="#DEB923" />
            </LinearGradient>
          </Defs>
          <Path fill="url(#bg)" d="M 0 4C 0 1.79086 1.79086 0 4 0L 89 0C 91.2091 0 93 1.79086 93 4L 93 26C 93 28.2091 91.2091 30 89 30L 4 30C 1.79086 30 0 28.2091 0 26L 0 4Z"/>
          <Path fill="url(#stroke)" d="M 4 1L 89 1L 89 -1L 4 -1L 4 1ZM 92 4L 92 26L 94 26L 94 4L 92 4ZM 89 29L 4 29L 4 31L 89 31L 89 29ZM 1 26L 1 4L -1 4L -1 26L 1 26ZM 4 29C 2.34315 29 1 27.6569 1 26L -1 26C -1 28.7614 1.23858 31 4 31L 4 29ZM 92 26C 92 27.6569 90.6569 29 89 29L 89 31C 91.7614 31 94 28.7614 94 26L 92 26ZM 89 1C 90.6569 1 92 2.34315 92 4L 94 4C 94 1.23858 91.7614 -1 89 -1L 89 1ZM 4 -1C 1.23858 -1 -1 1.23858 -1 4L 1 4C 1 2.34315 2.34315 1 4 1L 4 -1Z"/>
        </OrderButtonBg>
        {props.children}
      </OrderButtonContainer>
    </TouchableHighlight>
  );
}

export default class TourCard extends PureComponent {
  renderConcertRows() {
    return this.props.tour.events.map(it => (
      <View key={it.id}>
        <Separator />
        <DateRowsContainer>
          <RightLeftColumn>
            <DateBoldLabel>{it.date}</DateBoldLabel>
            <PlaceLabel>
              <PlaceLabel big>{it.city},</PlaceLabel> {it.hallName}
            </PlaceLabel>
          </RightLeftColumn>
          <RightRightColumn>
            <PriceLabel>Цена от:</PriceLabel>
            <PriceLabel bold>{it.smallestPrice}</PriceLabel>
            <OrderButton>
              <OrderButtonLabel>Купить билет</OrderButtonLabel>
            </OrderButton>
          </RightRightColumn>
        </DateRowsContainer>
      </View>
    ));
  }

  render() {
    const { tour } = this.props;
    const { TOUR_CARD_IMG_HEIGHT } = screenDimensions.get();
    if (tour.events.length === 0) return false;

    return (
      <View style={{ paddingTop: 12 }}>
        <ViewWithShadow
          shadowRadius={5}
          cornerRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={255}
          top={0}
        >
          <Container>
            <LeftColumn>
              <AuthorLabel>{tour.name}</AuthorLabel>
            </LeftColumn>
            <RightColumn>
              <DateLabel>
                {tour.events[0].date} -
                {tour.events[tour.events.length - 1].date}
              </DateLabel>
              <DateLabel>{tour.events.length}</DateLabel>
              {this.renderConcertRows()}
            </RightColumn>
          </Container>
        </ViewWithShadow>
        {/* <Image height={TOUR_CARD_IMG_HEIGHT} source={{ uri: tour.image }} /> */}
        <ImageWithShadow
          height={TOUR_CARD_IMG_HEIGHT}
          width={118}
          uri={tour.image}
          shadowRadius={8}
          borderRadius={2}
          colorAlpha={100}
          absolutePosition
          left={6}
          right={10}
          top={0}
        />
      </View>
    );
  }
}

TourCard.propTypes = {
  tour: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    // TODO we should use PropTypes.arrayOf(EventCard.propTypes.event)
    events: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
};
