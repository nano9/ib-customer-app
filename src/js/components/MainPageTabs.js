import React, { PureComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import Row from 'components/Layouts/Row';
import StarIcon from 'components/Icons/StarIcon';

const Wrapper = styled.View`
  margin-left: -20px;
  margin-right: -20px;
  margin-top: 10px;
`;

const Container = styled.View`
  background-color: #fff;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 25px;
  font-size: 20px;
  color: #4F4F4F;
  margin-left: 11px;
`;

const ButtonLabel = TitleLabel.extend`
  margin-left: 0px;
  font-size: 18px;
  line-height: 18px;
  padding-top: 17px;
  padding-bottom: 12px;
  text-align: center;
  color: ${({ selected }) => selected ? '#167CF5' : '#4F4F4F'};
`;

const TitleRow = Row.extend`
  display: flex;
  justify-content: center;
`;

const ButtonsRow = TitleRow.extend`
  max-width: 273px;
  display: flex;
  align-items: center;
`;

const ButtonContainer = styled.View`
  flex: 1;
`;

const ALL_EVENTS_TAB_ID = 'allEvents';
const TOP_EVENTS_TAB_ID = 'topEvents';
const TOURS_TAB_ID = 'tours';

export default class MainPageTabs extends PureComponent {
  render() {
    const {
      selectedTabId,
      onSelectTab,
    } = this.props;
    return (
      <Wrapper>
        <ViewWithShadow
          shadowRadius={10}
          cornerRadius={0}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
        >
          <Container>
            <TitleRow marginTop={12} marginBottom={0}>
              <StarIcon />
              <TitleLabel>Мероприятия</TitleLabel>
            </TitleRow>
            <ButtonsRow marginTop={5} marginBottom={0}>

              <ButtonContainer>
                <TouchableOpacity onPress={() => onSelectTab(ALL_EVENTS_TAB_ID)}>
                  <ButtonLabel selected={selectedTabId === ALL_EVENTS_TAB_ID}>Все</ButtonLabel>
                </TouchableOpacity>
              </ButtonContainer>

              <ButtonContainer>
                <TouchableOpacity onPress={() => onSelectTab(TOP_EVENTS_TAB_ID)}>
                <ButtonLabel selected={selectedTabId === TOP_EVENTS_TAB_ID}>Топ</ButtonLabel>
                </TouchableOpacity>
              </ButtonContainer>

              <ButtonContainer>
                <TouchableOpacity onPress={() => onSelectTab(TOURS_TAB_ID)}>
                <ButtonLabel selected={selectedTabId === TOURS_TAB_ID}>Туры</ButtonLabel>
                </TouchableOpacity>
              </ButtonContainer>

            </ButtonsRow>
          </Container>
        </ViewWithShadow>
      </Wrapper>
    );
  }
}

MainPageTabs.propTypes = {
  selectedTabId: PropTypes.string.isRequired,
  onSelectTab: PropTypes.func.isRequired,
};
