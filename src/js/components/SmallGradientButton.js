import React, { PureComponent } from 'react';
import styled from 'styled-components/native';
import { TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types';
import Svg, {
  Path,
  Defs,
  LinearGradient,
  Stop,
} from 'react-native-svg';

const ButtonContainer = styled.View`
  flex: 1;
  width: 50px;
  height: 40px;
  display: flex;
  border-radius: 0px;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
`;

const ArrowIconContainer = styled.View`
  position: absolute;
  left: 0;
  right: 5;
  top: 1;
  bottom: 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const ArrowIcon = styled.View`
  width: 12px;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: rotate(${({ arrowStyle }) => (arrowStyle === 'down') && '-'}90deg);
`;

export function SmallGradientButton({ arrowStyle }) {
  return (
    <ButtonContainer>
      <Svg width="100%" height="40" viewBox="-1 0 361 40" preserveAspectRatio="none">
        <Defs>
          <LinearGradient id="bg" x1="0%" y1="0%" x2="100%" y2="0%">
            <Stop offset="0%" stopColor="#E7A90A" />
            <Stop offset="100%" stopColor="#FAE101" />
          </LinearGradient>
          <LinearGradient id="stroke" x1="0%" y1="0%" x2="100%" y2="0%">
            <Stop offset="0%" stopColor="#DEB923" />
            <Stop offset="100%" stopColor="#E7D31F" />
          </LinearGradient>
        </Defs>
        <Path fill="url(#bg)" fillOpacity={1} d="M 0 3C 0 1.34314 1.34315 0 3 0L 356 0C 357.657 0 359 1.34315 359 3L 359 46C 359 47.6569 357.657 49 356 49L 3.00001 49C 1.34315 49 0 47.6569 0 46L 0 3Z" />
        <Path fill="url(#stroke)" fillOpacity={1} d="M 3 1L 356 1L 356 -1L 3 -1L 3 1ZM 358 3L 358 46L 360 46L 360 3L 358 3ZM 356 48L 3.00001 48L 3.00001 50L 356 50L 356 48ZM 1 46L 1 3L -1 3L -1 46L 1 46ZM 3.00001 48C 1.89544 48 1 47.1046 1 46L -1 46C -1 48.2091 0.790874 50 3.00001 50L 3.00001 48ZM 358 46C 358 47.1046 357.105 48 356 48L 356 50C 358.209 50 360 48.2091 360 46L 358 46ZM 356 1C 357.105 1 358 1.89543 358 3L 360 3C 360 0.790857 358.209 -1 356 -1L 356 1ZM 3 -1C 0.790862 -1 -1 0.790859 -1 3L 1 3C 1 1.89543 1.89543 1 3 1L 3 -1Z" />
      </Svg>
      {arrowStyle &&
        <ArrowIconContainer>
          <ArrowIcon arrowStyle={arrowStyle}>
            <Svg width="12" height="20" viewBox="0 0 12 20">
              <Path fill="#4F4F4F" d="M 0 9.70448L 1.61663 8.19027L 12 18.5522L 10.3605 20L 0 9.70448Z" />
              <Path fill="#4F4F4F" d="M 12 1.53567L 10.3605 0L 0 9.70448L 1.74877 11.3425L 12 1.53567Z" />
            </Svg>
          </ArrowIcon>
        </ArrowIconContainer>
      }
    </ButtonContainer>
  );
}

SmallGradientButton.propTypes = {
  arrowStyle: PropTypes.string,
};

SmallGradientButton.defaultProps = {
  arrowStyle: null,
};
