import React, { Component } from 'react';
import { TouchableOpacity, View, Image } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import Collapsible from 'react-native-collapsible';

import ViewWithShadow from 'components/ViewWithShadow';
import ArrowIcon from 'components/Icons/ArrowIcon';
import MapImg from 'img/map.png';
import CellphoneIcon from 'components/SideBar/Icons/CellphoneIcon';
import Row from 'components/Layouts/Row';

const Separator = styled.View`
  width: 100%;
  height: 1px;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  background-color: #E0E0E0;
`;

const Wrapper = styled.View`
  width: 100%;
`;

const ItemContainer = styled.View`
  padding-left: 36px;
  height: 59px;
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  z-index: 190;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: ${({ isActivated }) => isActivated ? '500' : 'normal'};
  font-size: 16px;
  color: ${({ isActivated }) => isActivated ? '#167CF6' : '#767676;'};
  margin-left: 30px;
`;

const CollapsableContentContainer = styled.View`
  width: 100%;
  z-index: 200;
  padding-left: 8px;
  padding-right: 8px;
  padding-bottom: 8px;
`;

const CashboxAddressContainer = styled.View`
  background-color: #fff;
  border-radius: 3px;
  padding-left: 16px;
  padding-right: 16px;
  padding-top: 23px;
  padding-bottom: 22px;
  margin-bottom: 0px;
`;

const AddressLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 24px;
  font-size: 16px;
  color: #4F4F4F;
`;

const DateLabel = AddressLabel.extend`
  color: #767676;
  margin-top: 17px;
`;

const PhoneNumberLabel = AddressLabel.extend`
  font-size: 18px;
  color: #167CF6;
  margin-top: 0;
  margin-left: 16px;
`;

const ShowOnMapLabel = AddressLabel.extend`
  color: #167CF6;
  margin-top: 0;
  margin-left: 7px;
`;

export default class CashBoxCollapsableItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contentCollapsed: true,
    };
  }

  onTriggered = () => {
    this.setState({ contentCollapsed: !this.state.contentCollapsed });
  };

  render() {
    const {
      label,
      address,
      date,
      number,
    } = this.props;

    return (
      <Wrapper>
        <Separator />
        <TouchableOpacity activeOpacity={0.7} onPress={this.onTriggered}>
          <ItemContainer>
            <Row>
              <ArrowIcon
                direction={this.state.contentCollapsed ? 'down' : 'up'}
                color={this.state.contentCollapsed ? '#4F4F4F' : '#167CF6'}
              />
              <TitleLabel isActivated={!this.state.contentCollapsed}>{label}</TitleLabel>
            </Row>
          </ItemContainer>
        </TouchableOpacity>
        <Collapsible collapsed={this.state.contentCollapsed}>
          <CollapsableContentContainer>
            <ViewWithShadow
              shadowRadius={12}
              cornerRadius={12}
              color={0x000000}
              colorAlpha={80}
              bgColor={0xffffff}
              bgColorAlpha={0xff}
            >
              <CashboxAddressContainer>
                <AddressLabel numberOfLines={2}>{address}</AddressLabel>
                <DateLabel>{date}</DateLabel>
                <Row marginTop={17}>
                  <CellphoneIcon />
                  <PhoneNumberLabel>{number}</PhoneNumberLabel>
                </Row>
                <Row marginTop={18}>
                  <Image style={{ width: 37, height: 36 }} source={MapImg} />
                  <ShowOnMapLabel>Посмотреть на карте</ShowOnMapLabel>
                </Row>
              </CashboxAddressContainer>
            </ViewWithShadow>
          </CollapsableContentContainer>
        </Collapsible>
      </Wrapper>
    );
  }
}

CashBoxCollapsableItem.propTypes = {
  label: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  number: PropTypes.string.isRequired,
};

CashBoxCollapsableItem.defaultProps = {
  
};
