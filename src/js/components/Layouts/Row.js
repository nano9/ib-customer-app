import styled from 'styled-components/native';

export default styled.View`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
  align-items: center;
  margin-top: ${({ marginTop }) => (marginTop ? `${marginTop}px` : 'auto')};
  margin-bottom: ${({ marginBottom }) => (marginBottom ? `${marginBottom}px` : 'auto')};
`;
