import React, { PureComponent } from 'react';
import { TouchableHighlight } from 'react-native';
import styled from 'styled-components';

import ViewWithShadow from 'components/ViewWithShadow';

import MinusIcon from './Icons/MinusIcon';
import PlusIcon from './Icons/PlusIcon';

const Wrapper = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding-bottom: 40px;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  text-align: center;
  color: #4F4F4F;
  margin-top: 34px;
`;

const SubtitleLabel = TitleLabel.extend`
  font-size: 16px;
  margin-top: 82px;
`;

const B = TitleLabel.extend`
  font-size:  16px; 
  margin-top: 0px;
`;

const ButtonsWrapper = styled.View`
  margin-top: 15px;
`;

const ButtonsContainer = styled.View`
  height: 50px;
  border-radius: 4px;
  background-color: #fff;
  padding: 0px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const ButtonContainer = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 36px;
  height: 100%;
  border-top-${({ side }) => side || 'left'}-radius: 4px;
  border-bottom-${({ side }) => side || 'left'}-radius: 4px;
  border: 1px solid #F0F0F0;
`;

const ButtonsCounterContainer = styled.View`
  width: 108px;
  height: 100%;
  border: 1px solid #F0F0F0;
  justify-content: center;
  align-items: center;
  margin-left: -1px;
  margin-right: -1px;
`;

const CounterLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  text-align: center;
  color: rgba(79, 79, 79, 0.8);
`;

const TicketsLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
`;

const TicketsContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 13px;
`;

export default class SchemelessBodyInfo extends PureComponent {
  state = {
    counter: 2,
  };

  increaseCounter = () => this.setState({ counter: this.state.counter + 1 });

  decreaseCounter = () => {
    const counter = (this.state.counter - 1 < 1) ? 1 : this.state.counter - 1;
    this.setState({ counter });
  }

  render() {
    return (
      <Wrapper>
        <TitleLabel>Выберите билеты</TitleLabel>
        <SubtitleLabel>Входной билет - <B>240 грн</B></SubtitleLabel>
        <ButtonsWrapper>
          <ViewWithShadow
            shadowRadius={5}
            borderRadius={5}
            color={0x000000}
            colorAlpha={80}
            bgColor={0xffffff}
            bgColorAlpha={0xff}
          >
            <ButtonsContainer>

              <TouchableHighlight
                onPress={this.decreaseCounter}
                style={{ borderTopLeftRadius: 4, borderBottomLeftRadius: 4 }}
                underlayColor="rgba(0,0,0,0.07)"
              >
                <ButtonContainer side="left">
                  <MinusIcon />
                </ButtonContainer>
              </TouchableHighlight>

              <ButtonsCounterContainer>
                <CounterLabel>{this.state.counter}</CounterLabel>
              </ButtonsCounterContainer>

              <TouchableHighlight
                onPress={this.increaseCounter}
                style={{ borderTopRightRadius: 4, borderBottomRightRadius: 4 }}
                underlayColor="rgba(0,0,0,0.07)"
              >
                <ButtonContainer side="right">
                  <PlusIcon />
                </ButtonContainer>
              </TouchableHighlight>

            </ButtonsContainer>
          </ViewWithShadow>
        </ButtonsWrapper>
        <TicketsContainer>
          <TicketsLabel>Билетов осталось: 356</TicketsLabel>
        </TicketsContainer>
      </Wrapper>
    );
  }
}
