import React, { PureComponent } from 'react';
import { Image, TouchableHighlight, View, ScrollView } from 'react-native';
import styled from 'styled-components';

import ViewWithShadow from 'components/ViewWithShadow';
import SideShadowSource from 'components/SideShadowSource';
import TicketCardSeparator from 'components/Tickets/TicketCardSeparator';

import CartIconWithCounter from './Icons/CartIconWithCounter';
import CloseIcon from './Icons/CloseIcon';

const Wrapper = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  padding: 0px;
`;

const HeaderWrapper = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  background-color: #fff;
  padding-top: 13px;
  padding-bottom: 13px;
  padding-left: 12px;
  padding-right: 12px;
`;

const ImageWrapper = styled.View`
  width: 86px;
  margin-right: 12px;
`;

const InfoWrapper = styled.View`
  flex: 1;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #006DF0;
`;

const PlaceLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #4F4F4F;
  margin-top: 14px;
`;

const DateLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #767676;
  margin-top: 12px;
`;

const CartHeaderWrapper = styled.View`
  height: 36px;
  background-color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding-left: 15px;
  padding-right: 8px;
  padding-bottom: 9px;
`;

const CartHeaderTitleLabel = styled.Text`
  flex: 1;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  text-align: center;
  color: #4F4F4F;
  margin-bottom: 4px;
`;

const CartBodyWrapper = styled.View`
  background-color: #fff;
  padding-top: 16px;
  padding-bottom: 30px;
  padding-left: 25px;
  padding-right: 25px;
`;

const TicketWrapper = styled.View`
  height: 64px;
  background-color: transparent;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
`;

const TicketLeftCell = styled.View`
  width: 71px;
  height: 100%;
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
  background-color: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const TicketRightCell = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  height: 100%;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
  background-color: #fff;
  padding-left: 10px;
  padding-right: 10px;
`;

const PriceLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 26px;
  text-align: center;
  color: #4F4F4F;
`;

const CurrencyLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  text-align: center;
  color: #767676;
`;

const TicketInfoCell = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
`;

const TicketPlaceLabel = styled.Text`
  line-height: 16px;
  height: 16px;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #767676;
  text-align: left;
  margin-bottom: 10px;
`;

const B = TicketPlaceLabel.extend`
  font-weight: 500;
`;

const Cell = styled.View`
  flex: 1;
  line-height: 16px;
  height: 16px;
`;

export default class CartBodyInfo extends PureComponent {
  state = {

  };

  renderTicket = () => {
    return (
      <ViewWithShadow
        shadowRadius={5}
        cornerRadius={3}
        color={0x000000}
        colorAlpha={100}
        bgColor={0xffffff}
        bgColorAlpha={0xff}
      >
        <TicketWrapper>
          <TicketLeftCell>
            <PriceLabel>100</PriceLabel>
            <CurrencyLabel>гривен</CurrencyLabel>
          </TicketLeftCell>
          <TicketCardSeparator direction="column" dots={5} />
          <TicketRightCell>
            <TicketInfoCell>
              <TicketPlaceLabel numberOfLines={1}>Сектор: <B>Партер (левей)</B></TicketPlaceLabel>
              <View style={{ flexDirection: 'row' }}>
                <Cell>
                  <TicketPlaceLabel numberOfLines={1}>Ряд: <B>3</B></TicketPlaceLabel>
                </Cell>
                <Cell>
                  <TicketPlaceLabel numberOfLines={1}>Место: <B>33</B></TicketPlaceLabel>
                </Cell>
              </View>
            </TicketInfoCell>
            <TouchableHighlight onPress={() => {}} style={{ borderRadius: 3 }} underlayColor="rgba(0,0,0,0.07)">
              <View style={{ width: 36, height: 36, justifyContent: 'center', alignItems: 'center' }}>
                <CloseIcon />
              </View>
            </TouchableHighlight>
          </TicketRightCell>
        </TicketWrapper>
      </ViewWithShadow>
    );
  }

  render() {
    return (
      <Wrapper>
        <HeaderWrapper>
          <ImageWrapper>
            <ViewWithShadow
              shadowRadius={5}
              cornerRadius={3}
              color={0x000000}
              colorAlpha={70}
              bgColor={0xffffff}
              bgColorAlpha={0xff}
            >
              <Image source={{ uri: 'http://via.placeholder.com/350x150' }} style={{ height: 116, borderRadius: 3 }} />
            </ViewWithShadow>
          </ImageWrapper>

          <InfoWrapper>
            <TitleLabel>The World Famous Glenn Miller Orchestra (Глен Миллер)</TitleLabel>
            <PlaceLabel>Днепропетровская филармония имени Л. Б. Когана</PlaceLabel>
            <DateLabel>3 декабря 2017 12:00</DateLabel>
          </InfoWrapper>
        </HeaderWrapper>
        <SideShadowSource goes="from-top" bgColor="#fff" />
        <CartHeaderWrapper>
          <CartIconWithCounter counter={3} />
          <CartHeaderTitleLabel>Корзина</CartHeaderTitleLabel>
          <TouchableHighlight onPress={() => {}} style={{ borderRadius: 3 }} underlayColor="rgba(0,0,0,0.07)">
            <View style={{ width: 31, height: 26, justifyContent: 'center', alignItems: 'center' }}>
              <CloseIcon />
            </View>
          </TouchableHighlight>
        </CartHeaderWrapper>
        <SideShadowSource goes="from-top" bgColor="#fff" />
        <ScrollView>
          <CartBodyWrapper>
            {this.renderTicket()}
            {this.renderTicket()}
          </CartBodyWrapper>
        </ScrollView>
      </Wrapper>
    );
  }
}
