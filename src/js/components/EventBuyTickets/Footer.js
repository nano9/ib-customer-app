import React, { PureComponent } from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import SideShadowSource from 'components/SideShadowSource';
import Row from 'components/Layouts/Row';
import ViewWithShadow from 'components/ViewWithShadow';
import ButtonMain from 'components/ButtonMain';
import ArrowIcon from 'components/Icons/ArrowIcon';

import EIcon from 'components/Icons/EIcon';
import InfoIcon from './Icons/InfoIcon';

const Wrapper = styled.View`
  width: 100%;
  background-color: transparent;
`;

const Container = styled.View`
  background-color: #fff;
  display: flex;
  justify-content: center;
  align-items: stretch;
  padding-bottom: 30px;
  padding-top: 22px;
  padding-left: 20px;
  padding-right: 20px;
`;

const TicketsLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  color: #4F4F4F;
`;

const PriceLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  color: #006DF0;
`;

const ButtonRow = Row.extend`
  height: 49px;
  width: 100%;
`;

export const FOOTER_TYPE_1 = 'type1';
export const FOOTER_TYPE_2 = 'type2';

export default class Footer extends PureComponent {
  toogleTooltipVisibility = () => {

  }

  render() {
    const { type, onToggleTooltip } = this.props;
    const btnData = {
      icon: (type === FOOTER_TYPE_1) ? <EIcon /> : <ArrowIcon direction="up" />,
      label: `${(type === FOOTER_TYPE_1 ? 'Оформить' : 'Просмотреть')} билет`,
      borderRadius: type === FOOTER_TYPE_1 ? 25 : 3,
    };
    return (
      <Wrapper>
        <SideShadowSource goes="from-bottom" bgColor="#F2F2F2" />
        <Container>
          <Row marginBottom={12}>
            <View style={{ flex: 1 }}>
              <TicketsLabel>Всего билетов:</TicketsLabel>
              <PriceLabel>Сумма:</PriceLabel>
            </View>
            <View>
              <TicketsLabel>2 шт</TicketsLabel>
              <Row>
                <PriceLabel style={{ marginRight: 5 }}>500 грн</PriceLabel>
                <TouchableOpacity onPress={onToggleTooltip} style={{ borderRadius: 20 }} activeOpacity={0.6}>
                  <View>
                    <InfoIcon />
                  </View>
                </TouchableOpacity>
              </Row>
            </View>
          </Row>
          <ViewWithShadow
            shadowRadius={3}
            borderRadius={btnData.borderRadius}
            color={0x000000}
            colorAlpha={80}
            bgColor={0xffffff}
            bgColorAlpha={0xff}
          >
            <ButtonRow>
              <ButtonMain
                label={btnData.label}
                icon={btnData.icon}
                fontScale="big"
                height={49}
                borderRadius={btnData.borderRadius}
                onPressGetRef={this.onRequestClose}
              />
            </ButtonRow>
          </ViewWithShadow>
        </Container>
      </Wrapper>
    );
  }
}

Footer.propTypes = {
  type: PropTypes.oneOf([FOOTER_TYPE_1, FOOTER_TYPE_2]),
  onToggleTooltip: PropTypes.func,
};

Footer.defaultProps = {
  type: FOOTER_TYPE_1,
  onToggleTooltip: undefined,
};
