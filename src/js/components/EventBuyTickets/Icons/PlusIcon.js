import React from 'react';
import Svg, { Path } from 'react-native-svg';

export default function PlusIcon() {
  return (
    <Svg width="16" height="16" viewBox="0 0 16 16">
      <Path fill="#4F4F4F" d="M 7 0L 9 0L 9 16L 7 16L 7 0Z" />
      <Path fill="#4F4F4F" d="M 16 7L 16 9L 0 9L 8.74229e-08 7L 16 7Z" />
    </Svg>
  );
}
