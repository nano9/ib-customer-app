import React from 'react';
import Svg, { Path } from 'react-native-svg';

export default function CloseIcon() {
  return (
    <Svg width="16" height="16" viewBox="0 0 16 16">
      <Path fill="#767676" d="M 0 1.45455L 1.45454 0L 16 14.5455L 14.5454 16L 0 1.45455Z" />
      <Path fill="#767676" d="M 16 1.45455L 14.5455 0L 4.89208e-05 14.5455L 1.45459 16L 16 1.45455Z" />
    </Svg>
  );
}
