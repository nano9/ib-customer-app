import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Picker, View, TouchableHighlight } from 'react-native';
import styled from 'styled-components/native';
import { Svg, Use, Defs, LinearGradient, Stop, Path, Rect } from 'react-native-svg';

import ViewWithShadow from 'components/ViewWithShadow';
import { SmallGradientButton } from 'components/SmallGradientButton';
import CheckBoxIcon from 'components/Icons/CheckBoxIcon';

export const InputLabel = styled.Text`
  font-family: Roboto;
  font-size: 16px;
  line-height: 19px;
  color: #4F4F4F;
`;

export const InputError = InputLabel.extend`
  flex: 1;
  text-align: right;
  margin-left: 10px;
  color: #EB5757;
`;

export const InputLabelContainer = styled.View`
  flex-direction: row;
  margin-bottom: 2px;
`;

export const InputContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  border-radius: 3px;
  height: 40px;
  border: 1px solid #B2B2B2;
  margin-bottom: 16px;
`;

export const InputIcon = styled.View`
  position: absolute;
  right: 14;
  top: 0;
  height: 100%;
  flex-direction: row;
  align-items: center;
  align-self: stretch;
`;

export const InputShadowWrapper = styled.View`
  position: absolute;
  top: 0;
  width: 100%;
  height: 8px;
`;


const StyledTextInputContainer = styled.View`
  width: 100%;
  height: 100%;
  padding: 0px;
  flex: 1;
  background-color: #CBCBCB;
`;

const Test = styled.View`
  width: 100%;
  height: 100%;
  background-color: transparent;
`;

const StyledTextInputContent = styled.TextInput`
  flex: 1;
  height: 100%;
  border-radius: 2px;
  margin: 5px;
  color: #4F4F4F;
  position: absolute;
  left: 0;
  right: 0;
  top: -4;
  bottom: 0;
  height: 40px;
  font-size: 16px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
`;

const StyledTextAreaContent = StyledTextInputContent.extend`
  height: 100%;
  width: 100%;
  border: 1px solid blue;
  position: relative;
`;

const TextInputLimiter = styled.View`
  width: ${({ width }) => width || '100%'};
  height: 100%;
  padding-bottom: 20px;
  margin-top: -6px;
  margin-left: -6px;
`;

export function StyledTextInput({
  placeholder,
  keyboardType,
  textMaxWidth,
  onFocus,
  onSubmitEditing,
}) {
  return (
    <StyledTextInputContainer>
      <ViewWithShadow
        shadowRadius={6}
        cornerRadius={6}
        color={0xffffff}
        colorAlpha={242}
        bgColor={0xffffff}
        bgColorAlpha={0xff}
        style={{
          position: 'absolute',
          left: -5,
          right: -5,
          top: -2,
          bottom: -20,
        }}
      >
        <TextInputLimiter width={textMaxWidth}>
          <StyledTextInputContent
            placeholderTextColor="#4F4F4F"
            placeholder={placeholder}
            multiline={false}
            numberOfLines={1}
            keyboardType={keyboardType}
            onFocus={onFocus}
            onSubmitEditing={onSubmitEditing}
            underlineColorAndroid="transparent"
          />
        </TextInputLimiter>
      </ViewWithShadow>
    </StyledTextInputContainer>
  );
}

export function StyledTextArea({
  placeholder,
  keyboardType,
  textMaxWidth,
  onFocus,
  onSubmitEditing,
}) {
  return (
    <StyledTextInputContainer>
      <ViewWithShadow
        shadowRadius={6}
        cornerRadius={6}
        color={0xffffff}
        colorAlpha={242}
        bgColor={0xffffff}
        bgColorAlpha={0xff}
        style={{
          position: 'absolute',
          left: -5,
          right: -5,
          top: -2,
          bottom: -20,
        }}
      >
        <TextInputLimiter width={textMaxWidth}>
          <StyledTextAreaContent
            placeholderTextColor="#4F4F4F"
            placeholder={placeholder}
            multiline
            numberOfLines={4}
            keyboardType="default"
            onFocus={onFocus}
            onSubmitEditing={onSubmitEditing}
            underlineColorAndroid="transparent"
          />
        </TextInputLimiter>
      </ViewWithShadow>
    </StyledTextInputContainer>
  );
}

const StyledPickerContent = styled(Picker)`
  flex: 1;
  width: 100%;
  height: 100%;
  border-radius: 2px;
  margin: 5px;
  color: #4F4F4F;
  border: 1px solid red;
  position: absolute;
  left: ${({ offsetLeft }) => offsetLeft || 0}px;
  right: 60;
  top: -2;
  bottom: 0;
  height: 42px;
`;

const GradientButtonContainer = styled.View`
  position: absolute;
  right: 0;
  top: 0;
  bottom: 0;
  height: 40px;
`;

function renderPickerItems(items) {
  const arr = [];
  items.forEach((item) => {
    arr.push(
      <Picker.Item
        key={item.value}
        style={{ paddingRight: 60 }}
        label={item.label}
        value={item.value}
      />
    );
  });
  return arr;
}

export function StyledPicker({ selectedValue, onValueChange, items }) {
  return (
    <StyledTextInputContainer>
      <ViewWithShadow
        shadowRadius={6}
        cornerRadius={6}
        color={0xffffff}
        colorAlpha={242}
        bgColor={0xffffff}
        bgColorAlpha={0xff}
        style={{ position: 'absolute', left: -5, right: -5, top: -2, bottom: -20 }}
      >
        <StyledPickerContent
          selectedValue={selectedValue}
          onValueChange={onValueChange}
          style={{ backgroundColor: 'transparent' }}
        >
          { renderPickerItems(items) }
        </StyledPickerContent>
        <GradientButtonContainer>
          <SmallGradientButton arrowStyle="down" />
        </GradientButtonContainer>
      </ViewWithShadow>
    </StyledTextInputContainer>
  );
}

const StyledPickerTypeContainer = styled.View`
  width: 98px;
  height: 40px;
  background-color: rgba(79, 79, 79, ${({ transparent }) => (transparent ? 0 : 0.1)});
  position: absolute;
  top: 2;
  left: 5;
  bottom 0;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  padding-left: 11px;
`;

const StyledPickerTypeLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  color: #767676;
`;

export class StyledTypedPicker extends PureComponent {
  static propTypes = {
    selectedValue: PropTypes.string,
    onValueChange: PropTypes.func,
    type: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    })).isRequired,
  };

  static defaultProps = {
    selectedValue: 'empty',
    onValueChange: undefined,
  };

  state = {
    selectedValue: this.props.selectedValue,
  };

  onValueChange = (value, position) => {
    if (value !== this.state.selectedValue) {
      this.setState({ selectedValue: value });
    }
    if (this.props.onValueChange) {
      this.props.onValueChange(value, position);
    }
  }

  render() {
    const { type, items } = this.props;
    return (
      <StyledTextInputContainer>
        <ViewWithShadow
          shadowRadius={6}
          cornerRadius={6}
          color={0xffffff}
          colorAlpha={242}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
          style={{ position: 'absolute', left: -5, right: -5, top: -2, bottom: -20 }}
        >
          <StyledPickerTypeContainer transparent={this.state.selectedValue === 'empty'}>
            <StyledPickerTypeLabel>{type}</StyledPickerTypeLabel>
          </StyledPickerTypeContainer>
          <StyledPickerContent
            offsetLeft={98}
            onValueChange={this.onValueChange}
            selectedValue={this.state.selectedValue}
            enable
            style={{ backgroundColor: 'transparent' }}
          >
            { renderPickerItems(items) }
          </StyledPickerContent>
          <GradientButtonContainer>
            <SmallGradientButton arrowStyle="down" />
          </GradientButtonContainer>
        </ViewWithShadow>
      </StyledTextInputContainer>
    );
  }
}

export function InputShadow() {
  return (
    <InputShadowWrapper>
      <Svg width="100%" height="8">
        <LinearGradient id="shadow" x1="0" y1="0" x2="0" y2="100%">
          <Stop offset="0" stopColor="#000000" />
          <Stop offset="1" stopColor="#ffffff" />
        </LinearGradient>
        <Rect fill="url(#shadow)" x="0" y="0" width="100%" height="8" opacity={0.3} rx={3} ry={3} />
      </Svg>
    </InputShadowWrapper>
  );
}

export function LockIcon() {
  return (
    <Svg width="22" height="27" viewBox="-1 -1 22 27">
      <Use href="#path0_fill" fill="url(#paint0_linear)" />
      <Use href="#path1_fill" fill="url(#paint1_linear)" />
      <Use href="#path2_stroke" fill="url(#paint2_linear)" />
      <Defs>
        <LinearGradient id="paint0_linear" x1="0" y1="0" x2="1" y2="0" gradientUnits="userSpaceOnUse" gradientTransform="matrix(19.6429 1.85549e-14 1.35964e-14 25 -4.71804e-15 -12.5)">
          <Stop offset="0" stopColor="#006DF0" />
          <Stop offset="1" stopColor="#328DFA" />
        </LinearGradient>
        <LinearGradient id="paint1_linear" x1="0" y1="0" x2="1" y2="0" gradientUnits="userSpaceOnUse" gradientTransform="matrix(19.6429 1.85549e-14 1.35964e-14 25 -4.71804e-15 -12.5)">
          <Stop offset="0" stopColor="#006DF0" />
          <Stop offset="1" stopColor="#328DFA" />
        </LinearGradient>
        <LinearGradient id="paint2_linear" x1="0" y1="0" x2="1" y2="0" gradientUnits="userSpaceOnUse" gradientTransform="matrix(19.6429 1.85549e-14 1.35964e-14 25 -4.71804e-15 -12.5)">
          <Stop offset="0" stopColor="#006DF0" />
          <Stop offset="1" stopColor="#328DFA" />
        </LinearGradient>
        <Path id="path0_fill" d="M 16.9643 9.31157L 16.9643 6.71296C 16.9643 3.01157 13.7598 0 9.82143 0C 5.88304 0 2.67857 3.01157 2.67857 6.71296L 2.67857 9.31157C 1.16027 9.58287 0 10.9551 0 12.6065L 0 21.6537C 0 23.4986 1.44777 25 3.22723 25L 16.4156 25C 18.1951 25 19.6429 23.4986 19.6429 21.6532L 19.6429 12.606C 19.6429 10.9551 18.4826 9.58287 16.9643 9.31157ZM 3.57143 6.71296C 3.57143 3.52176 6.375 0.925926 9.82143 0.925926C 13.2679 0.925926 16.0714 3.52176 16.0714 6.71296L 16.0714 9.25926L 3.57143 9.25926L 3.57143 6.71296ZM 18.75 21.6532C 18.75 22.988 17.7027 24.0741 16.4156 24.0741L 3.22723 24.0741C 1.94018 24.0741 0.892857 22.988 0.892857 21.6532L 0.892857 12.606C 0.892857 11.2713 1.94018 10.1852 3.22723 10.1852L 16.4156 10.1852C 17.7027 10.1852 18.75 11.2713 18.75 12.606L 18.75 21.6532Z"/>
        <Path id="path1_fill" d="M 9.82143 12.963C 8.83661 12.963 8.03571 13.7935 8.03571 14.8148L 8.03571 17.5926C 8.03571 18.6139 8.83661 19.4444 9.82143 19.4444C 10.8062 19.4444 11.6071 18.6139 11.6071 17.5926L 11.6071 14.8148C 11.6071 13.7935 10.8062 12.963 9.82143 12.963ZM 10.7143 17.5926C 10.7143 18.1032 10.3138 18.5185 9.82143 18.5185C 9.32902 18.5185 8.92857 18.1032 8.92857 17.5926L 8.92857 14.8148C 8.92857 14.3042 9.32902 13.8889 9.82143 13.8889C 10.3138 13.8889 10.7143 14.3042 10.7143 14.8148L 10.7143 17.5926Z"/>
        <Path id="path2_stroke" d="M 16.9643 9.31157L 16.4643 9.31157L 16.4643 9.73015L 16.8763 9.80378L 16.9643 9.31157ZM 2.67857 9.31157L 2.76652 9.80378L 3.17857 9.73015L 3.17857 9.31157L 2.67857 9.31157ZM 16.0714 9.25926L 16.0714 9.75926L 16.5714 9.75926L 16.5714 9.25926L 16.0714 9.25926ZM 3.57143 9.25926L 3.07143 9.25926L 3.07143 9.75926L 3.57143 9.75926L 3.57143 9.25926ZM 17.4643 9.31157L 17.4643 6.71296L 16.4643 6.71296L 16.4643 9.31157L 17.4643 9.31157ZM 17.4643 6.71296C 17.4643 2.70665 14.0063 -0.5 9.82143 -0.5L 9.82143 0.5C 13.5134 0.5 16.4643 3.3165 16.4643 6.71296L 17.4643 6.71296ZM 9.82143 -0.5C 5.63659 -0.5 2.17857 2.70665 2.17857 6.71296L 3.17857 6.71296C 3.17857 3.3165 6.12949 0.5 9.82143 0.5L 9.82143 -0.5ZM 2.17857 6.71296L 2.17857 9.31157L 3.17857 9.31157L 3.17857 6.71296L 2.17857 6.71296ZM 2.59062 8.81937C 0.826504 9.13459 -0.5 10.7204 -0.5 12.6065L 0.5 12.6065C 0.5 11.1897 1.49403 10.0312 2.76652 9.80378L 2.59062 8.81937ZM -0.5 12.6065L -0.5 21.6537L 0.5 21.6537L 0.5 12.6065L -0.5 12.6065ZM -0.5 21.6537C -0.5 23.7575 1.15469 25.5 3.22723 25.5L 3.22723 24.5C 1.74085 24.5 0.5 23.2397 0.5 21.6537L -0.5 21.6537ZM 3.22723 25.5L 16.4156 25.5L 16.4156 24.5L 3.22723 24.5L 3.22723 25.5ZM 16.4156 25.5C 18.4882 25.5 20.1429 23.7575 20.1429 21.6532L 19.1429 21.6532C 19.1429 23.2398 17.902 24.5 16.4156 24.5L 16.4156 25.5ZM 20.1429 21.6532L 20.1429 12.606L 19.1429 12.606L 19.1429 21.6532L 20.1429 21.6532ZM 20.1429 12.606C 20.1429 10.7204 18.8163 9.13458 17.0522 8.81937L 16.8763 9.80378C 18.1489 10.0312 19.1429 11.1898 19.1429 12.606L 20.1429 12.606ZM 4.07143 6.71296C 4.07143 3.83344 6.61421 1.42593 9.82143 1.42593L 9.82143 0.425926C 6.13579 0.425926 3.07143 3.21007 3.07143 6.71296L 4.07143 6.71296ZM 9.82143 1.42593C 13.0287 1.42593 15.5714 3.83344 15.5714 6.71296L 16.5714 6.71296C 16.5714 3.21007 13.5071 0.425926 9.82143 0.425926L 9.82143 1.42593ZM 15.5714 6.71296L 15.5714 9.25926L 16.5714 9.25926L 16.5714 6.71296L 15.5714 6.71296ZM 16.0714 8.75926L 3.57143 8.75926L 3.57143 9.75926L 16.0714 9.75926L 16.0714 8.75926ZM 4.07143 9.25926L 4.07143 6.71296L 3.07143 6.71296L 3.07143 9.25926L 4.07143 9.25926ZM 18.25 21.6532C 18.25 22.7291 17.4096 23.5741 16.4156 23.5741L 16.4156 24.5741C 17.9958 24.5741 19.25 23.2468 19.25 21.6532L 18.25 21.6532ZM 16.4156 23.5741L 3.22723 23.5741L 3.22723 24.5741L 16.4156 24.5741L 16.4156 23.5741ZM 3.22723 23.5741C 2.2333 23.5741 1.39286 22.7291 1.39286 21.6532L 0.392857 21.6532C 0.392857 23.2468 1.64705 24.5741 3.22723 24.5741L 3.22723 23.5741ZM 1.39286 21.6532L 1.39286 12.606L 0.392857 12.606L 0.392857 21.6532L 1.39286 21.6532ZM 1.39286 12.606C 1.39286 11.5301 2.2333 10.6852 3.22723 10.6852L 3.22723 9.68518C 1.64705 9.68518 0.392857 11.0124 0.392857 12.606L 1.39286 12.606ZM 3.22723 10.6852L 16.4156 10.6852L 16.4156 9.68518L 3.22723 9.68518L 3.22723 10.6852ZM 16.4156 10.6852C 17.4096 10.6852 18.25 11.5301 18.25 12.606L 19.25 12.606C 19.25 11.0124 17.9958 9.68518 16.4156 9.68518L 16.4156 10.6852ZM 18.25 12.606L 18.25 21.6532L 19.25 21.6532L 19.25 12.606L 18.25 12.606ZM 9.82143 12.463C 8.54348 12.463 7.53571 13.5347 7.53571 14.8148L 8.53571 14.8148C 8.53571 14.0524 9.12973 13.463 9.82143 13.463L 9.82143 12.463ZM 7.53571 14.8148L 7.53571 17.5926L 8.53571 17.5926L 8.53571 14.8148L 7.53571 14.8148ZM 7.53571 17.5926C 7.53571 18.8727 8.54348 19.9444 9.82143 19.9444L 9.82143 18.9444C 9.12973 18.9444 8.53571 18.355 8.53571 17.5926L 7.53571 17.5926ZM 9.82143 19.9444C 11.0994 19.9444 12.1071 18.8727 12.1071 17.5926L 11.1071 17.5926C 11.1071 18.355 10.5131 18.9444 9.82143 18.9444L 9.82143 19.9444ZM 12.1071 17.5926L 12.1071 14.8148L 11.1071 14.8148L 11.1071 17.5926L 12.1071 17.5926ZM 12.1071 14.8148C 12.1071 13.5347 11.0994 12.463 9.82143 12.463L 9.82143 13.463C 10.5131 13.463 11.1071 14.0524 11.1071 14.8148L 12.1071 14.8148ZM 10.2143 17.5926C 10.2143 17.8444 10.0207 18.0185 9.82143 18.0185L 9.82143 19.0185C 10.607 19.0185 11.2143 18.3621 11.2143 17.5926L 10.2143 17.5926ZM 9.82143 18.0185C 9.62214 18.0185 9.42857 17.8444 9.42857 17.5926L 8.42857 17.5926C 8.42857 18.3621 9.03589 19.0185 9.82143 19.0185L 9.82143 18.0185ZM 9.42857 17.5926L 9.42857 14.8148L 8.42857 14.8148L 8.42857 17.5926L 9.42857 17.5926ZM 9.42857 14.8148C 9.42857 14.563 9.62214 14.3889 9.82143 14.3889L 9.82143 13.3889C 9.03589 13.3889 8.42857 14.0453 8.42857 14.8148L 9.42857 14.8148ZM 9.82143 14.3889C 10.0207 14.3889 10.2143 14.563 10.2143 14.8148L 11.2143 14.8148C 11.2143 14.0453 10.607 13.3889 9.82143 13.3889L 9.82143 14.3889ZM 10.2143 14.8148L 10.2143 17.5926L 11.2143 17.5926L 11.2143 14.8148L 10.2143 14.8148Z"/>
      </Defs>
    </Svg>
  );
}

const CheckboxContainer = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding-left: 33px;
  padding-top: 10px;
  padding-bottom: 10px;
`;

const CheckboxLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 24px;
  font-size: 16px;
  color: #767676;
  margin-left: 38px;
`;

export class Checkbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isChecked: false,
    };
    this.toggleCheckedState = this.toggleCheckedState.bind(this);
  }

  toggleCheckedState() {
    this.setState({ isChecked: !this.state.isChecked });
  }

  render() {
    const { isChecked } = this.state;
    const { label } = this.props;
    return (
      <TouchableHighlight
        onPress={this.toggleCheckedState}
        underlayColor="rgba(0,0,0,0.05)"
        style={{ marginBottom: 17 }}
      >
        <CheckboxContainer>
          <CheckBoxIcon checked={isChecked} />
          <CheckboxLabel>{label}</CheckboxLabel>
        </CheckboxContainer>
      </TouchableHighlight>
    );
  }
}

