import React from 'react';
import PropTypes from 'prop-types';
import { requireNativeComponent, ViewPropTypes, View } from 'react-native';

export const RTCShadowView = requireNativeComponent('RTCShadowView', {
  name: 'RTCShadowView',
  propTypes: {
    shadowRadius: PropTypes.number,
    cornerRadius: PropTypes.number,
    color: PropTypes.number,
    colorAlpha: PropTypes.number,
    bgColor: PropTypes.number,
    bgColorAlpha: PropTypes.number,
    offsetX: PropTypes.number,
    offsetY: PropTypes.number,
    ...ViewPropTypes,
  },
  defaultProps: {
    color: 0,
    colorAlpha: 255,
    bgColor: 0,
    bgColorAlpha: 255,
    cornerRadius: 0,
    shadowRadius: 0,
    offsetX: 0,
    offsetY: 0,
  },
});

export default function ShadowView({ children, contentContainerStyle, ...otherProps }) {
  return (
    <RTCShadowView
      {...otherProps}
    >
      <View
        collapsable={false}
        style={[{ padding: otherProps.shadowRadius, borderRadius: otherProps.cornerRadius }, contentContainerStyle]}
      >
        {children}
      </View>
    </RTCShadowView>
  );
}

ShadowView.propTypes = RTCShadowView.propTypes;
