import React, { Component } from 'react';
import { TouchableOpacity, View, Image } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import Collapsible from 'react-native-collapsible';

import ViewWithShadow from 'components/ViewWithShadow';
import ArrowIcon from 'components/Icons/ArrowIcon';
import MapImg from 'img/map.png';
import CellphoneIcon from 'components/SideBar/Icons/CellphoneIcon';
import Row from 'components/Layouts/Row';
import { P, Span } from 'components/Markdown';

const Separator = styled.View`
  width: 100%;
  height: 1px;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  background-color: #E0E0E0;
`;

const Wrapper = styled.View`
  width: 100%;
`;

const ItemContainer = styled.View`
  padding-left: 20px;
  padding-right: 20px;
  height: 59px;
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  z-index: 190;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: ${({ isActivated }) => isActivated ? '#167CF6' : '#4F4F4F'};
  margin-left: 20px;
  margin-right: 20px;
`;

const CollapsableContentContainer = styled.View`
  width: 100%;
  z-index: 200;
  padding-top: 8px;
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: 8px;
  background-color: #F2F2F2;
`;

export default class FAQCollapsableItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contentCollapsed: true,
    };
  }

  onTriggered = () => {
    this.setState({ contentCollapsed: !this.state.contentCollapsed });
  };

  render() {
    const {
      label,
      text,
    } = this.props;

    return (
      <Wrapper>
        <Separator />
        <TouchableOpacity activeOpacity={0.7} onPress={this.onTriggered}>
          <ItemContainer>
            <Row>
              <ArrowIcon
                direction={this.state.contentCollapsed ? 'down' : 'up'}
                color={this.state.contentCollapsed ? '#4F4F4F' : '#167CF6'}
              />
              <TitleLabel
                isActivated={!this.state.contentCollapsed}
                numberOfLines={2}
              >
                {label}
              </TitleLabel>
            </Row>
          </ItemContainer>
        </TouchableOpacity>
        <Collapsible collapsed={this.state.contentCollapsed}>
          <CollapsableContentContainer>
            <P withPadding>
              Для начала постарайтесь не нервничать. Проверьте входящие письма, 
              действительно ли не пришло письмо с билетом, уделите пристальное 
              внимание папке «Спам» и папке "Промоакции". Если данного письма действительно 
              не нашлось, то проверьте свой личный кабинет на нашем сайте, скорее 
              всего билеты можно будет скачать в вашем личном кабинете, в разделе «заказы».
            </P>
            <P>
              Если же выше описанные способы не помогли, убедитесь в том, что платеж прошел 
              успешно (свяжитесь с банком или проверьте списание средств в личном кабинете банка). 
              После этого позвоните нам по номеру <Span>+38-097-000-70-40</Span>, либо напишите на нашу почту 
              <Span>info@internet-bilet.com.ua</Span>{'\n'}
              Наши операторы с радостью ответят вам на все интересующие 
              вопросы и помогут найти заветный билет.
            </P>
          </CollapsableContentContainer>
        </Collapsible>
      </Wrapper>
    );
  }
}

FAQCollapsableItem.propTypes = {
  label: PropTypes.string.isRequired,
  text: PropTypes.arrayOf(PropTypes.string).isRequired,
};

FAQCollapsableItem.defaultProps = {
  
};
