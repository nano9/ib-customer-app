import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Svg, {
  Rect,
  Defs,
  LinearGradient,
  Stop,
} from 'react-native-svg';

const DividerWrapper = styled(Svg)`
  zIndex: 999;
  elevation: 999;
  position: absolute;
  bottom: 54px;
  width: 100%;
  height: 10px;
  border: 1px solid red;
`;

export const SHADOW_ORIENTATION_TOP = 'top';
export const SHADOW_ORIENTATION_BOTTOM = 'bottom';

export default class Divider extends Component {
  static propTypes = {
    shadowOrientation: PropTypes.oneOf([SHADOW_ORIENTATION_TOP, SHADOW_ORIENTATION_BOTTOM]),
  };

  static defaultProps = {
    shadowOrientation: SHADOW_ORIENTATION_TOP,
  }

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const { shadowOrientation } = this.props;
    return (
      <DividerWrapper>
        <Defs>
          <LinearGradient id="shadow" x1="0%" y1="0%" x2="0%" y2="100%">
            <Stop offset="0%" stopColor="#000" stopOpacity={shadowOrientation === SHADOW_ORIENTATION_BOTTOM ? 0 : 0.2} />
            <Stop offset="100%" stopColor="#000" stopOpacity={shadowOrientation === SHADOW_ORIENTATION_BOTTOM ? 0.2 : 0} />
          </LinearGradient>
        </Defs>
        <Rect fill="url(#shadow)" x="0" y="0" width="100%" height="10" />
      </DividerWrapper>
    );
  }
}
