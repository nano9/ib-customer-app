import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Collapsible from 'react-native-collapsible';
import { View } from 'react-native';

import SideBarItem from './SideBarItem';

export const AccordionItemHeader = ({ children }) => children;
export const AccordionItemContent = ({ children }) => children;

export default class AccordionItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      contentCollapsed: true,
    };
  }

  onTriggered = () => {
    this.setState({ contentCollapsed: !this.state.contentCollapsed });
  };

  render() {
    const childProps = {
      height: this.props.height,
      layoutColumn: this.props.layoutColumn,
      withArrow: this.props.withArrow,
      arrowFloatTop: this.props.arrowFloatTop,
      triggerPress: this.props.triggerPress,
      hasShadow: this.props.hasShadow,
      onTriggered: this.onTriggered,
      noSeparator: this.props.noSeparator,
    };
    this.props.children.forEach((child) => {
      if (child.type === AccordionItemHeader) {
        this.header = child;
      } else if (child.type === AccordionItemContent) {
        this.content = child;
      }
    });
    return (
      <View>
        <SideBarItem {...childProps}>{this.header}</SideBarItem>
        <Collapsible collapsed={this.state.contentCollapsed}>
          {this.content}
        </Collapsible>
      </View>
    );
  }
}

AccordionItem.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
  height: PropTypes.number,
  triggerPress: PropTypes.bool,
  withArrow: PropTypes.bool,
  arrowFloatTop: PropTypes.bool,
  layoutColumn: PropTypes.bool,
  hasShadow: PropTypes.bool,
  noSeparator: PropTypes.bool,
};

AccordionItem.defaultProps = {
  height: 63,
  triggerPress: false,
  withArrow: false,
  arrowFloatTop: false,
  layoutColumn: false,
  hasShadow: false,
  noSeparator: false,
};
