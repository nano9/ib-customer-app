import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Link } from 'react-router-native';
import { View, TouchableHighlight } from 'react-native';

import SideBarSeparator from './SideBarSeparator';
import ArrowVerticalIcon from './Icons/ArrowVerticalIcon';

const Container = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
  height: ${({ height }) => height}px;
`;

const ChildWrapper = styled.View`
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding-left: 24px;
`;

const ChildContainer = styled.View`
  flex: 0.88;
  display: flex;
  flex-direction: ${({ layoutColumn }) => (layoutColumn ? 'column' : 'row')};
  justify-content: ${({ layoutColumn }) => (layoutColumn ? 'center' : 'flex-start')};
  align-items: ${({ layoutColumn }) => (layoutColumn ? 'flex-start' : 'center')};
`;

const NotificationLabel = styled.Text`
  width: 17px;
  height: 17px;
  border-radius: 9px;
  background: #006DF0;
  margin-left: -8px;
  margin-top: -14px;
  text-align: center;
  line-height: 17px;
  font-family: Roboto;
  font-size: 13px;
  color: #F2F2F2;
`;

const ArrowVerticalContainer = styled.View`
  flex: 0.12;
  height: 100%;
  padding-top: ${({ arrowFloatTop }) => (arrowFloatTop ? '23' : '6')}px;
  padding-left: 3px;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: ${({ arrowFloatTop }) => (arrowFloatTop ? 'flex-start' : 'center')};
  padding-right: 24px;
  padding-left: 10px;
`;

function SideBarItemChild(props) {
  const { children } = props;
  const triggeredChildren = React.Children.map(children, child =>
    React.cloneElement(child, { triggered: props.triggered }),
  );
  return (
    <ChildWrapper>
      <ChildContainer layoutColumn={props.layoutColumn}>
        {triggeredChildren}
        {props.notifications > 0 &&
          <NotificationLabel>{props.notifications}</NotificationLabel>
        }
      </ChildContainer>
      {props.withArrow &&
        <ArrowVerticalContainer arrowFloatTop={props.arrowFloatTop}>
          <ArrowVerticalIcon down={!props.triggered} />
        </ArrowVerticalContainer>
      }
    </ChildWrapper>
  );
}

SideBarItemChild.propTypes = {
  children: PropTypes.node.isRequired,
  layoutColumn: PropTypes.bool,
  withArrow: PropTypes.bool,
  arrowFloatTop: PropTypes.bool,
  notifications: PropTypes.number,
  triggered: PropTypes.bool,
};

SideBarItemChild.defaultProps = {
  layoutColumn: false,
  withArrow: false,
  arrowFloatTop: false,
  notifications: 0,
  triggered: false,
};

export default class SideBarItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      triggered: false,
    };
  }

  onTriggerPress = () => {
    if (this.props.onTriggered) {
      this.props.onTriggered(!this.state.triggered);
    }
    this.setState({ triggered: !this.state.triggered });
  };

  render() {
    const {
      height,
      link,
      hasShadow,
      children,
      triggerPress,
      noSeparator,
    } = this.props;

    const childProps = {
      triggered: this.state.triggered,
      ...this.props,
    };

    return (
      <Container height={height}>
        {link &&
          <Link to={link}>
            <View>
              <SideBarItemChild {...childProps}>{children}</SideBarItemChild>
            </View>
          </Link>
        }
        {!link && triggerPress &&
          <TouchableHighlight onPress={this.onTriggerPress}>
            <View>
              <SideBarItemChild {...childProps}>{children}</SideBarItemChild>
            </View>
          </TouchableHighlight>
        }
        {!link && !triggerPress &&
          <SideBarItemChild {...childProps}>{children}</SideBarItemChild>
        }
        {!noSeparator &&
          <SideBarSeparator shadow={hasShadow} />
        }
      </Container>
    );
  }
}

SideBarItem.propTypes = {
  children: PropTypes.node.isRequired,
  hasShadow: PropTypes.bool,
  height: PropTypes.number,
  link: PropTypes.string,
  triggerPress: PropTypes.bool,
  onTriggered: PropTypes.func,
  noSeparator: PropTypes.bool,
};

SideBarItem.defaultProps = {
  hasShadow: false,
  height: 63,
  link: null,
  triggerPress: false,
  onTriggered: undefined,
  noSeparator: false,
};
