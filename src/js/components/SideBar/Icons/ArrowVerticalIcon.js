import React from 'react';
import Svg, { Path } from 'react-native-svg';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

const Wrapper = styled.View`
  width: 10px;
  height: 18px;
  transform: rotate(${props => (props.down ? '-90deg' : '90deg')});
`;

export default function ArrowVerticalIcon({ down }) {
  return (
    <Wrapper down={down}>
      <Svg width="18" height="18" viewBox="0 2 18 10">
        <Path fill="#B9BBBE" fill-rule="evenodd" d="M 8.63378 0L 10 1.3821L 2.74504 8.8778L 1.34719 7.37124L 0 8.73404L 8.63378 0ZM 0 8.73404L 8.63378 18L 10 16.6969L 2.74504 8.8778L 1.45731 10.2083L 0 8.73404Z" />
        <Path fill="#B9BBBE" d="M 1.34719 7.37124L 0 8.73404L 1.45731 10.2083L 2.74504 8.8778L 1.34719 7.37124Z" />
      </Svg>
    </Wrapper>
  );
}

ArrowVerticalIcon.propTypes = {
  down: PropTypes.bool,
};

ArrowVerticalIcon.defaultProps = {
  down: false,
};
