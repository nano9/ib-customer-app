import React from 'react';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';
import Svg, { Path, LinearGradient, Stop, Defs, Rect } from 'react-native-svg';

const Container = styled.View`
  width: 18px;
  height: 18px;
  border: ${({ checked }) => (checked ? 'none' : '1px solid #73787D')};
  border-radius: 3px;
  margin-right: 8px;
  padding-bottom: 2px;
`;

const IconWrapper = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default function CheckBoxIcon({ checked }) {
  return (
    <Container checked={checked}>
      {checked &&
        <Svg width="18" height="18" viewBox="0 0 18 18">
          <Defs>
            <LinearGradient id="bg" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse">
              <Stop offset="0.0276243%" stopColor="#006DF0" />
              <Stop offset="100%" stopColor="#328DFA" />
            </LinearGradient>
          </Defs>
          <Rect fill="url(#bg)" width="18" height="18" rx="3" ry="3" />
        </Svg>
      }
      {checked &&
        <IconWrapper>
          <Svg width="14" height="11" viewBox="0 0 14 11">
            <Path fill="#FFFFFF" d="M 1.40221 3.61968C 1.62903 3.39369 1.99679 3.39369 2.22361 3.61968L 5.94096 7.32345C 6.16778 7.54944 6.16778 7.91585 5.94096 8.14184L 4.70886 9.36943C 4.48204 9.59542 4.11429 9.59542 3.88747 9.36943L 0.170117 5.66567C -0.0567056 5.43967 -0.0567056 5.07327 0.170117 4.84727L 1.40221 3.61968Z" />
            <Path fill="#FFFFFF" d="M 13.676 1.39709C 13.9029 1.62308 13.9029 1.98949 13.676 2.21548L 5.64705 10.2151C 5.42023 10.4411 5.05247 10.4411 4.82565 10.2151L 3.59356 8.98753C 3.36673 8.76154 3.36673 8.39513 3.59356 8.16914L 11.6225 0.169495C 11.8494 -0.0564983 12.2171 -0.0564983 12.4439 0.169495L 13.676 1.39709Z" />
          </Svg>
        </IconWrapper>
      }
    </Container>
  );
}

CheckBoxIcon.propTypes = {
  checked: PropTypes.bool,
};

CheckBoxIcon.defaultProps = {
  checked: false,
};

