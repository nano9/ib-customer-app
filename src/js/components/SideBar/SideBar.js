import React, { PureComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import call from 'react-native-phone-call';

import NestedScrollView from 'components/NestedScrollView';

import SideBarItem from './SideBarItem';
import SideBarSeparator from './SideBarSeparator';
import AccordionItem, { AccordionItemHeader, AccordionItemContent } from './AccordionItem';
import CitySelectItem from './CitySelectItem';
import CellphoneIcon from './Icons/CellphoneIcon';

// Do not use ScrollView here
// NestedScrollView refuses to work inside ScrollView on older devices.
// Tested on android 4.4 api 19
// height: 100% is required to make scroll work at all, otherwise it will overflow it's container
const Container = styled(NestedScrollView)`
  height: 100%;
`;

const Row = styled.View`
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
  align-items: center;
`;

const SideBarText = styled.Text`
  color: #aeb7bf;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
`;

const PhonesWrapper = styled.View`
  height: 74px;
  width: 249px;
`;

const PhonesContainer = styled.View`
  padding-left: 30px;
`;

const PhoneLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 14px;
  font-size: 16px;
  color: #167CF6;
  margin-left: 8px;
  margin-top: 9px;
  margin-bottom: 13px;
`;

export default class SideBar extends PureComponent {
  callPhoneNumber = () => {
    // TODO: Show Notification on error
    call({
      number: '380970007040',
      prompt: true,
    });
  };

  render() {
    const { city, allCities } = this.props;
    return (
      <Container>
        <CitySelectItem city={city} allCities={allCities} noSeparator />
        <SideBarSeparator shadow />
        <SideBarItem link="/profile/notifications" notifications={3}>
          <SideBarText numberOfLines={1}>Уведомления</SideBarText>
        </SideBarItem>
        <AccordionItem withArrow triggerPress>
          <AccordionItemHeader>
            <SideBarText numberOfLines={1}>Наши телефоны</SideBarText>
          </AccordionItemHeader>
          <AccordionItemContent>
            <PhonesWrapper>
              <PhonesContainer>
                <TouchableOpacity onPress={this.callPhoneNumber}>
                  <Row>
                    <CellphoneIcon />
                    <PhoneLabel>+38-097-000-70-40</PhoneLabel>
                  </Row>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.callPhoneNumber}>
                  <Row>
                    <CellphoneIcon />
                    <PhoneLabel>+38-097-000-70-40</PhoneLabel>
                  </Row>
                </TouchableOpacity>
              </PhonesContainer>
              <SideBarSeparator />
            </PhonesWrapper>
          </AccordionItemContent>
        </AccordionItem>
        <SideBarItem link="/cashboxes">
          <SideBarText numberOfLines={1}>Кассы в городе</SideBarText>
        </SideBarItem>
        <SideBarItem link="/onlineAdviser">
          <SideBarText numberOfLines={1}>Online консультант</SideBarText>
        </SideBarItem>
        <SideBarItem link="/pc">
          <SideBarText numberOfLines={1}>Личный кабинет</SideBarText>
        </SideBarItem>
      </Container>
    );
  }
}

const cityShape = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
};

SideBar.propTypes = {
  city: PropTypes.shape(cityShape).isRequired,
  allCities: PropTypes.arrayOf(PropTypes.shape(cityShape)).isRequired,
};
