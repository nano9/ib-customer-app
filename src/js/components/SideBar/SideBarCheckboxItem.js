import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import CheckBoxIcon from './Icons/CheckBoxIcon';

const Container = styled.View`
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
  align-items: center;
`;

const Label = styled.Text`
  color: #666;
  margin-left: 8px;
  font-size: 14px;
`;

export default function SideBarCheckboxItem({ label, triggered }) {
  return (
    <Container>
      <CheckBoxIcon checked={triggered} />
      <Label numberOfLines={1}>{label}</Label>
    </Container>
  );
}

SideBarCheckboxItem.propTypes = {
  label: PropTypes.string.isRequired,
  triggered: PropTypes.bool.isRequired,
};
