import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { View } from 'react-native';

import NestedScrollView from 'components/NestedScrollView';

import SideBarSeparator from './SideBarSeparator';
import AccordionItem, { AccordionItemHeader, AccordionItemContent } from './AccordionItem';
import LocationIcon from './Icons/LocationIcon';
import ArrowVerticalIcon from './Icons/ArrowVerticalIcon';

const Row = styled.View`
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
  align-items: center;
`;

const CityHeaderLabel = styled.Text`
  color: #FAE101;
  flex: 0.88;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
`;

const SelectLabel = styled.Text`
  color: #666;
  margin-left: 8px;
  font-size: 14px;
`;

const Content = styled(NestedScrollView)`
  height: 250px;
  width: 100%;
`;

const CityContentLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 19px;
  font-size: 16px;
  text-align: center;
  color: #767676;
  margin-top: 10px;
  margin-bottom: 10px;
`;

const ArrowContainer = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-bottom: ${({ down }) => (down ? '14px' : '0px')};
  padding-top: ${({ down }) => (down ? '5px' : '14px')};
`;

const SeparatorContainer = styled.View`
  width: 249px;
`;


export default class CitySelectItem extends PureComponent {
  renderCities = (allCities) => {
    if (allCities) {
      return (
        <AccordionItemContent>
          <SeparatorContainer>
            <SideBarSeparator />
          </SeparatorContainer>
          <ArrowContainer>
            <ArrowVerticalIcon />
          </ArrowContainer>
          <Content>
            {allCities.map(city => (
              <CityContentLabel key={city.name}>{city.name}</CityContentLabel>
            ))}
          </Content>
          <ArrowContainer down>
            <ArrowVerticalIcon down />
          </ArrowContainer>
        </AccordionItemContent>
      );
    }
    return null;
  };

  render() {
    const { city, allCities } = this.props;
    return (
      <View>
        <AccordionItem
          height={80}
          layoutColumn
          withArrow
          arrowFloatTop
          triggerPress
          noSeparator
        >
          <AccordionItemHeader>
            <Row>
              <CityHeaderLabel numberOfLines={1}>{city.name}</CityHeaderLabel>
            </Row>
            <Row>
              <LocationIcon />
              <SelectLabel>Выберите город</SelectLabel>
            </Row>
          </AccordionItemHeader>
          {this.renderCities(allCities)}
        </AccordionItem>
      </View>
    );
  }
}

const cityShape = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
};

CitySelectItem.propTypes = {
  city: PropTypes.shape(cityShape).isRequired,
  allCities: PropTypes.arrayOf(PropTypes.shape(cityShape)),
};

CitySelectItem.defaultProps = {
  allCities: undefined,
};
