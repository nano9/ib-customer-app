import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import Svg, {
  Rect,
  Defs,
  LinearGradient,
  Stop,
} from 'react-native-svg';

const DividerWrapper = styled(Svg)`
  width: 100%;
  height: 8px;
  border: 1px solid red;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  align-self: flex-end;
`;

export default class SideBarSeparator extends PureComponent {
  render() {
    const { shadow } = this.props;
    return (
      <DividerWrapper>
        <Defs>
          <LinearGradient id="highlight" x1="0%" y1="100%" x2="0%" y2="0%">
            <Stop offset="0%" stopColor="#000" stopOpacity={0} />
            <Stop offset="100%" stopColor="#fff" stopOpacity={0.1} />
          </LinearGradient>
          <LinearGradient id="shadow" x1="0%" y1="100%" x2="0%" y2="0%">
            <Stop offset="0%" stopColor="#000" stopOpacity={0} />
            <Stop offset="100%" stopColor="#000" stopOpacity={0.5} />
          </LinearGradient>
        </Defs>
        <Rect fill="#000" x="0" y="0" width="100%" height="1" />
        {!shadow &&
        <Rect fill="url(#highlight)" x="0" y="1" width="100%" height="4" />
        }
        {shadow &&
        <Rect fill="url(#shadow)" x="0" y="0" width="100%" height="8" />
        }
      </DividerWrapper>
    );
  }
}

SideBarSeparator.propTypes = {
  shadow: PropTypes.bool,
};

SideBarSeparator.defaultProps = {
  shadow: false,
};
