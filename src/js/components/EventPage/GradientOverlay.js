import React from 'react';
import styled from 'styled-components/native';
import Svg, { LinearGradient, Stop, Defs, Rect } from 'react-native-svg';

const Wrapper = styled.View`
  width: 100%;
  height: 60px;
  position: absolute;
  bottom: 30;
`;

const ShadowContainer = styled(Svg)`
  width: 100%;
  height: 100%;
`;

export default function GradientOverlay() {
  return (
    <Wrapper>
      <ShadowContainer>
        <Defs>
          <LinearGradient id="grad_overlay" x1="0%" y1="0%" x2="0%" y2="100%">
            <Stop offset="0%" stopColor="#fff" stopOpacity={0} />
            <Stop offset="100%" stopColor="#fff" stopOpacity={1} />
          </LinearGradient>
        </Defs>
        <Rect fill="url(#grad_overlay)" x="0" y="0" width="100%" height="60" />
      </ShadowContainer>
    </Wrapper>
  );
}
