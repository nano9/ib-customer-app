import React from 'react';
import styled from 'styled-components/native';
import Svg, { LinearGradient, Stop, Defs, Rect } from 'react-native-svg';

const Wrapper = styled.View`
  width: 100%;
  height: 14px;
`;

const ShadowContainer = styled(Svg)`
  position: absolute;
  width: 100%;
  height: 100%;
`;

export default function FooterShadow() {
  return (
    <Wrapper>
      <ShadowContainer>
        <Defs>
          <LinearGradient id="shadow" x1="0%" y1="0%" x2="0%" y2="100%">
            <Stop offset="0%" stopColor="#000" stopOpacity={0.2} />
            <Stop offset="100%" stopColor="#000" stopOpacity={0} />
          </LinearGradient>
        </Defs>
        <Rect fill="url(#shadow)" x="0" y="0" width="100%" height="14" />
      </ShadowContainer>
    </Wrapper>
  );
}
