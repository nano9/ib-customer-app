import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Image, View, TouchableHighlight } from 'react-native';
import styled from 'styled-components/native';

import Row from 'components/Layouts/Row';
import ViewWithShadow from 'components/ViewWithShadow';

import CalendarIcon from './Icons/CalendarIcon';
import PlaceIcon from './Icons/PlaceIcon';
import MarkerIcon from './Icons/MarkerIcon';
import TicketIcon from './Icons/TicketIcon';
import HeartIcon from './Icons/HeartIcon';
import GraphIcon from './Icons/GraphIcon';

const Container = styled.View`
  width: 100%;
  padding: 10px;
  padding-right: 15px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
`;

const Cell = styled.View`
  flex: 1;
`;

const RightCell = Cell.extend`
  margin-left: 7px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

const BottomCell = Cell.extend`
  height: 35px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding-left: 7px;
`;

const BottomCellLeft = BottomCell.extend`
  padding-left: 7px;
  justify-content: flex-start;
`;

const CounterText = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #999999;
  margin-left: 7px;
`;

const DateLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #4F4F4F;
  margin-left: 11px;
`;

const RegularLabel = DateLabel.extend`
  margin-left: 10px;
  flex: 1;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #006DF0;
`;

const SeparatorLine = styled.View`
  width: 100%;
  height: 1px;
  background-color: #DEDEDE;
  margin-top: 10px;
  margin-bottom: 10px;
`;

const InfoRow = Row.extend`
  margin-top: 0px;
  margin-bottom: 0px;
`;

const BottomIconContainer = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default function EventPageInfo({ date, image, inFavorites, onAddToFavorites }) {
  return (
    <View>
      <Container>
        <Cell style={{ maxWidth: 150 }}>
          <Row style={{ marginLeft: 5, marginRight: 5, marginBottom: 13 }}>
            <CalendarIcon />
            <DateLabel numberOfLines={1}>{date}</DateLabel>
          </Row>
          <ViewWithShadow
            shadowRadius={5}
            cornerRadius={3}
            color={0x000000}
            colorAlpha={100}
            bgColor={0xffffff}
            bgColorAlpha={0xff}
          >
            <Image source={{ uri: image.size2 }} style={{ height: 200, borderRadius: 3 }} />
          </ViewWithShadow>
        </Cell>
        <RightCell>
          <TitleLabel>The World Famous Glenn Miller Orchestra (Глен Миллер)</TitleLabel>
          <SeparatorLine />
          <InfoRow>
            <PlaceIcon style={{ marginLeft: 2 }} />
            <RegularLabel>Днепропетровская филармония имени Л. Б. Когана</RegularLabel>
          </InfoRow>
          <SeparatorLine />
          <InfoRow>
            <MarkerIcon style={{ marginLeft: 2 }} />
            <RegularLabel>Днепр</RegularLabel>
          </InfoRow>
          <SeparatorLine />
          <InfoRow>
            <BottomCellLeft>
              <TicketIcon />
              <CounterText>356</CounterText>
            </BottomCellLeft>
            <BottomCell>
              <TouchableHighlight onPress={onAddToFavorites} style={{ borderRadius: 5, width: '100%', height: '100%' }} underlayColor="rgba(0,0,0,0.07)">
                <BottomIconContainer>
                  <HeartIcon isActive={inFavorites} />
                </BottomIconContainer>
              </TouchableHighlight>
            </BottomCell>
            <BottomCell>
              <TouchableHighlight onPress={() => {}} style={{ borderRadius: 5, width: '100%', height: '100%' }} underlayColor="rgba(0,0,0,0.07)">
                <BottomIconContainer>
                  <GraphIcon />
                </BottomIconContainer>
              </TouchableHighlight>
            </BottomCell>
          </InfoRow>
        </RightCell>
      </Container>
    </View>
  );
}

EventPageInfo.propTypes = {
  date: PropTypes.string.isRequired,
  image: PropTypes.shape({
    size1: PropTypes.string.isRequired,
    size2: PropTypes.string.isRequired,
  }).isRequired,
  inFavorites: PropTypes.bool.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
};
