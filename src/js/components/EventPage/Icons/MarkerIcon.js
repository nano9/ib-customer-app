import React from 'react';
import Svg, { Path } from 'react-native-svg';

import RoundIconContainer from './RoundIconContainer';

export default function MarkerIcon() {
  return (
    <RoundIconContainer>
      <Svg width="12" height="16" viewBox="0 0 12 16">
        <Path fill="#FFFFFF" d="M 6 0C 2.69158 0 -6.31987e-08 2.59944 -6.31987e-08 5.79457C -6.31987e-08 9.75982 5.36941 15.581 5.59802 15.8269C 5.81275 16.0579 6.18764 16.0575 6.40198 15.8269C 6.63059 15.581 12 9.75982 12 5.79457C 11.9999 2.59944 9.30839 0 6 0ZM 6 8.70998C 4.33545 8.70998 2.98128 7.40213 2.98128 5.79457C 2.98128 4.187 4.33549 2.87919 6 2.87919C 7.66452 2.87919 9.01869 4.18704 9.01869 5.7946C 9.01869 7.40217 7.66452 8.70998 6 8.70998Z" />
      </Svg>
    </RoundIconContainer>
  );
}
