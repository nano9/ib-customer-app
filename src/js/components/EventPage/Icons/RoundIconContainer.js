import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import styled from 'styled-components/native';
import Svg, { LinearGradient, Stop, Defs, Circle } from 'react-native-svg';

const RoundIconWrapper = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default function RoundIconContainer({ children }) {
  return (
    <View style={{ width: 30, height: 30 }}>
      <Svg width="30" height="30" viewBox="0 0 30 30">
        <Defs>
          <LinearGradient id="event_info_icon_bg" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse">
            <Stop offset="0%" stopColor="#006DF0" />
            <Stop offset="100%" stopColor="#328DFA" />
          </LinearGradient>
        </Defs>
        <Circle
          cx="15"
          cy="15"
          r="15"
          fill="url(#event_info_icon_bg)"
          fillOpacity={1}
        />
      </Svg>
      <RoundIconWrapper>{children}</RoundIconWrapper>
    </View>
  );
}

RoundIconContainer.propTypes = {
  children: PropTypes.node.isRequired,
};
