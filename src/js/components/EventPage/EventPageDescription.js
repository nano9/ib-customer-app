import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableHighlight } from 'react-native';
import styled from 'styled-components/native';

import Row from 'components/Layouts/Row';
import GradientOverlay from './GradientOverlay';

const Container = styled.View`
  width: 100%;
  padding: 20px;
  padding-top: 6px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  text-align: center;
  color: #4F4F4F;
  margin-bottom: 12px;
`;

const P = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
`;

const ShowMoreButton = styled.View`
  width: 100%;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ShowMoreLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  text-align: center;
  color: #006DF0;
`;

export default class EventPageDescription extends PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
  };

  state = {
    isShowFull: false,
  };

  toggleSize = () => {
    this.setState({ isShowFull: !this.state.isShowFull });
  }

  renderLessText = text => (
    <View>
      <P numberOfLines={5}>{text}</P>
      <GradientOverlay />
      <TouchableHighlight onPress={this.toggleSize} style={{ width: '100%' }} underlayColor="rgba(0,0,0,0.05)">
        <ShowMoreButton>
          <ShowMoreLabel>Подробнее</ShowMoreLabel>
        </ShowMoreButton>
      </TouchableHighlight>
    </View>
  );

  renderText = (text) => {
    if (this.state.isShowFull) {
      return <P>{text}</P>;
    }
    return this.renderLessText(text);
  }

  render() {
    const { title, text } = this.props;
    return (
      <Container>
        <TitleLabel>{title.toUpperCase()}</TitleLabel>
        { this.renderText(text) }
      </Container>
    );
  }
}
