import React, { Component } from 'react';
import styled from 'styled-components/native';
import { ListView, Modal, TouchableWithoutFeedback, StyleSheet, TouchableHighlight } from 'react-native';
import { Link } from 'react-router-native';
import PropTypes from 'prop-types';

import MainButton from 'components/MainButton';

const Container = styled.View`
  margin-top: 16px;
  margin-left: 8px;
  margin-right: 8px;
  margin-bottom: 12px;
  z-index: 900;
`;

const DropdownContainer = styled.View`
  position: absolute;
  left: 8px;
  right: 8px;
  top: ${props => (props.offsetY || 0)}px;
  flex: 1;
  border: 1px solid #E0E0E0;
  background: white;
  opacity: 0.9;
`;

const ModalLayout = styled.View`
  flex: 1;
  position: absolute;
  left: 0px;
  right: 0px;
  top: 0px;
  bottom: 0px;
`;

const ListItemCointainer = styled.View`
  flex: 1;
  height: 65px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const ListItemsSeparator = styled.View`
  flex: 1;
  height: 1px;
  background: #E0E0E0;
`;

const Label = styled.Text`
  flex: 1;
  text-align: center;
  line-height: 23px;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  color: #4F4F4F;
`;

export default class MainSelectDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      selectedItemIndex: this.props.selectedItemIndex || this.props.options[0].index,
    };
    this.button = null;
    this.buttonFrame = null;
  }

  onPressGetRef = (buttonRef) => {
    if (this.state.isOpen) {
      this.hide();
    } else {
      this.show(buttonRef);
    }
  };

  get selectedIndex() {
    return this.props.selectedItemIndex || this.state.selectedItemIndex;
  }

  get dataSource() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    return ds.cloneWithRows(this.props.options.filter(it => it.index !== this.selectedIndex));
  }

  updatePosition(buttonRef, callback) {
    if (buttonRef && buttonRef.measure) {
      buttonRef.measure((fx, fy, width, height, px, py) => {
        this.buttonFrame = {
          x: px, y: py, w: width, h: height,
        };
        if (callback) {
          callback();
        }
      });
    }
  }

  hide = () => {
    this.setState({ isOpen: false });
    return false;
  };

  show(buttonRef) {
    this.updatePosition(buttonRef, () => {
      this.setState({ isOpen: true });
    });
  }

  renderSeparator = (sectionID, rowID) => {
    const key = `spr_${rowID}`;
    return (
      <ListItemsSeparator key={key} />
    );
  };

  renderRow = (rowData, sectionID, rowID) => {
    const { label, index, path } = rowData;
    const highlighted = rowID === this.selectedIndex;
    const row = (
      <ListItemCointainer highlighted={highlighted}>
        <Label>{label}</Label>
      </ListItemCointainer>
    );

    const preservedProps = {
      key: index,
      accessible: this.props.accessible,
      onPress: () => {
        if (!this.props.selectedItemIndex) {
          this.setState({
            selectedItemIndex: index,
            isOpen: false,
          });
        } else {
          this.setState({
            isOpen: false,
          });
        }
        if (this.props.onItemSelected) {
          this.props.onItemSelected(rowData);
        }
      },
      underlayColor: 'rgba(0,0,0,0.03)',
    };

    if (path) {
      return <Link {...preservedProps} to={path}>{row}</Link>;
    }

    return <TouchableHighlight {...preservedProps}>{row}</TouchableHighlight>;
  };

  render() {
    const styles = StyleSheet.create({
      modal: {
        flex: 1,
        borderWidth: 2,
        borderColor: 'green',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'stretch',
        zIndex: 901,
      },
      modalLayout: {
        flex: 1,
        backgroundColor: '#ddd',
        borderWidth: 2,
        borderColor: 'green',
      },
    });
    const offsetY = this.buttonFrame ? (this.buttonFrame.y + this.buttonFrame.h - 1) : 0;
    const { label = '' } = this.props.options.find(it => it.index === this.selectedIndex) || {};

    return (
      <Container>
        <MainButton
          label={label}
          arrowStyle={this.state.isOpen ? 'down' : 'up'}
          onPressGetRef={this.onPressGetRef}
          disabled={this.props.disabled}
        />
        {this.state.isOpen &&
          <Modal
            style={styles.modal}
            animationType="fade"
            visible
            transparent
            onRequestClose={this.hide}
            supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
          >
            <TouchableWithoutFeedback
              onPress={this.hide}
              accessible
              disabled={!this.state.isOpen}
            >
              <ModalLayout>
                <DropdownContainer offsetY={offsetY}>
                  <ListView
                    scrollEnabled={false}
                    dataSource={this.dataSource}
                    renderRow={this.renderRow}
                    renderSeparator={this.renderSeparator}
                    automaticallyAdjustContentInsets={false}
                    showsVerticalScrollIndicator={false}
                  />
                </DropdownContainer>
              </ModalLayout>
            </TouchableWithoutFeedback>
          </Modal>
        }
      </Container>
    );
  }
}

MainSelectDropdown.propTypes = {
  accessible: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.shape({
    index: PropTypes.any.isRequired,
    label: PropTypes.string.isRequired,
  })).isRequired,
  onItemSelected: PropTypes.func.isRequired,
  selectedItemIndex: PropTypes.any,
  disabled: PropTypes.bool,
};

MainSelectDropdown.defaultProps = {
  accessible: true,
  selectedItemIndex: undefined,
};
