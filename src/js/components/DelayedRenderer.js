/* global requestAnimationFrame */

import React, { PureComponent } from 'react';

export default class DelayedRenderer extends PureComponent {
  state = {
    renderID: 0,
  };

  componentWillUpdate() {
    this.renderID = this.state.renderID + 1;
  }

  renderID = 1;

  render() {
    if (this.renderID !== this.state.renderID) {
      requestAnimationFrame(() => this.setState({ renderID: this.renderID }));
      return false;
    }

    const { component: Component, componentProps } = this.props;
    return <Component {...componentProps} />;
  }
}
