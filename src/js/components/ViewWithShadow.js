import React from 'react';
import styled from 'styled-components/native';
import { Platform } from 'react-native';
import PropTypes from 'prop-types';

import ShadowView from 'components/ShadowView';

//
// TODO ADD IOS SUPPORT
//
const ShadowContainer = styled(ShadowView)`
  position: ${({ absolute }) => (absolute ? 'absolute' : 'relative')};
  top: ${({ top }) => (top !== null ? top : 'auto')};
  left: ${({ left }) => (left !== null ? left : 'auto')};
  right: ${({ right }) => (right !== null ? right : 'auto')};
  bottom: ${({ bottom }) => (bottom !== null ? bottom : 'auto')};
  height: ${({ height }) => height || 'auto'};
  padding: 0;
  margin: 0;
  margin-top: ${({ marginTop }) => marginTop || 0}px;
  ${Platform.OS === 'ios'
    ? `box-shadow: 0px 0px ${({ shadowRadius }) => shadowRadius || 0}px rgba(126, 151, 168, 0.25);`
    : ''}
`;

export default function ViewWithShadow(props) {
  const {
    shadowRadius,
    borderRadius,
    color,
    colorAlpha,
    bgColor,
    height,
    absolutePosition,
    left,
    right,
    top,
    bottom,
    marginTop,
    children,
    style,
  } = props;
  return (
    <ShadowContainer
      shadowRadius={shadowRadius}
      cornerRadius={borderRadius}
      color={color}
      colorAlpha={colorAlpha}
      absolute={absolutePosition}
      left={left}
      right={right}
      top={top}
      bottom={bottom}
      height={height}
      marginTop={marginTop}
      style={style}
    >
      {children}
    </ShadowContainer>
  );
}

ViewWithShadow.propTypes = {
  shadowRadius: PropTypes.number,
  borderRadius: PropTypes.number,
  color: PropTypes.number,
  colorAlpha: PropTypes.number,
  absolutePosition: PropTypes.bool,
  height: PropTypes.number,
  left: PropTypes.oneOfType([PropTypes.number, null]),
  right: PropTypes.oneOfType([PropTypes.number, null]),
  top: PropTypes.oneOfType([PropTypes.number, null]),
  bottom: PropTypes.oneOfType([PropTypes.number, null]),
  children: PropTypes.node,
  marginTop: PropTypes.number,
};

ViewWithShadow.defaultProps = {
  shadowRadius: 0,
  borderRadius: 0,
  color: 0x000000,
  colorAlpha: 0xff,
  absolutePosition: false,
  height: null,
  top: null,
  left: null,
  right: null,
  bottom: null,
  children: null,
  marginTop: 0,
};

