import React, { PureComponent } from 'react';
import {
  Modal,
  View,
  TouchableWithoutFeedback,
  ScrollView,
  Dimensions,
  Animated,
} from 'react-native';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';

import TicketIcon from 'components/Icons/TicketIcon';
import Row from 'components/Layouts/Row';
import ButtonMain from 'components/ButtonMain';
import ViewWithShadow from 'components/ViewWithShadow';

import { screenDimensions } from 'constants';
import { EventTitle } from './EventCard';

const Overlay = styled.View`
  height: 100%;
`;

const Container = styled.View`
  position: relative;
  border-radius: 2px;
  background-color: #FFF;
  height: 100%;
  padding-bottom: 10px;
  margin-bottom: 0px;
  padding-left: 8px;
  padding-right: 8px;
`;

const CityLabel = styled.Text`
  width: 100%;
  font-family: Roboto;
  font-weight: 500;
  font-size: 14px;
  text-align: left;
  line-height: 16px;
  color: #4F4F4F;
  margin-top: 8px;
`;

const SecondaryLabel = CityLabel.extend`
  font-size: ${({ size }) => (size || '13')}px;
  font-weight: ${({ weight }) => (weight || 'normal')};
  line-height: 16px;
  color: ${({ color }) => (color || '#767676')};
  margin-top: ${({ marginTop }) => (marginTop ? `${marginTop}px` : 'auto')};
  padding-left: ${({ paddingLeft }) => (paddingLeft ? `${paddingLeft}px` : 'auto')};
`;

const MinCardContainer = styled.View`
  position: absolute;
  top: 0px;
  left: 2px;
  right: 2px;
  bottom: 0px;
  padding-left: 5px;
  padding-right: 5px;
  padding-bottom: 10px;
`;

export const Image = styled.Image`
  width: 100%;
  height: ${({ height }) => height || 195}px;
  border-radius: 2px;
`;

const WRAPPER_SCALE_ARG = 1.8505;
const CONTENT_SCALE_ARG = 0.5403;

export default class EventCardModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      animated: {
        opacity: new Animated.Value(0),
        wrapperOffsetY: new Animated.Value(0),
        wrapperOffsetX: new Animated.Value(0),
        contentOffsetY: new Animated.Value(0),
        scaleContainerY: new Animated.Value(1),
        scaleContentY: new Animated.Value(1),
        alpha: new Animated.Value(0),
      },
      isExpanded: false,
      isAnimating: false,
    };
  }

  componentDidMount() {
    this.onToggleCard();
  }

  onToggleCard = () => {
    const {
      opacity,
      wrapperOffsetY,
      wrapperOffsetX,
      contentOffsetY,
      scaleContainerY,
      scaleContentY,
    } = this.state.animated;
    const { EVENT_CARD_IMG_HEIGHT } = screenDimensions.get();
    const wrapperMinHeight = EVENT_CARD_IMG_HEIGHT + 31;
    const wrapperMaxHeight = wrapperMinHeight * WRAPPER_SCALE_ARG;

    const duration = 200;
    const staggerDelay = 200;

    const { isExpanded } = this.state;
    const { x, y, align } = this.props;
    const { width, height } = Dimensions.get('window');

    const destX = {
      center: width / 3,
      left: 20,
      right: ((width / 3) * 2) - 20,
    }[align];

    const contentOffsetYFinal = -((wrapperMaxHeight - wrapperMinHeight) / 2) + 8;
    const wrapperOffsetYFinal = ((height - wrapperMinHeight) / 2) - y;

    wrapperOffsetY.setValue(isExpanded ? wrapperOffsetYFinal: 0);
    wrapperOffsetX.setValue(isExpanded ? (destX - x) : 0);
    opacity.setValue(isExpanded ? 0.76 : 0);
    scaleContainerY.setValue(isExpanded ? WRAPPER_SCALE_ARG : 1);
    scaleContentY.setValue(isExpanded ? CONTENT_SCALE_ARG : 1);
    contentOffsetY.setValue(isExpanded ? contentOffsetYFinal : 0);

    const animations = [
      Animated.parallel([
        Animated.spring(
          wrapperOffsetY,
          {
            toValue: isExpanded ? 0 : wrapperOffsetYFinal,
            duration,
            useNativeDriver: true,
          },
        ),
        Animated.spring(
          wrapperOffsetX,
          {
            toValue: isExpanded ? 0 : (destX - x),
            duration,
            useNativeDriver: true,
          },
        ),
        Animated.timing(
          opacity,
          {
            toValue: isExpanded ? 0 : 0.76,
            duration,
            delay: duration,
            useNativeDriver: true,
          },
        ),
      ]),

      Animated.parallel([
        Animated.spring(
          scaleContainerY,
          {
            toValue: isExpanded ? 1 : WRAPPER_SCALE_ARG,
            duration,
            useNativeDriver: true,
          },
        ),
        Animated.spring(
          scaleContentY,
          {
            toValue: isExpanded ? 1 : CONTENT_SCALE_ARG,
            duration,
            useNativeDriver: true,
          },
        ),
        Animated.spring(
          contentOffsetY,
          {
            toValue: isExpanded ? 0 : contentOffsetYFinal,
            duration,
            useNativeDriver: true,
          },
        ),
      ]),
    ];

    Animated.stagger(staggerDelay, isExpanded ? animations.reverse() : animations).start(() => {
      this.setState({ isAnimating: false });
      // if (!this.state.isExpanded) {
      //   if (this.props.onRequestClose) {
      //     this.props.onRequestClose();
      //   }
      // }
    });

    if (this.state.isExpanded) {
      setTimeout(() => {
        this.props.onShowEventCard();
      }, 300);
      setTimeout(() => {
        this.props.onRequestClose();
      }, 400);
    }

    this.setState({
      isExpanded: !this.state.isExpanded,
      isAnimating: true,
    });
  }

  onRequestClose = () => {
    this.onToggleCard();
  }

  render() {
    const {
      title,
      imgUri,
      city,
      priceStarts,
      date,
      freeTicketsAmount,
    } = this.props;
    const { isAnimating, isExpanded } = this.state;

    const {
      contentOffsetY,
      scaleContentY,
      scaleContainerY,
      alpha,
      wrapperOffsetY,
      wrapperOffsetX,
      opacity,
    } = this.state.animated;
    const { width, height } = Dimensions.get('window');
    const { EVENT_CARD_IMG_HEIGHT } = screenDimensions.get();

    const wrapperMinHeight = EVENT_CARD_IMG_HEIGHT + 31; // (31 - 9 + 6 + 10)
    const wrapperMaxHeight = wrapperMinHeight * WRAPPER_SCALE_ARG;
    const cardWidth = width / 3;
    const scrollViewContainerProps = (height < (200 + EVENT_CARD_IMG_HEIGHT))
      ? null
      : {
        contentContainerStyle: {
          width: '100%',
          height: '100%',
        },
      };
    return (
      <Modal
        visible={this.props.visible}
        transparent
        onRequestClose={this.onRequestClose}
        supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
      >
        <ScrollView {...scrollViewContainerProps}>
          <TouchableWithoutFeedback onPress={this.onRequestClose}>
            <View>
              <Animated.View
                style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                  opacity,
                  backgroundColor: '#000',
                  height: '100%',
                }}
              />
              <Overlay>
                <Animated.View
                  style={{
                    // borderColor: 'yellow',
                    // borderWidth: 1,
                    height: wrapperMinHeight,
                    width: cardWidth,
                    opacity: 1,
                    paddingTop: 10,
                    position: 'absolute',
                    left: this.props.x,
                    top: this.props.y,
                    transform: [
                      { translateY: wrapperOffsetY },
                      { translateX: wrapperOffsetX },
                      { scaleY: scaleContainerY },
                    ],
                  }}
                >
                  <ViewWithShadow
                    shadowRadius={3}
                    cornerRadius={3}
                    color={0x000000}
                    colorAlpha={80}
                    bgColor={0xffffff}
                    bgColorAlpha={0xff}
                  >
                    <Container />
                  </ViewWithShadow>

                  <Animated.View
                    style={{
                      position: 'absolute',
                      left: 0,
                      top: 0,
                      right: 0,
                      // borderColor: 'blue',
                      // borderWidth: 1,
                      height: wrapperMaxHeight,
                      transform: [
                        { translateY: contentOffsetY },
                        { scaleY: scaleContentY },
                      ],
                    }}
                  >
                    {/* { isExpanded &&
                     <Animated.View
                     style={{
                     position: 'absolute',
                     left: 18,
                     right: 18,
                     top: EVENT_CARD_IMG_HEIGHT + 10,
                     opacity: alpha,
                     }}
                     >
                     <EventTitle align="left" numberOfLines={2}>{title}</EventTitle>
                     <CityLabel>{city}</CityLabel>
                     <SecondaryLabel marginTop={7}>от {priceStarts} грн</SecondaryLabel>
                     <SecondaryLabel marginTop={4} size={12} weight={500}>{date}</SecondaryLabel>
                     <Row marginTop={7} marginBottom={12}>
                     <TicketIcon />
                     <SecondaryLabel paddingLeft={7} color="#167CF6" weight={500}>{freeTicketsAmount}</SecondaryLabel>
                     </Row>
                     <Row marginTop={0}>
                     <ButtonMain
                     label="Перейти"
                     height={38}
                     arrowDirection="right"
                     onPressGetRef={this.onRequestClose}
                     />
                     </Row>
                     </Animated.View>
                     } */}
                    <MinCardContainer>
                      <Image
                        height={EVENT_CARD_IMG_HEIGHT}
                        source={{ uri: imgUri }}
                      />
                      {!isExpanded &&
                      <EventTitle numberOfLines={1}>{title}</EventTitle>
                      }
                      {isExpanded &&
                      <View>
                        <EventTitle align="left" numberOfLines={2}>{title}</EventTitle>
                        <CityLabel>{city}</CityLabel>
                        <SecondaryLabel marginTop={7}>от {priceStarts} грн</SecondaryLabel>
                        <SecondaryLabel marginTop={4} size={12} weight={500}>{date}</SecondaryLabel>
                        <Row marginTop={7} marginBottom={12}>
                          <TicketIcon />
                          <SecondaryLabel paddingLeft={7} color="#167CF6" weight={500}>{freeTicketsAmount}</SecondaryLabel>
                        </Row>
                        <Row marginTop={0}>
                          <ButtonMain
                            label="Перейти"
                            height={38}
                            arrowDirection="right"
                            onPressGetRef={this.onRequestClose}
                          />
                        </Row>
                      </View>
                      }
                    </MinCardContainer>
                  </Animated.View>
                </Animated.View>
              </Overlay>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </Modal>
    );
  }
}

EventCardModal.propTypes = {
  imgUri: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  priceStarts: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  freeTicketsAmount: PropTypes.number.isRequired,
  onRequestClose: PropTypes.func,
  onShowEventCard: PropTypes.func,
  visible: PropTypes.bool,
  align: PropTypes.oneOf(['left', 'right', 'center']),
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
};

EventCardModal.defaultProps = {
  onRequestClose: undefined,
  onShowEventCard: undefined,
  visible: false,
  align: 'center',
};
