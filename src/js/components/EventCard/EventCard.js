import React, { PureComponent } from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';
import ShadowView from 'components/ShadowView';
import { screenDimensions, EVENT_CARD_MARGIN_BOTTOM } from 'constants';

const Wrapper = styled.View`
  flex: 1;
  height: ${({ height }) => height || 183}px;
  margin: 0px;
  margin-bottom: ${EVENT_CARD_MARGIN_BOTTOM}px;
`;

const Background = styled(ShadowView)`
  flex: 1;
  width: 100%;
  height: 174px;
  margin-top: 9px;
  padding-top: 149px;
`;

const Container = styled.View`
  position: absolute;
  top: 0px;
  left: 2px;
  right: 2px;
  bottom: 0px;
  padding-left: 5px;
  padding-right: 5px;
  padding-bottom: 10px;
`;

export const Image = styled.Image`
  width: 100%;
  height: ${({ height }) => height || 152}px;
  border-radius: 2px;
`;

export const EventTitle = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 15px;
  font-size: 13px;
  text-align: ${({ align }) => align || 'center'};
  color: #006DF0;
  margin-top: 6px;
`;

export default class EventCard extends PureComponent {
  render() {
    const { visible, eventData } = this.props;
    const { name, image } = eventData;
    const { EVENT_CARD_IMG_HEIGHT } = screenDimensions.get();
    return (
      <TouchableWithoutFeedback onPress={this.props.onPress}>
        <Wrapper height={EVENT_CARD_IMG_HEIGHT + 31}>
          {visible &&
            <Background
              shadowRadius={3}
              cornerRadius={3}
              color={0x000000}
              colorAlpha={80}
              bgColor={0xffffff}
              bgColorAlpha={0xff}
            />
          }
          {visible &&
            <Container>
              <Image height={EVENT_CARD_IMG_HEIGHT} source={{ uri: image.size2 }} />
              <EventTitle numberOfLines={1}>{name}</EventTitle>
            </Container>
          }
        </Wrapper>
      </TouchableWithoutFeedback>
    );
  }
}

EventCard.propTypes = {
  visible: PropTypes.bool,
  eventData: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    date: PropTypes.string,
    freeTicketsAmount: PropTypes.number,
    hallName: PropTypes.string,
    hasScheme: PropTypes.bool,
    city: PropTypes.string,
    image: PropTypes.shape({
      size1: PropTypes.string,
      size2: PropTypes.string,
    }),
    priceRangeBounds: PropTypes.shape({
      fromNotice: PropTypes.string,
      hasMultiPrices: PropTypes.bool,
      hasPrices: PropTypes.bool,
      minPrice: PropTypes.string,
    }),
    url: PropTypes.string,
  }).isRequired,
  onPress: PropTypes.func,
};

EventCard.defaultProps = {
  visible: true,
  onPress: undefined,
};
