import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Circle,
} from 'react-native-svg';
import { NavButton } from './styles';

export default class NavButtonGradient extends PureComponent {
  render() {
    const { active, small } = this.props;
    return (
      <NavButton small={small}>
        {active &&
        <Svg width="11" height="11" viewBox="0 0 11 11">
          <Defs>
            <LinearGradient id="paint0_linear" x1="0%" y1="0%" x2="100%" y2="0%">
              <Stop
                offset="0.0276243"
                stopColor="#006DF0"
              />
              <Stop
                offset="1"
                stopColor="#328DFA"
              />
            </LinearGradient>
          </Defs>
          <Circle
            cx="5.5"
            cy="5.5"
            r="5.5"
            fill="url(#paint0_linear)"
          />
        </Svg>
        }
      </NavButton>
    );
  }
}
NavButtonGradient.defaultProps = {
  active: false,
  small: false,
};
NavButtonGradient.propTypes = {
  active: PropTypes.bool,
  small: PropTypes.bool,
};
