import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Dimensions } from 'react-native';
import SliderDisplay from './SliderDisplay';
import ShadowOverlay from './ShadowOverlay';
import NavButtonGradient from './NavButtonGradient';
import {
  SliderContainer,
  ScreensCounter,
  ScreensCounterOuter,
  ScreensCounterText,
  NavContainer,
} from './styles';

export default class BannerSlider extends PureComponent {
  constructor(props) {
    super(props);
    if (props.activeSlide === undefined) {
      this.state = {
        activeSlide: 0,
      };
    } else {
      this.state = {};
    }
  }

  componentWillMount() {
    Dimensions.addEventListener('change', this.dimensionChange);
  }

  componentWillUnmount() {
    Dimensions.removeEventListener('change', this.dimensionChange);
  }

  onScroll = (event) => {
    const { nativeEvent: { contentOffset, layoutMeasurement } } = event;
    if (contentOffset.x % layoutMeasurement.width === 0) {
      const newOffset = (contentOffset.x / layoutMeasurement.width);

      if (!this.props.activeSlide) {
        this.setState(
          ({ activeSlide }) => (newOffset === activeSlide ? {} : { activeSlide: newOffset }),
        );
      }

      if (this.props.onSlideChange) this.props.onSlideChange(newOffset);
    }
  };

  get activeSlide() {
    if (this.props.activeSlide !== undefined) return this.props.activeSlide;
    return this.state.activeSlide;
  }

  scrollViewRef = (view) => {
    this.scrollView = view;
    this.updateScrollPos();
  };

  dimensionChange = () => this.forceUpdate();
  updateScrollPos() {
    if (this.scrollViewWidth && this.scrollView) {
      this.scrollView.scrollTo({ x: this.activeSlide * this.scrollViewWidth, animated: false });
    }
  }

  updatePage = ({ nativeEvent: { layout: { width } } }) => {
    this.scrollViewWidth = width;
    this.updateScrollPos();
  };

  render() {
    const pages = this.props.posters;

    return (
      <SliderContainer>
        <SliderDisplay
          innerRef={this.scrollViewRef}
          onScroll={this.onScroll}
          pages={pages}
          onLayout={this.updatePage}
        />

        <ShadowOverlay />
        <ScreensCounterOuter>
          <ScreensCounter>
            <ScreensCounterText>
              {this.activeSlide + 1}/{this.props.posters.length}
            </ScreensCounterText>
          </ScreensCounter>
        </ScreensCounterOuter>
      </SliderContainer>
    );
  }
}
