import styled from 'styled-components/native';

export const SliderContainer = styled.View`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const ScreensCounterOuter = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: center;
  margin-top: -35px;
`;

export const ScreensCounter = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
  width: 40px;
  height: 20px;
  background: rgba(0, 0, 0, 0.2);
  border: 1px solid rgba(255, 255, 255, 0.7);
  border-radius: 33px;
`;

export const ScreensCounterText = styled.Text`
  font-size: 14px;
  color: #FFFFFF;
  opacity: 0.8;
  line-height: 10px;
  margin-top: 4px;
`;

export const NavContainer = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 25px;
`;

export const NavButton = styled.View`
  width: ${props => (props.small ? '6px' : '11px')};
  height: ${props => (props.small ? '6px' : '11px')};
  border-radius: 6px;
  background-color: #A0A0A0;
  margin-left: 6px;
  margin-right: 6px;
`;

export const PostersShadowOverlay = styled.View`
  position: absolute;
  left: 6;
  right: 6;
  bottom: 21px;
  height: 100px;
  flex: 1;
`;

export const StyledScrollView = styled.ScrollView`
  width: 100%;
`;

export const HorLayout = styled.View`
  flex: 1;
  flex-direction: row;
  margin: 0px;
`;

export const PosterImage = styled.Image`
  flex: ${({ weight = '1' }) => weight};
  width: auto;
  height: ${({ height }) => (height ? `${height}px` : '100%')};
  margin: 1px;
  border-radius: 3px;
`;
