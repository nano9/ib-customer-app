import React, { Component } from 'react';
import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Rect,
} from 'react-native-svg';
import { PostersShadowOverlay } from './styles';

export default class ShadowOverlay extends Component {
  shouldComponentUpdate() {
    return false;
  }
  render() {
    return (
      <PostersShadowOverlay>
        <Svg width="100%" height="100">
          <Defs>
            <LinearGradient id="shadow" x1="0%" y1="0%" x2="0%" y2="100%">
              <Stop offset="0%" stopColor="#000" stopOpacity={0} />
              <Stop offset="100%" stopColor="#000" stopOpacity={0.2} />
            </LinearGradient>
          </Defs>
          <Rect fill="url(#shadow)" x="0" y="0" width="100%" height="100" />
        </Svg>
      </PostersShadowOverlay>
    );
  }
}
