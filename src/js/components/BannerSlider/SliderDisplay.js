import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Dimensions } from 'react-native';
import { screenDimensions } from 'constants';
import { StyledScrollView, HorLayout, PosterImage } from './styles';

function renderPage(posterHeight, { banners }) {
  return [
    <HorLayout key="row1">
      <PosterImage height={posterHeight} weight={1} source={{ uri: banners[0].image }} />
      <PosterImage height={posterHeight} weight={2} source={{ uri: banners[1].image }} />
    </HorLayout>,
    <HorLayout key="row2">
      <PosterImage height={posterHeight} weight={1} source={{ uri: banners[2].image }} />
      <PosterImage height={posterHeight} weight={1} source={{ uri: banners[3].image }} />
      <PosterImage height={posterHeight} weight={1} source={{ uri: banners[4].image }} />
    </HorLayout>,
  ];
}

export default class SliderDisplay extends Component {
  static propTypes = {
    pages: PropTypes.arrayOf(PropTypes.object).isRequired,
    onScroll: PropTypes.func,
    innerRef: PropTypes.func,
    onLayout: PropTypes.func,
  };

  static defaultProps = {
    onScroll: undefined,
    innerRef: undefined,
    onLayout: undefined,
  };

  shouldComponentUpdate({ pages }) {
    return (
      pages.length !== this.props.pages.length ||
      pages.some((v, i) => v.id !== this.props.pages[i].id)
    );
  }

  render() {
    const { pages, onScroll, onLayout, innerRef } = this.props;
    const { width } = Dimensions.get('window');
    const { SLIDER_POSTER_HEIGHT } = screenDimensions.get();
    return (
      <StyledScrollView
        horizontal
        pagingEnabled
        scrollEventThrottle={16}
        onScroll={onScroll}
        onLayout={onLayout}
        innerRef={innerRef}
      >
        {
          pages.map(page => (
            <View key={page.id} style={{ width }}>
              { renderPage(SLIDER_POSTER_HEIGHT, page) }
            </View>
          ))
        }
      </StyledScrollView>
    );
  }
}
