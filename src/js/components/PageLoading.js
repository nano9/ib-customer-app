import React from 'react';
import { ActivityIndicator } from 'react-native';

import styled from 'styled-components/native';

const LoadingContainer = styled.View`
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default function PageLoading() {
  return (
    <LoadingContainer>
      <ActivityIndicator size="large" color="#006DF0" />
    </LoadingContainer>
  );
}
