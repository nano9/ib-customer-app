import styled from 'styled-components/native';

export const CardItem = styled.View`
  flex-direction: row;
  height: 60px;
  padding-left: 19px;
  padding-right: 0;
  align-items: center;
  justify-content: space-between;
`;

export const CardTextContainer = styled.View`
  flex: 1;
  height: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const CardText = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  line-height: 32px;
  font-size: 18px;
  text-align: center;
  color: #767676;
`;

export const VisaImage = styled.Image`
  width: 39px;
  height: 23px;
`;

export const MasterCardImage = styled.Image`
  width: 33px;
  height: 27px;
`;
