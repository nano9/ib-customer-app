import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Platform, TouchableHighlight } from 'react-native';
import styled from 'styled-components/native';
import { Svg, Use, Defs, Path } from 'react-native-svg';

import ShadowView from 'components/ShadowView';

const TopNavBarWrapper = styled.View`
  height: 80px;
  width: 100%;
  z-index: 500;
  elevation: 500;
`;

const Shadow = styled(ShadowView)`
  ${Platform.OS === 'ios' ? 'box-shadow: 0px 0px 7px rgba(126, 151, 168, 0.25); margin: 7px;' : ''}
  position: absolute;
  top: -8px;
  left: -7px;
  right: -7px;
`;

const TopNavBarContext = styled.View`
  flex-direction: row;
  align-items: center;
  height: 80px;
  width: 100%;
  padding: 0;
  padding-right: 30px;
`;

const Label = styled.Text`
  font-family: Roboto;
  font-size: 20px;
  color: #4F4F4F;
  margin-left: 9px;
  margin-right: 39px;
`;

const BackButton = styled(Svg)`
  margin-left: 20px;
`;

const BackButtonContainer = styled.View`
  height: 100%;
  width: 54px;
  display: flex;
  justify-content: center;
  align-items: flex-end;
  padding-right: 13px;
`;

export default class TopNavBar extends PureComponent {
  static propTypes = {
    goBack: PropTypes.func,
    label: PropTypes.string.isRequired,
  };

  static defaultProps = {
    goBack: undefined,
  };

  render() {
    const labelStyles = !this.props.goBack ? { marginLeft: 20 } : null;
    return (
      <TopNavBarWrapper>
        <Shadow shadowRadius={7} colorAlpha={64} bgColor={0xffffff} bgColorAlpha={255}>
          <TopNavBarContext>
            {this.props.goBack &&
              <TouchableHighlight onPress={this.props.goBack} underlayColor="rgba(0,0,0,0.05)">
                <BackButtonContainer>
                  <BackButton width="12" height="20" viewBox="0 0 12 20">
                    <Use href="#path0_fill" fill="#4F4F4F" />
                    <Use href="#path1_fill" fill="#4F4F4F" />
                    <Defs>
                      <Path id="path0_fill" d="M 0 9.70448L 1.61663 8.19027L 12 18.5522L 10.3605 20L 0 9.70448Z" />
                      <Path id="path1_fill" d="M 12 1.53567L 10.3605 0L 0 9.70448L 1.74877 11.3425L 12 1.53567Z" />
                    </Defs>
                  </BackButton>
                </BackButtonContainer>
              </TouchableHighlight>
            }
            <Label style={labelStyles} numberOfLines={1}>
              {this.props.label}
            </Label>
          </TopNavBarContext>
        </Shadow>
      </TopNavBarWrapper>
    );
  }
}
