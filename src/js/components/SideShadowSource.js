import React from 'react';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';
import Svg, { LinearGradient, Stop, Defs, Rect } from 'react-native-svg';

const Wrapper = styled.View`
  width: 100%;
  height: 10px;
  background-color: ${({ bgColor }) => bgColor || 'transparent'};
`;

const ShadowContainer = styled(Svg)`
  position: absolute;
  width: 100%;
  height: 100%;
`;


export const SHADOW_GOES_FROM_BOTTOM = 'from-bottom';
export const SHADOW_GOES_FROM_TOP = 'from-top';

export default function SideShadowSource({ goes, bgColor }) {
  return (
    <Wrapper bgColor={bgColor}>
      <ShadowContainer>
        <Defs>
          <LinearGradient id="shadow" x1="0%" y1="0%" x2="0%" y2="100%">
            <Stop offset="0%" stopColor="#000" stopOpacity={goes === SHADOW_GOES_FROM_BOTTOM ? 0 : 0.18} />
            <Stop offset="100%" stopColor="#000" stopOpacity={goes === SHADOW_GOES_FROM_BOTTOM ? 0.18 : 0} />
          </LinearGradient>
        </Defs>
        <Rect fill="url(#shadow)" x="0" y="0" width="100%" height="10" />
      </ShadowContainer>
    </Wrapper>
  );
}

SideShadowSource.propTypes = {
  goes: PropTypes.oneOf([SHADOW_GOES_FROM_BOTTOM, SHADOW_GOES_FROM_TOP]),
  bgColor: PropTypes.string,
};

SideShadowSource.defaultProps = {
  goes: SHADOW_GOES_FROM_TOP,
  bgColor: 'transparent',
};
