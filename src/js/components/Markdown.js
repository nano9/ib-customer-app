import React, { Component } from 'react';
import styled from 'styled-components/native';

export const P = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  color: #767676;
  padding: 0px;
  padding-bottom: ${({ withPadding }) => withPadding ? 15 : 0}px;
`;

export const Span = P.extend`
  color: #167CF6;
`;

export const B = P.extend`
  font-weight: 500;
  color: #4F4F4F;
`;

export const H1 = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  text-align: center;
  color: #006DF0;
  padding-bottom: ${({ withPadding }) => withPadding ? 22 : 0}px;
`;

export const H2 = H1.extend`
  color: #4F4F4F;
  text-align: left;
  padding-bottom: ${({ withPadding }) => withPadding ? 8 : 0}px;
`;
