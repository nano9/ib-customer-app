import React from 'react';
import { Route } from 'react-router-native';

import DelayedRenderer from 'components/DelayedRenderer';

export default function DelayedRoute({ component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props => (
        <DelayedRenderer component={component} componentProps={props} />
      )}
    />
  );
}
