import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight } from 'react-native';
import { Svg, Use, Defs, Path } from 'react-native-svg';
import styled from 'styled-components/native';

import SideShadowSource from 'components/SideShadowSource';


const Wrapper = styled.View`
  width: 100%;
  padding: 0px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
  padding-bottom: 20px;
`;

const CoverImageContainer = styled.View`
  width: 100%;
  height: 186px;
`;

const CoverImage = styled.Image`
  width: 100%;
  height: 100%;
`;

const CoverOverlay = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.14);
`;

const CoverLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  text-align: center;
  color: #FFFFFF;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.5);
  position: absolute;
  left: 35;
  right: 35;
  bottom: 18;
`;

const BackButtonContainer = styled.View`
  position: absolute;
  left: 0px;
  top: 0px;
  padding: 0px;
  display: flex;
`;

const AddressTitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  color: #4F4F4F;
  margin-top: 10px;
  margin-left: 20px;
  margin-right: 20px;
  margin-bottom: 0px;
`;

const B = AddressTitleLabel.extend`
  font-weight: 500;
`;

const AddressDescriptionLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  color: #4F4F4F;
  margin-left: 20px;
  margin-right: 20px;
  margin-top: 12px;
`;

export default function Header({ goBack, image, title, address, description }) {
  return (
    <Wrapper>
      <CoverImageContainer>
        <CoverImage source={{ uri: image }} />
        <CoverOverlay />
        <CoverLabel>{title}</CoverLabel>
        <BackButtonContainer>
          <TouchableHighlight onPress={goBack} style={{ flex: 1, padding: 20 }} underlayColor="rgba(0,0,0,0.05)">
            <Svg width="12" height="20" viewBox="0 0 12 20">
              <Use href="#path0_fill" fill="#FFFFFF" />
              <Use href="#path1_fill" fill="#FFFFFF" />
              <Defs>
                <Path id="path0_fill" d="M 0 9.70448L 1.61663 8.19027L 12 18.5522L 10.3605 20L 0 9.70448Z" />
                <Path id="path1_fill" d="M 12 1.53567L 10.3605 0L 0 9.70448L 1.74877 11.3425L 12 1.53567Z" />
              </Defs>
            </Svg>
          </TouchableHighlight>
        </BackButtonContainer>
      </CoverImageContainer>
      <SideShadowSource goes="from-top" bgColor="#fff" />
      <AddressTitleLabel><B>Адресс:</B> {address}</AddressTitleLabel>
      <AddressDescriptionLabel>{description}</AddressDescriptionLabel>
    </Wrapper>
  );
}

Header.propTypes = {
  goBack: PropTypes.func.isRequired,
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};
