import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import Row from 'components/Layouts/Row';
import EventCard from 'components/EventCard';

const Wrapper = styled.View`
  width: 100%;
  padding: 0px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
  padding-bottom: 20px;
`;

const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #4F4F4F;
  margin-top: 0px;
  margin-left: 20px;
  margin-right: 20px;
  margin-bottom: 0px;
`;

const ContentWrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-left: 5px;
  padding-right: 5px;
  padding-top: 12px;
  padding-bottom: 40px;
`;

const EmptyView = styled.View`
  flex: ${({ flex }) => flex || 1};
`;

export default class Events extends PureComponent {
  renderList = (events) => {
    let p = 0;
    const rows = [];
    while (true) {
      const rowData = events.slice(p, p + 3);

      if (rowData.length === 0) {
        break;
      }

      rows.push(<Row key={p}>{ this.renderRow(rowData) }</Row>);
      p += 3;
    }
    return rows;
  }

  renderRow = (rowData) => {
    const row = [];
    rowData.forEach((data) => {
      row.push(<EventCard key={data.id} eventData={data} onPress={undefined} visible />);
    });
    if (row.length < 3) {
      row.push(<EmptyView key="empty" flex={3 - row.length} />);
    }
    return row;
  }

  render() {
    // const events = [
    //   {
    //     id: 11,
    //     name: 'Event',
    //     date: null,
    //     freeTicketsAmount: 1,
    //     hallName: 'hall',
    //     hasScheme: true,
    //     city: 'kharkiv',
    //     image: {
    //       size1: 'http://via.placeholder.com/350x150',
    //       size2: 'http://via.placeholder.com/350x150',
    //     },
    //     priceRangeBounds: null,
    //     url: null,
    //   },
    // ];
    const { events } = this.props;
    return (
      <Wrapper>
        <TitleLabel>Текущие события</TitleLabel>
        <ContentWrapper>
          { this.renderList(events) }
        </ContentWrapper>
      </Wrapper>
    );
  }
}

Events.propTypes = {

};
