import { Dimensions } from 'react-native';

export const IMAGES_ASPECT_RATIO = 0.46933333333;

export const EVENT_CARD_BOTTOM_HEIGHT = 31;
export const EVENT_CARD_MARGIN_BOTTOM = 10;
export const TOUR_CARD_MARGIN_BOTTOM = 0;

export const SCREEN_MODE_PORTRAIT = 'SCREEN_MODE_PORTRAIT';
export const SCREEN_MODE_LANDSCAPE = 'SCREEN_MODE_LANDSCAPE';

let data = {
  ORIENTATION: SCREEN_MODE_PORTRAIT,
  EVENT_CARD_IMG_HEIGHT: 183,
};

function ScreenDimensions() {
  this.update = function update(window) {
    data = Object.freeze({
      WND_WIDTH: window.width,
      WND_HEIGHT: window.height,
      ORIENTATION: window.width > window.height ? SCREEN_MODE_LANDSCAPE : SCREEN_MODE_PORTRAIT,
      EVENT_CARD_IMG_HEIGHT: window.width * 0.46933333333,
      TOUR_CARD_IMG_HEIGHT: 144, // window.width * 0.384,
      SLIDER_POSTER_HEIGHT: window.width * 0.46933333333,
    });
  };
  this.get = function get() {
    return data;
  };
}

export const screenDimensions = new ScreenDimensions();

const window = Dimensions.get('window');
screenDimensions.update(window);

