import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { routerReducer, routerMiddleware, push } from 'react-router-redux';
import thunk from 'redux-thunk';

import { createMemoryHistory } from 'history';

import * as reducers from 'reducers';
import { requestKey, requestCityList } from 'actions/api/utils.js';

import initialState from './initialState.json';


export const history = createMemoryHistory();

/* eslint-disable no-underscore-dangle  */
// eslint-disable-next-line no-undef
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer,
  }),
  initialState,
  composeEnhancers(applyMiddleware(thunk, routerMiddleware(history))),
);
/* eslint-enable */

export function preloadStore() {
  store
    .dispatch(requestKey())
    .then(() => store.dispatch(requestCityList()))
    .then(() => store.dispatch(push('/main')));
}

export default store;
