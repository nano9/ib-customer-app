import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Route, Switch, Redirect } from 'react-router-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import EventDetailsComponent from 'components/EventDetailsComponent';
import EventBuyTicketsWithSchemeContainer from 'containers/EventBuyTickets/EventBuyTicketsWithSchemeContainer';
import EventBuyTicketsSchemelessContainer from 'containers/EventBuyTickets/EventBuyTicketsSchemelessContainer';

import { requestEventDetails, removeEventDetails } from 'actions/api/event';

function mapStateToProps(state, props) {
  return {
    detailed: state.events.details[props.match.params.eventID],
    simple: state.events.byID[props.match.params.eventID],
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    requestEventDetails,
    removeEventDetails,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class BuyTicketsRouter extends Component {
  static propTypes = {
    requestEventDetails: PropTypes.func.isRequired,
    removeEventDetails: PropTypes.func.isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        eventID: PropTypes.string.isRequired,
      }),
    }).isRequired,
    simple: PropTypes.object.isRequired,
    detailed: PropTypes.object.isRequired,
  };

  state = {
    isLoading: true,
  };

  componentWillMount() {
    this.props
      .requestEventDetails(this.props.match.params.eventID)
      .then(() => this.setState({ isLoading: false }));
  }

  componentWillReceiveProps(nextProps) {
    const { eventID: oldEventID } = this.props.match.params;
    const { eventID: newEventID } = nextProps.match.params;
    if (newEventID !== oldEventID) {
      this.setState({ isLoading: true });

      this.props.removeEventDetails(oldEventID);
      this.props
        .requestEventDetails(newEventID)
        .then(() => this.setState({ isLoading: false }));
    }
  }

  componentWillUnmount() {
    this.props.removeEventDetails(this.props.match.params.eventID);
  }

  renderDetails = () =>
    <EventDetailsComponent simple={this.props.simple} detailed={this.props.detailed} />;

  renderBuy = () => (
    this.props.detailed.hasScheme ?
      <EventBuyTicketsWithSchemeContainer eventID={this.props.match.params.eventID} />
      :
      <EventBuyTicketsSchemelessContainer eventID={this.props.match.params.eventID} />
  );

  render() {
    return (
      <View>
        <Text>{this.props.simple.name}</Text>
        <Switch>
          <Route path="/details" render={this.renderDetails} />
          { this.state.isLoading && <Route path="/buy" render={this.renderBuy} /> }
          <Redirect from="/" to="/details" />
        </Switch>
      </View>
    );
  }
}
