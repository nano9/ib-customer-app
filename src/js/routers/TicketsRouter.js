import React from 'react';
import { Switch, Route } from 'react-router-native';

import TicketsPaidContainer from 'containers/Tickets/TicketsPaidContainer';
import TicketsNotPaidContainer from 'containers/Tickets/TicketsNotPaidContainer';
import TicketInfoContainer from 'containers/Tickets/TicketInfoContainer';

export default function TicketsRouter() {
  return (
    <Switch>
      <Route path="/pc/tickets/paid" component={TicketsPaidContainer} />
      <Route path="/pc/tickets/info" component={TicketInfoContainer} />
      <Route path="/" component={TicketsNotPaidContainer} />
    </Switch>
  );
}
