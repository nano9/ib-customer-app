import React from 'react';
import { Switch, Route } from 'react-router-native';

import MenuComponent from 'containers/PersonalCabinet/MenuContainer';
import OrdersHistoryContainer from 'containers/PersonalCabinet/OrdersHistoryContainer';
import EditPersonalInfoContainer from 'containers/PersonalCabinet/EditPersonalInfoContainer';
import PersonalSubscriptionContainer from 'containers/PersonalCabinet/PersonalSubscriptionContainer';
import CardListContainer from 'containers/PersonalCabinet/CardListContainer';

import ChangePasswordContainer from 'containers/Auth/ChangePasswordContainer';

export default function PersonalCabinetRouter() {
  return (
    <Switch>
      <Route path="/pc/history" component={OrdersHistoryContainer} />
      <Route path="/pc/edit" component={EditPersonalInfoContainer} />
      <Route path="/pc/subs" component={PersonalSubscriptionContainer} />
      <Route path="/pc/changePassword" component={ChangePasswordContainer} />
      <Route path="/pc/cardManagement" component={CardListContainer} />
      <Route path="/" component={MenuComponent} />
    </Switch>
  );
}
