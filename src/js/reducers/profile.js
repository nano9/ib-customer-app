import { PROFILE_AUTHORIZED } from 'actions/types';

export default function profile(state = {}, action) {
  switch (action.type) {
    case PROFILE_AUTHORIZED:
      return {
        ...state,
        authorized: true,
      };
    default:
      return state;
  }
}
