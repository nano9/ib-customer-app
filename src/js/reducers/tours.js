import { TOURS_UPDATE, TOURS_CLEAR } from 'actions/types';

export default function tours(state = {}, action) {
  switch (action.type) {
    case TOURS_CLEAR:
      return {
        ...state,
        allTours: [],
        byID: {},
      };
    case TOURS_UPDATE:
      return {
        ...state,
        allTours: state.allTours.concat(action.tours.map(it => it.id)),
        byID: {
          ...state.byID,
          ...action.tours.reduce((acc, val) => ({
            ...acc,
            [val.id]: val,
          }), {}),
        },
      };
    default:
      return state;
  }
}
