import { HALL_PAGE_DATA_SET } from 'actions/types';

export default function hall(state = {}, action) {
  switch (action.type) {
    case HALL_PAGE_DATA_SET:
      return {
        data: action.data,
      };
    default:
      return state;
  }
}
