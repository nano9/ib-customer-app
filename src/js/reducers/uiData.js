import { POSTERS_SET, SHOW_MESSAGE } from 'actions/types';

export default function uiData(state = {}, action) {
  switch (action.type) {
    case POSTERS_SET:
      return {
        ...state,
        posters: action.posters,
      };
    // TODO handle this action. Waiting for visuals from @airomad
    case SHOW_MESSAGE:
      return state;
    default:
      return state;
  }
}
