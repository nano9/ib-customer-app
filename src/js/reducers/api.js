import { API_SET_KEY } from 'actions/types';

export default function api(state = {}, action) {
  switch (action.type) {
    case API_SET_KEY:
      return {
        authKey: action.authKey,
        ...state,
      };
    default:
      return state;
  }
}
