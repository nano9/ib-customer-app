import {
  EVENTS_UPDATE,
  EVENTS_ADD_ALL_EVENTS,
  EVENTS_ADD_TOP_EVENTS,
  EVENTS_CLEAR_ALL_LIST,
  EVENTS_CLEAR_TOP_LIST,
  EVENTS_CLEAR_ALL,
  EVENTS_DETAILS_UPDATE,
  EVENTS_DETAILS_REMOVE,
} from 'actions/types';

export default function events(state = {}, action) {
  switch (action.type) {
    case EVENTS_CLEAR_ALL:
      return {
        ...state,
        byID: {},
        allEvents: [],
        topEvents: [],
      };
    case EVENTS_UPDATE:
      return {
        ...state,
        byID: {
          ...state.byID,
          ...action.events.reduce((acc, val) => ({
            ...acc,
            [val.id]: {
              ...state.byID[val.id],
              ...val,
            },
          }), {}),
        },
      };

    case EVENTS_CLEAR_ALL_LIST:
      return {
        ...state,
        allEvents: [],
      };
    case EVENTS_ADD_ALL_EVENTS:
      return {
        ...state,
        allEvents: [
          ...state.allEvents,
          ...action.events.map(({ id }) => id),
        ],
      };

    case EVENTS_ADD_TOP_EVENTS:
      return {
        ...state,
        topEvents: [
          ...state.topEvents,
          ...action.events.map(({ id }) => id),
        ],
      };
    case EVENTS_CLEAR_TOP_LIST:
      return {
        ...state,
        topEvents: [],
      };

    case EVENTS_DETAILS_UPDATE:
      return {
        ...state,
        details: {
          ...state.details,
          [action.eventDetails.id]: {
            ...action.eventDetails,
            ...state.details[action.eventDetails.id],
          },
        },
      };
    case EVENTS_DETAILS_REMOVE: {
      const { [action.eventID]: toDelete, ...other } = state.details;
      return {
        ...state,
        details: other,
      };
    }
    default:
      return state;
  }
}
