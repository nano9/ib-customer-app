import { CITIES_UPDATE } from 'actions/types';

export default function cities(state = {}, action) {
  switch (action.type) {
    case CITIES_UPDATE:
      return {
        ...state,
        byID: {
          ...state.byID,
          ...action.cities.reduce((acc, val) => ({
            ...acc,
            [val.id]: val,
          }), {}),
        },
        byName: {
          ...state.byName,
          ...action.cities.reduce((acc, val) => ({
            ...acc,
            [val.name]: val,
          }), {}),
        },
      };
    default:
      return state;
  }
}
