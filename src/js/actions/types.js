export const API_SET_KEY = 'api/SetKey';

export const CITIES_UPDATE = 'cities/update';

export const EVENTS_UPDATE = 'events/update';
export const EVENTS_ADD_TOP_EVENTS = 'events/addTopEvent';
export const EVENTS_ADD_ALL_EVENTS = 'events/addAllEvents';
export const EVENTS_CLEAR_TOP_LIST = 'events/clearTopList';
export const EVENTS_CLEAR_ALL_LIST = 'events/clearAllList';
export const EVENTS_CLEAR_ALL = 'events/clearAll';

export const EVENTS_DETAILS_UPDATE = 'events/updateDetails';
export const EVENTS_DETAILS_REMOVE = 'events/removeDetails';

export const TOURS_UPDATE = 'tours/update';
export const TOURS_CLEAR = 'tours/clear';

export const POSTERS_SET = 'uiData/setPosters';
export const SHOW_MESSAGE = 'uiData/message';

export const PROFILE_AUTHORIZED = 'profile/authorize';

export const HALL_PAGE_DATA_SET = 'hallpage/setData';
