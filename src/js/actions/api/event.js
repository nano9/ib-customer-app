import { request } from './utils';
import {
  EVENTS_DETAILS_UPDATE,
  EVENTS_DETAILS_REMOVE,
} from 'actions/types';

export function requestEventBasket(eventID) {
  return request(
    {
      url: 'basket/contents',
      query: {
        eventId: eventID,
      },
    },
    ({ event, ...basket }, dispatch) => {
      // TODO Handle if eventDetails.event.id !== eventID
      dispatch({
        type: EVENTS_DETAILS_UPDATE,
        eventDetails: {
          id: eventID,
          basket,
        },
      });
    },
  );
}

export function requestEventDetails(eventID) {
  return Promise.all(request(
    {
      url: 'event',
      query: {
        eventId: eventID,
      },
    },
    (eventDetails, dispatch) => {
      // TODO Handle if eventDetails.id !== eventID
      dispatch({
        type: EVENTS_DETAILS_UPDATE,
        eventDetails,
      });
    },
  ), requestEventBasket);
}

export function removeEventDetails(eventID) {
  return {
    type: EVENTS_DETAILS_REMOVE,
    eventID,
  };
}
