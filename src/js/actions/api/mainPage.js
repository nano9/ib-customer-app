import { request } from './utils';
import {
  EVENTS_UPDATE,
  EVENTS_ADD_TOP_EVENTS,
  EVENTS_ADD_ALL_EVENTS,
  TOURS_UPDATE,
  TOURS_CLEAR,
  EVENTS_CLEAR_ALL_LIST,
  EVENTS_CLEAR_TOP_LIST,
  POSTERS_SET,
} from 'actions/types';

export function requestEvents({ mode, refresh = false }) {
  return (dispatch, getState) => (
    dispatch(request(
      {
        url: 'index/events',
        query: {
          size: 60,
          cityId: 3,
          offset: refresh ? 0 : getState().events[mode === 'all' ? 'allEvents' : 'topEvents'].length,
          mode,
        },
      },
      (events) => {
        if (!refresh && events.length === 0) return;

        if (refresh) {
          dispatch({ type: mode === 'all' ? EVENTS_CLEAR_ALL_LIST : EVENTS_CLEAR_TOP_LIST });
        }

        dispatch({
          type: EVENTS_UPDATE,
          events,
        });

        dispatch({
          type: mode === 'all' ? EVENTS_ADD_ALL_EVENTS : EVENTS_ADD_TOP_EVENTS,
          events,
        });
      },
    ))
  );
}

export function requestTours() {
  return request(
    {
      url: 'index/tours',
    },
    (result, dispatch) => {
      const tours = result.map(({ events, ...other }) => ({
        events: events.map(it => it.id),
        ...other,
      }));

      dispatch({
        type: EVENTS_UPDATE,
        events: [].concat(...result.map(it => it.events)),
      });

      dispatch({ type: TOURS_CLEAR });

      dispatch({
        type: TOURS_UPDATE,
        tours,
      });
    },
  );
}

export function requestPosters() {
  return request(
    {
      url: 'index/bannerBlocks',
      query: {
        cityId: 3,
      },
    },
    (result, dispatch) => {
      dispatch({ type: POSTERS_SET, posters: result });
    },
  );
}
