/* global fetch */
import { Platform, AsyncStorage } from 'react-native';

import { API_SET_KEY, CITIES_UPDATE, SHOW_MESSAGE } from 'actions/types';

import { API_RECORD_REQUESTS, API_EMULATE_REQUESTS } from 'react-native-dotenv';

const recordRequests = API_RECORD_REQUESTS === 'true';
const emulateRequests = API_EMULATE_REQUESTS === 'true';

export const BASE_URL = 'http://api.internetbilet.com.ua/api2';

export function request({
  url,
  method = 'GET',
  query = {},
  data = undefined,
}, handler) {
  return async (dispatch, getState) => {
    const { api: { authKey = '' } } = getState();

    let result;
    if (emulateRequests) {
      result = JSON.parse(await AsyncStorage.getItem(`@apiUtils/Request/${JSON.stringify({ url, method, query })}`));
    } else {
      const qs =
        Object
          .entries({ authKey, ...query })
          .filter(it => Boolean(it[1]))
          .map(it => `${encodeURIComponent(it[0])}=${encodeURIComponent(it[1])}`)
          .join('&');

      const req = await fetch(`${BASE_URL}/${url}?${qs}`, {
        method,
        body: data && JSON.stringify(data),
      });

      result = await req.json();
      if (recordRequests) {
        await AsyncStorage.setItem(`@apiUtils/Request/${JSON.stringify({ url, method, query })}`, JSON.stringify(result));
      }
    }

    if (result.code === 1) {
      return handler(result.data, dispatch, getState);
    }
    // TODO return error action based on result.code
    return {
      type: SHOW_MESSAGE,
      isError: true,
      message: '',
    };
  };
}

export function requestKey() {
  return request(
    {
      url: 'consumerAuth',
      query: { key: Platform.OS },
    },
    ({ authKey }, dispatch) => {
      dispatch({
        type: API_SET_KEY,
        authKey,
      });
    },
  );
}

export function requestCityList() {
  return request(
    {
      url: 'index/cities',
    },
    (cities, dispatch) => {
      dispatch({
        type: CITIES_UPDATE,
        cities,
      });
    },
  );
}

