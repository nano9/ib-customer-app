import { HALL_PAGE_DATA_SET } from 'actions/types';
import { request } from './utils';

// TODO Change to valid authKey
// THIS USED HERE JUST BECAUSE I TEST IT IN A SEPARATE WAY
export function requestHallData() {
  return request(
    {
      url: 'index/hall',
      query: {
        hallId: 47,
        authKey: 'testib',
      },
    },
    (result, dispatch) => {
      // TODO: handle response codes
      dispatch({ type: HALL_PAGE_DATA_SET, data: result });
    },
  );
}

export default null;
