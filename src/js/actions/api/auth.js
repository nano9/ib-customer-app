import { request } from './utils';
import { PROFILE_AUTHORIZED } from 'actions/types';

export function authorize({ email, password }) {
  return request({
    url: 'index/events',
    method: 'POST',
    data: {
      email,
      pwd: password,
    },
  }, (_, dispatch) => {
    dispatch({ type: PROFILE_AUTHORIZED });
  });
}
