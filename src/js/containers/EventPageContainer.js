import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'react-native';
import styled from 'styled-components/native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import TopNavBar from 'components/TopNavBar';
import Row from 'components/Layouts/Row';
import ViewWithShadow from 'components/ViewWithShadow';
import ButtonMain from 'components/ButtonMain';

import EventPageInfo from 'components/EventPage/EventPageInfo';
import EventPageDescription from 'components/EventPage/EventPageDescription';
import FooterShadow from 'components/EventPage/FooterShadow';

const Wrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #fff;
`;

const ButtonRow = Row.extend`
  height: 49px;
  width: 100%;
`;

const Footer = styled.View`
  width: 100%;
  height: 94px;
  display: flex;
  justify-content: center;
  align-items: stretch;
  padding-bottom: 30px;
  padding-left: 5px;
  padding-right: 5px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class EventPageContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
  }

  state = {
    isEventInFavorites: true,
  }

  // 2282282282282228228282828282
  addToFavoritesToggle = () => {
    this.setState({ isEventInFavorites: !this.state.isEventInFavorites });
  }

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="The World Famous Glenn Miller" key="header" />,
      <ScrollView key="body">
        <Wrapper>
          <EventPageInfo
            date="27 октября 23:00"
            image={{ size1: 'http://via.placeholder.com/120x200', size2: 'http://via.placeholder.com/120x200' }}
            inFavorites={this.state.isEventInFavorites}
            onAddToFavorites={this.addToFavoritesToggle}
          />
          <FooterShadow />
          <EventPageDescription
            title="Глен Миллер"
            text="Уже несколько лет подряд программа Театра Теней TEULIS «Властелины Теней» срывает овации зрителей по всему миру. Невообразимая пластика актеров, их акробатические трюки творят за тонким экраном целые истории. Но в 2017 году артисты готовы завоевать ваши сердца еще более поразительным действием. Встречайте, новое шоу Вечная"
          />
        </Wrapper>
      </ScrollView>,
      <Footer key="footer">
        <ViewWithShadow
          shadowRadius={3}
          cornerRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
        >
          <ButtonRow>
            <ButtonMain
              label="Купить билет"
              height={49}
              onPressGetRef={this.onRequestClose}
            />
          </ButtonRow>
        </ViewWithShadow>
      </Footer>,
    ];
  }
}
