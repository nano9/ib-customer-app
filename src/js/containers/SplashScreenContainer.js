import React from 'react';
import styled from 'styled-components/native';
import { Image, Dimensions } from 'react-native';

import bgImg from 'img/splash-bg.png';
import logoImg from 'img/logo-big.png';

import { preloadStore } from 'store';

const SplashScreenContainer = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
`;

const LogoContainer = styled.View`
  width: 100%;
  position: absolute;
  top: 18%;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const Logo = styled(Image)`
  width: ${({ smallScreen }) => (smallScreen ? '100%' : '397px')};
`;

export default class AppLoadingComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.smallScreen = false;
  }

  componentWillMount() {
    preloadStore();
  }

  componentDidMount() {
    const { width } = Dimensions.get('window');
    this.smallScreen = width < 397;
  }

  render() {
    return (
      <SplashScreenContainer>
        <Image style={{ width: '100%' }} source={bgImg} />
        <LogoContainer>
          <Logo
            smallScreen={this.smallScreen}
            source={logoImg}
          />
        </LogoContainer>
      </SplashScreenContainer>
    );
  }
}
