import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import styled from 'styled-components/native';

import TopNavBar from 'components/TopNavBar';
import Row from 'components/Layouts/Row';
import EventCard from 'components/EventCard';

const Wrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-left: 5px;
  padding-right: 5px;
  padding-top: 12px;
  padding-bottom: 40px;
`;

const EmptyView = styled.View`
  flex: ${({ flex }) => flex || 1};
`;

export default class FavoritesContainer extends Component {
  renderList = (events) => {
    let p = 0;
    const rows = [];
    while (true) {
      const rowData = events.slice(p, p + 3);
      
      if (rowData.length === 0) {
        break;
      }

      rows.push(<Row>{ this.renderRow(rowData) }</Row>);
      p += 3;
    }
    return rows;
  }

  renderRow = (rowData) => {
    const row = [];
    rowData.forEach((data) => {
      row.push(<EventCard eventData={data} onPress={undefined} visible />);
    });
    if (row.length < 3) {
      row.push(<EmptyView flex={3 - row.length} />);
    }
    return row;
  }

  render() {
    const events = [
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
    ]
    return [
      <TopNavBar label="Избранное" key="header" />,
      <ScrollView key="body">
        <Wrapper>
          { this.renderList(events) }
        </Wrapper>
      </ScrollView>,
    ];
  }
}
