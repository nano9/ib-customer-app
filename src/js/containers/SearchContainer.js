import React, { PureComponent } from 'react';
import { ScrollView, Animated } from 'react-native';
import styled from 'styled-components/native';

import { StyledTypedPicker } from 'components/FormComponents';

import SearchTopNavBar, {
  HEADER_TYPE_NORMAL,
  HEADER_TYPE_SMALL,
  INPUT_SEARCH_ROW_TYPE_EMPTY,
  INPUT_SEARCH_ROW_TYPE_FILLED,
} from 'components/Search/SearchTopNavBar';

import SearchControlButton from 'components/Search/SearchControlButton';
import Row from 'components/Layouts/Row';
import EventCard from 'components/EventCard';

const Wrapper = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
  background-color: #fff;
`;

const FilterModalWrapper = styled(ScrollView)`
  position: absolute;
  left: 0px;
  right: 0px;
  top: 0px;
  bottom: 0px;
  padding-top: 53px;
  background-color: rgba(0, 0, 0, 0.5);
`;

const FilterContainer = styled.View`
  display: flex;
  justify-content: center;
  align-items: stretch;
  background-color: #fff;
  border-radius: 3px;
  margin-left: 7px;
  margin-right: 7px;
  margin-top: 0px;
  margin-bottom: 100px;
  padding: 14px;
`;

const BigLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 21px;
  font-size: 20px;
  text-align: center;
  color: rgba(79, 79, 79, 0.8);
  margin-bottom: 0px;
`;

const InputContainer = styled.View`
  height: 40px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  border-radius: 3px;
  border: 1px solid #B2B2B2;
  margin-top: 12px;
  margin-bottom: 3px;
`;

const ControlButtonsContainer = styled.View`
  position: absolute;
  right: 15px;
  bottom: 0px;
  background-color: transparent;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 15px;
`;

const EmptyView = styled.View`
  flex: ${({ flex }) => flex || 1};
`;

export default class SearchContainer extends PureComponent {
  state = {
    headerType: HEADER_TYPE_SMALL,
    inputSearchRowType: INPUT_SEARCH_ROW_TYPE_EMPTY,
    containerOffsetY: new Animated.Value(-22),
    isShowFilterProps: true,
    isShowResults: true,
  };

  toggleHeaderType = () => {
    const headerTypeNow = (this.state.headerType === HEADER_TYPE_NORMAL
      ? HEADER_TYPE_SMALL
      : HEADER_TYPE_NORMAL);
    this.setState({ headerType: headerTypeNow });

    const ANIMATION_DURATION = 150;
    const { containerOffsetY, headerType } = this.state;

    containerOffsetY.setValue(headerType === HEADER_TYPE_NORMAL ? 0 : -22);

    Animated.spring(
      containerOffsetY,
      {
        toValue: headerType === HEADER_TYPE_NORMAL ? -22 : 0,
        duration: ANIMATION_DURATION,
        useNativeDriver: true,
      },
    ).start();

    this.toggleInputSearchRowType();
  }

  toggleInputSearchRowType = () => {
    const inputSearchRowTypeNow = (this.state.inputSearchRowType === INPUT_SEARCH_ROW_TYPE_EMPTY
      ? INPUT_SEARCH_ROW_TYPE_FILLED
      : INPUT_SEARCH_ROW_TYPE_EMPTY);
    this.setState({ inputSearchRowType: inputSearchRowTypeNow });
  }

  toggleFilterProps = () => {
    this.setState({ isShowFilterProps: !this.state.isShowFilterProps });
  }

  submitInputSearchRow = () => {
    // TO DO SOMETHING
    this.toggleInputSearchRowType();
  }

  renderFilter = () => (
    <FilterContainer>
      <BigLabel>Расширенный поиск</BigLabel>
      <InputContainer>
        <StyledTypedPicker
          type="Рубрика"
          items={[
            { label: 'Выберите', value: 'empty' },
            { label: 'Рубрика 1', value: 'v1' },
            { label: 'Рубрика 2', value: 'v2' },
          ]}
        />
      </InputContainer>
      <InputContainer>
        <StyledTypedPicker
          type="Жанр"
          items={[
            { label: 'Выберите', value: 'empty' },
            { label: 'Жанр 1', value: 'v1' },
          ]}
        />
      </InputContainer>
      <InputContainer>
        <StyledTypedPicker
          type="Город"
          items={[
            { label: 'Выберите', value: 'empty' },
            { label: 'Город 1', value: 'v1' },
          ]}
        />
      </InputContainer>
      <InputContainer>
        <StyledTypedPicker
          type="Зал"
          items={[
            { label: 'Выберите', value: 'empty' },
            { label: 'Зал 1', value: 'v1' },
          ]}
        />
      </InputContainer>
      <InputContainer>
        <StyledTypedPicker
          type="Период"
          items={[
            { label: 'Выберите', value: 'empty' },
            { label: 'Период 1', value: 'v1' },
          ]}
        />
      </InputContainer>
    </FilterContainer>
  );

  renderFilterModal = () => (
    <FilterModalWrapper contentContainerStyle={{ paddingBottom: 100 }}>
      { this.renderFilter() }
    </FilterModalWrapper>
  );

  renderSimpleFilter = () => (
    <ScrollView
      style={{ position: 'absolute', top: -15, bottom: 0, left: 0, right: 0 }}
      contentContainerStyle={{ paddingBottom: 100 }}
    >
      { this.renderFilter() }
    </ScrollView>
  );

  // 228228228228228228228228228228228228228228
  renderFakeResults = () => {
    const events = [
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
      {
        id: 1,
        name: 'Event',
        date: null,
        freeTicketsAmount: 1,
        hallName: 'hall',
        hasScheme: true,
        city: 'kharkiv',
        image: {
          size1: 'http://via.placeholder.com/350x150',
          size2: 'http://via.placeholder.com/350x150',
        },
        priceRangeBounds: null,
        url: null,
      },
    ];
    return (
      <ScrollView
        style={{ position: 'absolute', top: -15, bottom: 0, left: 0, right: 0 }}
        contentContainerStyle={{ paddingBottom: 200, paddingTop: 34, paddingLeft: 7, paddingRight: 7 }}
      >
        { this.renderList(events) }
      </ScrollView>
    );
  }

  renderList = (events) => {
    let p = 0;
    const rows = [];
    while (true) {
      const rowData = events.slice(p, p + 3);

      if (rowData.length === 0) {
        break;
      }

      rows.push(<Row key={p.toString()}>{ this.renderRow(rowData) }</Row>);
      p += 3;
    }
    return rows;
  }

  renderRow = (rowData) => {
    const row = [];
    rowData.forEach((data) => {
      row.push(<EventCard key={data.name} eventData={data} onPress={undefined} visible />);
    });
    if (row.length < 3) {
      row.push(<EmptyView key="empty" flex={3 - row.length} />);
    }
    return row;
  }

  render() {
    const { isShowFilterProps, isShowResults } = this.state;
    return (
      <Wrapper>
        <SearchTopNavBar
          headerType={this.state.headerType}
          inputSearchRowType={this.state.inputSearchRowType}
          onFocusInputSearchRow={this.toggleInputSearchRowType}
          onSubmitInputSearchRow={this.submitInputSearchRow}
          headerHeight={(isShowFilterProps || isShowResults) ? 106 : 120}
        />
        <Animated.View style={{ height: '100%', width: '100%', transform: [{ translateY: this.state.containerOffsetY }] }}>
          {/* { this.renderSimpleFilter() } */}
          { this.renderFakeResults() }
          { isShowFilterProps && this.renderFilterModal() }
        </Animated.View>
        <ControlButtonsContainer>
          <SearchControlButton onPress={this.toggleFilterProps} iconType="props" />
          <SearchControlButton onPress={this.toggleHeaderType} />
        </ControlButtonsContainer>
      </Wrapper>
    );
  }
}
