import React, { Component } from 'react';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack, replace } from 'react-router-redux';

import { Svg, Use, Defs, Path } from 'react-native-svg';

import { InputLabel, StyledTextInput, InputError, InputLabelContainer, LockIcon, InputContainer, InputIcon } from 'components/FormComponents';
import TopNavBar from 'components/TopNavBar';
import ViewWithShadow from 'components/ViewWithShadow';
import Row from 'components/Layouts/Row';
import ButtonMain from 'components/ButtonMain';

import PersonalInfoFormComponent from 'components/PersonalInfoFormComponent';

const AuthScrollView = styled.ScrollView`
  flex: 1;
`;

const AuthWrapper = styled.View`
  padding: 20px;
  margin: 0;
`;

const MainButtonWrapper = styled.View`
  margin-top: 8px;
  padding-bottom: 40px;
  padding-left: 5px;
  padding-right: 5px;
`;

const Footer = styled.View`
  height: 100%;
  background-color: white;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const FooterWrapper = styled.TouchableHighlight`
  height: 54px;
`;

const FooterText = styled.Text`
  font-family: Roboto;
  font-weight: 500;
  font-size: 16px;
  color: #4F4F4F;
  opacity: 0.5;
  margin: 12px;
`;

const ButtonRow = Row.extend`
  padding: 0px;
  margin: 0px;
  height: 49px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
    replace,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class RegisterContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
  };

  goToAuth = () => {
    this.props.replace('/auth');
  };

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="Регистрация" key="header" />,
      <AuthScrollView key="context">
        <AuthWrapper>
          <PersonalInfoFormComponent />
          <InputLabelContainer>
            <InputLabel>Пароль</InputLabel>
            <InputError>Пароли не совпадают</InputError>
          </InputLabelContainer>
          <InputContainer>
            <StyledTextInput placeholder="Введите пароль" secureTextEntry underlineColorAndroid="transparent" />
            <InputIcon><LockIcon /></InputIcon>
          </InputContainer>
          <InputContainer>
            <StyledTextInput placeholder="Повторите пароль" secureTextEntry underlineColorAndroid="transparent" />
            <InputIcon><LockIcon /></InputIcon>
          </InputContainer>
        </AuthWrapper>
        <MainButtonWrapper>
          <ViewWithShadow
            shadowRadius={3}
            cornerRadius={3}
            color={0x000000}
            colorAlpha={80}
            bgColor={0xffffff}
            bgColorAlpha={0xff}
          >
            <ButtonRow>
              <ButtonMain
                label="Зарегистрироваться"
                height={49}
                onPressGetRef={this.onRequestClose}
              />
            </ButtonRow>
          </ViewWithShadow>
        </MainButtonWrapper>
      </AuthScrollView>,
      <FooterWrapper key="footer" onPress={this.goToAuth}>
        <Footer>
          <Svg width="20" height="20" viewBox="0 0 20 20">
            <Use href="#path0_fill" transform="translate(8 0)" fill="#A9A9A9" />
            <Use href="#path1_fill" transform="translate(8 0)" fill="#A9A9A9" />
            <Use href="#path2_fill" transform="translate(0 4)" fill="#A9A9A9" />
            <Use href="#path3_fill" transform="translate(0 4)" fill="#A9A9A9" />
            <Defs>
              <Path id="path0_fill" d="M 10.4301 0L 0.792287 0C 0.354717 0 -2.25758e-07 0.33115 -2.25758e-07 0.73965C -2.25758e-07 1.14815 0.354717 1.4793 0.792287 1.4793L 10.2582 1.4793L 10.2582 18.4862L 0.8221 18.4862C 0.38453 18.4862 0.0298123 18.8174 0.0298123 19.2259C 0.0298123 19.6344 0.38453 19.9655 0.8221 19.9655L 10.43 19.9655C 11.2091 19.9655 11.8428 19.3739 11.8428 18.6466L 11.8428 1.31887C 11.8428 0.591651 11.2091 0 10.4301 0Z" />
              <Path id="path1_fill" d="M 10.4486 20L 0.840594 20C 0.393518 20 0.0298123 19.6605 0.0298123 19.2431C 0.0298123 18.8257 0.393518 18.4862 0.840594 18.4862L 10.2582 18.4862L 10.2582 1.51383L 0.810782 1.51383C 0.363705 1.51383 -2.25758e-07 1.17429 -2.25758e-07 0.756915C -2.25758e-07 0.339541 0.363705 0 0.810782 0L 10.4486 0C 11.2378 0 11.8798 0.599386 11.8798 1.33613L 11.8798 18.6639C 11.8798 19.4006 11.2378 20 10.4486 20ZM 0.840594 18.5207C 0.413935 18.5207 0.0668005 18.8448 0.0668005 19.2431C 0.0668005 19.6414 0.413935 19.9655 0.840594 19.9655L 10.4485 19.9655C 11.2173 19.9655 11.8428 19.3816 11.8428 18.6639L 11.8428 1.33613C 11.8428 0.618447 11.2174 0.0345308 10.4486 0.0345308L 0.810782 0.0345308C 0.384123 0.0345308 0.036988 0.358602 0.036988 0.756915C 0.036988 1.15523 0.384123 1.4793 0.810782 1.4793L 10.2952 1.4793L 10.2952 18.5207L 0.840594 18.5207Z" />
              <Path id="path2_fill" d="M -8.46593e-09 4.52516L -8.46593e-09 8.74645C -8.46593e-09 9.80578 0.923151 10.6677 2.05791 10.6677L 7.21936 10.6677L 7.21936 11.3495C 7.21936 11.6212 7.27857 11.8838 7.39535 12.1298C 7.50675 12.3645 7.66525 12.5729 7.86643 12.7491C 8.25114 13.0861 8.75326 13.2717 9.28019 13.2717C 9.792 13.2717 10.2835 13.0931 10.6641 12.769L 16.1994 8.05524C 16.6259 7.69208 16.8705 7.17474 16.8705 6.63589C 16.8705 6.097 16.6259 5.57966 16.1995 5.21646L 10.6642 0.502699C 10.2835 0.178524 9.79207 -2.31835e-08 9.28026 -2.31835e-08C 8.75326 -2.31835e-08 8.25118 0.185569 7.86646 0.52252C 7.66529 0.698696 7.50679 0.907055 7.39538 1.14183C 7.27861 1.38786 7.21939 1.65037 7.21939 1.92205L 7.21939 2.60393L 2.05791 2.60393C 0.923188 2.60393 -8.46593e-09 3.46582 -8.46593e-09 4.52516ZM 8.33059 4.08327C 8.59203 4.08327 8.80393 3.88541 8.80393 3.64134L 8.80393 1.92209C 8.80393 1.65724 9.03629 1.4793 9.28023 1.4793C 9.38993 1.4793 9.50197 1.51532 9.59629 1.5956L 15.1316 6.3093C 15.3373 6.48454 15.3373 6.787 15.1316 6.96224L 9.59629 11.676C 9.50197 11.7563 9.38993 11.7923 9.28023 11.7923C 9.03633 11.7923 8.80393 11.6143 8.80393 11.3495L 8.80393 9.63026C 8.80393 9.3862 8.59203 9.18834 8.33059 9.18834L 2.05791 9.18834C 1.79648 9.18834 1.58457 8.99051 1.58457 8.74641L 1.58457 4.52516C 1.58457 4.2811 1.79648 4.08323 2.05791 4.08323L 8.33059 4.08327Z" />
              <Path id="path3_fill" d="M 9.29872 13.3062C 8.76705 13.3062 8.26046 13.1189 7.87231 12.7789C 7.66935 12.6012 7.50942 12.391 7.39697 12.154C 7.27913 11.9058 7.21939 11.641 7.21939 11.3668L 7.21939 10.7022L 2.07641 10.7022C 0.931474 10.7022 -8.46593e-09 9.83258 -8.46593e-09 8.76371L -8.46593e-09 4.54246C -8.46593e-09 3.47356 0.931474 2.60397 2.07641 2.60397L 7.21936 2.60397L 7.21936 1.93935C 7.21936 1.66525 7.27909 1.40036 7.39694 1.15212C 7.50934 0.915239 7.66928 0.704981 7.87227 0.527216C 8.26043 0.18726 8.76702 -4.21518e-09 9.29872 -4.21518e-09C 9.81511 -4.21518e-09 10.3111 0.180113 10.6951 0.507189L 16.2304 5.22095C 16.6607 5.58743 16.9075 6.10943 16.9075 6.65315C 16.9075 7.19688 16.6607 7.71884 16.2304 8.08525L 10.6951 12.799C 10.3111 13.1261 9.81511 13.3062 9.29872 13.3062ZM 2.07641 2.6385C 0.951854 2.6385 0.0369882 3.49262 0.0369882 4.54246L 0.0369882 8.76375C 0.0369882 9.81359 0.951854 10.6677 2.07641 10.6677L 7.25634 10.6677L 7.25634 11.3668C 7.25634 11.6361 7.31501 11.8963 7.43074 12.1401C 7.54115 12.3727 7.6982 12.5792 7.89757 12.7538C 8.27885 13.0878 8.77645 13.2717 9.29868 13.2717C 9.80583 13.2717 10.2929 13.0947 10.6702 12.7735L 16.2055 8.05977C 16.6281 7.69985 16.8705 7.18714 16.8705 6.65315C 16.8705 6.11913 16.6281 5.60642 16.2055 5.24647L 10.6702 0.532776C 10.293 0.211501 9.8059 0.0345998 9.29876 0.0345998C 8.77648 0.0345998 8.27888 0.218511 7.89764 0.552458C 7.69828 0.727046 7.54119 0.93354 7.43078 1.16617C 7.31508 1.41 7.25638 1.67012 7.25638 1.93942L 7.25638 2.63857L 2.07641 2.63857L 2.07641 2.6385ZM 9.29876 11.8268C 9.06078 11.8268 8.80397 11.6509 8.80397 11.3668L 8.80397 9.64756C 8.80397 9.41341 8.5999 9.2229 8.34912 9.2229L 2.07641 9.2229C 1.80521 9.2229 1.58457 9.01689 1.58457 8.76371L 1.58457 4.54246C 1.58457 4.28924 1.80521 4.08327 2.07641 4.08327L 8.34905 4.08327L 8.34909 4.08327C 8.47056 4.08327 8.58477 4.0391 8.67066 3.95892C 8.75659 3.87871 8.80389 3.77208 8.80389 3.65864L 8.80389 1.93939C 8.80389 1.65523 9.06074 1.47933 9.29868 1.47933C 9.42074 1.47933 9.53437 1.52108 9.62721 1.60012L 15.1626 6.31385C 15.2645 6.40066 15.323 6.52428 15.323 6.65308C 15.323 6.78188 15.2645 6.90551 15.1626 6.99232L 9.62729 11.706C 9.53448 11.7851 9.42086 11.8268 9.29876 11.8268ZM 2.07641 4.1178C 1.82559 4.1178 1.62156 4.3083 1.62156 4.54246L 1.62156 8.76375C 1.62156 8.9979 1.82563 9.18841 2.07641 9.18841L 8.34905 9.18841C 8.62025 9.18841 8.84088 9.39442 8.84088 9.6476L 8.84088 11.3668C 8.84088 11.6297 9.07853 11.7923 9.29868 11.7923C 9.41142 11.7923 9.5164 11.7537 9.60228 11.6806L 15.1376 6.96683C 15.2318 6.88655 15.2859 6.77222 15.2859 6.65312C 15.2859 6.53402 15.2318 6.41966 15.1376 6.33941L 9.60228 1.62571C 9.51636 1.55257 9.41139 1.51393 9.29868 1.51393C 9.0121 1.51393 8.84088 1.7303 8.84088 1.93946L 8.84088 3.65871C 8.84088 3.91193 8.62025 4.1179 8.34905 4.1179L 2.07641 4.1178Z" />
            </Defs>
          </Svg>
          <FooterText>Уже есть аккаунт?</FooterText>
        </Footer>
      </FooterWrapper>,
    ];
  }
}
