import React, { Component } from 'react';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack, replace } from 'react-router-redux';

import { Svg, Use, Defs, Path } from 'react-native-svg';

import { InputLabel, StyledTextInput, InputError, InputLabelContainer, LockIcon, InputContainer, InputIcon, InputShadow } from 'components/FormComponents';
import TopNavBar from 'components/TopNavBar';
import ViewWithShadow from 'components/ViewWithShadow';
import Row from 'components/Layouts/Row';
import ButtonMain from 'components/ButtonMain';

const AuthWrapper = styled.View`
  padding: 20px;
  margin: 0;
`;

const MainButtonWrapper = styled.View`
  margin-top: 8px;
  padding-bottom: 40px;
  padding-left: 5px;
  padding-right: 5px;
  position: absolute;
  bottom: 40px;
  left: 0;
  right: 0;
`;

const Footer = styled.View`
  height: 100%;
  background-color: white;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const FooterWrapper = styled.TouchableHighlight`
  height: 54px;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;

const FooterText = styled.Text`
  font-family: Roboto;
  font-weight: 500;
  font-size: 16px;
  margin: 12px;
  color: #4F4F4F;
  opacity: 0.5;
`;

const ButtonRow = Row.extend`
  padding: 0px;
  margin: 0px;
  height: 49px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
    replace,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class AuthContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
  };

  goToReg = () => {
    this.props.replace('/register');
  };

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="Авторизация" key="header" />,
      <AuthWrapper key="context">
        <InputLabelContainer>
          <InputLabel>E-mail</InputLabel><InputError>Неправильный E-mail или пароль</InputError>
        </InputLabelContainer>
        <InputContainer>
          <InputShadow />
          <StyledTextInput keyboardType="email-address" placeholder="E-mail" underlineColorAndroid="transparent" />
        </InputContainer>

        <InputLabelContainer>
          <InputLabel>Пароль</InputLabel>
        </InputLabelContainer>
        <InputContainer>
          <InputShadow />
          <StyledTextInput placeholder="Введите пароль" secureTextEntry underlineColorAndroid="transparent" />
          <InputIcon><LockIcon /></InputIcon>
        </InputContainer>
      </AuthWrapper>,
      <MainButtonWrapper key="button">
        <ViewWithShadow
          shadowRadius={3}
          cornerRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
        >
          <ButtonRow>
            <ButtonMain
              label="Войти"
              height={49}
              onPressGetRef={this.onRequestClose}
            />
          </ButtonRow>
        </ViewWithShadow>
      </MainButtonWrapper>,
      <FooterWrapper key="footer" onPress={this.goToReg}>
        <Footer>
          <Svg width="24" height="20" viewBox="0 0 24 20">
            <Use href="#path0_fill" transform="translate(14 0)" fill="#A9A9A9" />
            <Use href="#path1_fill" transform="translate(16 3)" fill="#A9A9A9" />
            <Use href="#path2_fill" transform="translate(0 4)" fill="#A9A9A9" />
            <Defs>
              <Path id="path0_fill" d="M 8.53755 1.46444C 7.59293 0.520042 6.33706 -1.00797e-09 5.00115 -1.00797e-09C 3.66531 -1.00797e-09 2.40938 0.520042 1.46485 1.46444C 0.520231 2.40884 -3.1456e-07 3.66437 -3.1456e-07 5C -3.1456e-07 6.33553 0.520231 7.59109 1.46485 8.53546C 2.40944 9.47986 3.66531 10 5.00115 10C 6.33703 10 7.59296 9.47986 8.53755 8.53546C 10.4875 6.58599 10.4875 3.41395 8.53755 1.46444ZM 7.74344 7.74157C 7.01098 8.47396 6.03702 8.87724 5.00115 8.87724C 3.96525 8.87724 2.99142 8.47396 2.25896 7.74157C 1.52644 7.00929 1.12306 6.03566 1.12306 5.00003C 1.12306 3.96441 1.52644 2.99068 2.25896 2.25839C 2.99142 1.52611 3.96531 1.12279 5.00115 1.12279C 6.03706 1.12279 7.01098 1.52607 7.74344 2.25839C 9.25551 3.77003 9.25551 6.22987 7.74344 7.74157Z" />
              <Path id="path1_fill" d="M 4.72883 1.99555L 3.40055 1.99555L 3.40055 0.682739C 3.40055 0.305658 3.09126 1.00525e-07 2.70979 1.00525e-07C 2.32832 1.00525e-07 2.01903 0.305658 2.01903 0.682739L 2.01903 1.99555L 0.690759 1.99555C 0.309289 1.99555 3.27441e-07 2.30125 3.27441e-07 2.67829C 3.27441e-07 3.05545 0.309289 3.36103 0.690759 3.36103L 2.01903 3.36103L 2.01903 4.67393C 2.01903 5.05109 2.32832 5.35667 2.70979 5.35667C 3.09126 5.35667 3.40055 5.05109 3.40055 4.67393L 3.40055 3.36103L 4.72883 3.36103C 5.1103 3.36103 5.41959 3.05545 5.41959 2.67829C 5.41959 2.30125 5.11034 1.99555 4.72883 1.99555Z" />
              <Path id="path2_fill" d="M 14.6885 10.9955C 13.7455 10.3383 12.6255 9.8534 11.4066 9.56245C 12.5371 8.59466 13.2707 7.04648 13.2707 5.30501C 13.2707 2.37983 11.2011 1.91242e-07 8.6572 1.91242e-07C 6.11343 1.91242e-07 4.0438 2.37979 4.0438 5.30501C 4.0438 7.04652 4.7774 8.5947 5.90786 9.56257C 4.68908 9.85336 3.56906 10.3383 2.62607 10.9955C 0.932622 12.1757 0 13.7681 0 15.4794C 0 15.8564 0.309289 16.1621 0.690759 16.1621L 16.6237 16.1621C 17.0052 16.1621 17.3144 15.8564 17.3144 15.4794C 17.3145 13.7681 16.3819 12.1757 14.6885 10.9955ZM 13.2707 14.7966L 13.2707 13.5773C 13.2707 13.2002 12.9614 12.8945 12.5799 12.8945C 12.1985 12.8945 11.8892 13.2002 11.8892 13.5773L 11.8892 14.7966L 5.42536 14.7966L 5.42536 13.5773C 5.42536 13.2002 5.11607 12.8945 4.7346 12.8945C 4.35313 12.8945 4.04384 13.2002 4.04384 13.5773L 4.04384 14.7966L 1.45378 14.7966C 1.66711 13.7955 2.34747 12.8606 3.42217 12.1116C 4.81145 11.1434 6.67068 10.6102 8.65732 10.6102C 10.644 10.6102 12.5032 11.1433 13.8924 12.1116C 14.9671 12.8606 15.6474 13.7955 15.8607 14.7966L 13.2707 14.7966ZM 5.42536 5.30505C 5.42536 3.13278 6.87519 1.36552 8.65724 1.36552C 10.4393 1.36552 11.8892 3.13286 11.8892 5.30505C 11.8892 7.47743 10.4394 9.2447 8.65732 9.2447C 6.87528 9.2447 5.42536 7.47735 5.42536 5.30505Z" />
            </Defs>
          </Svg>
          <FooterText>Регистрация</FooterText>
        </Footer>
      </FooterWrapper>,
    ];
  }
}
