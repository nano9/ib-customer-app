import React, { Component } from 'react';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack, replace } from 'react-router-redux';

import { Svg, Use, Defs, Path } from 'react-native-svg';

import {
  InputLabel,
  StyledTextInput,
  InputError,
  InputLabelContainer,
  LockIcon,
  InputContainer,
  InputIcon,
  InputShadowб
} from 'components/FormComponents';
import TopNavBar from 'components/TopNavBar';
import ViewWithShadow from 'components/ViewWithShadow';
import PersonalInfoFormComponent from 'components/PersonalInfoFormComponent';
import Row from 'components/Layouts/Row';
import ButtonMain from 'components/ButtonMain';

const AuthScrollView = styled.ScrollView`
  flex: 1;
`;

const AuthWrapper = styled.View`
  padding: 20px;
`;

const MainButtonWrapper = styled.View`
  margin-top: auto;
  height: 49px;
`;

const Footer = styled.View`
  background-color: white;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 20px;
`;

const FooterWrapper = styled.TouchableHighlight`
  padding-top: 20px;
`;

const FooterText = styled.Text`
  font-family: Roboto;
  font-weight: 500;
  font-size: 16px;
  margin: 12px;
  color: #4F4F4F;
  opacity: 0.5;
`;

const ButtonWrapper = styled.View`
  padding-left: 5px;
  padding-right: 5px;
  margin-bottom: 30px;
`;

const ButtonRow = Row.extend`
  padding: 0px;
  margin: 0px;
  height: 49px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class RegisterContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
  };

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="Смена пароля" key="header" />,
      <AuthScrollView key="context">
        <AuthWrapper>
          <InputLabelContainer>
            <InputError>Пароли не совпадают</InputError>
          </InputLabelContainer>
          <InputContainer>
            <StyledTextInput placeholder="Новый пароль" secureTextEntry underlineColorAndroid="transparent" />
            <InputIcon><LockIcon /></InputIcon>
          </InputContainer>
          <InputLabelContainer>
          </InputLabelContainer>
          <InputContainer>
            <StyledTextInput placeholder="Повторите пароль" secureTextEntry underlineColorAndroid="transparent" />
            <InputIcon><LockIcon /></InputIcon>
          </InputContainer>
        </AuthWrapper>
      </AuthScrollView>,
      <ButtonWrapper key="btn">
        <ViewWithShadow
          shadowRadius={3}
          cornerRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
        >
          <ButtonRow>
            <ButtonMain
              label="Сохранить"
              height={49}
              onPressGetRef={this.onRequestClose}
            />
          </ButtonRow>
        </ViewWithShadow>
      </ButtonWrapper>
    ];
  }
}
