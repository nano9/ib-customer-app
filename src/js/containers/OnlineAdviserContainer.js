import React, { Component } from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import TopNavBar from 'components/TopNavBar';
import ViewWithShadow from 'components/ViewWithShadow';
import Row from 'components/Layouts/Row';
import ButtonMain from 'components/ButtonMain';
import {
  InputLabel,
  InputError,
  InputLabelContainer,
  InputContainer,
  StyledTextInput,
  StyledPicker
} from 'components/FormComponents';

const FormContainer = styled.View`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-top: 45px;
  padding-left: 20px;
  padding-right: 20px;
  margin-bottom: 62px;
`;

const InputRow = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const RowInputWrapper = styled.View`
  flex: 1;
  align-items: stretch;
  margin-right: ${({ withMarginRight }) => withMarginRight ? 16 : 0}px;
`;

const ButtonContainer = styled.View`
  width: 100%;
  padding-left: 5px;
  padding-right: 5px;
`;

const ButtonRow = Row.extend`
  padding: 0px;
  margin: 0px;
  height: 49px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class OnlineAdviserContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
  };

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="Online консультант" key="header" />,
      <FormContainer key="content">
        <InputLabelContainer>
          <InputLabel>ФИО</InputLabel><InputError>Поле не может быть пустым</InputError>
        </InputLabelContainer>
        <InputContainer>
          <StyledTextInput placeholder="ФИО" />
        </InputContainer>

        <InputLabelContainer>
          <InputLabel>Телефон</InputLabel><InputError>Некорректный телефон</InputError>
        </InputLabelContainer>
        <InputRow>
          <RowInputWrapper withMarginRight>
            <InputContainer>
              <StyledPicker
                items={[
                  { label: '+38 Украина', value: '1' },
                  { label: '+123', value: '2' },
                ]}
              />
            </InputContainer>
          </RowInputWrapper>
          <RowInputWrapper>
            <InputContainer>
              <StyledTextInput keyboardType="phone-pad" placeholder="(098) 000-00-00" />
            </InputContainer>
          </RowInputWrapper>
        </InputRow>
      </FormContainer>,
      <ButtonContainer key="button">
        <ViewWithShadow
          shadowRadius={3}
          cornerRadius={3}
          color={0x000000}
          colorAlpha={80}
          bgColor={0xffffff}
          bgColorAlpha={0xff}
        >
          <ButtonRow>
            <ButtonMain
              label="Перезвоните мне"
              height={49}
              onPressGetRef={this.onRequestClose}
            />
          </ButtonRow>
        </ViewWithShadow>
      </ButtonContainer>
    ];
  }
}
