import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';

import EventPageContainer from 'containers/EventPageContainer';
import EventBuyTicketsSchemelessContainer from 'containers/EventBuyTickets/EventBuyTicketsSchemelessContainer';
import EventBuyTicketsWithSchemeContainer from 'containers/EventBuyTickets/EventBuyTicketsWithSchemeContainer';
import EventBuyTicketsCartContainer from 'containers/EventBuyTickets/EventBuyTicketsCartContainer';
import HallPageContainer from 'containers/HallPageContainer';
import TicketInfoContainer from 'containers/Tickets/TicketInfoContainer';
import TicketsNotPaidContainer from 'containers/Tickets/TicketsNotPaidContainer';
import TicketsPaidContainer from 'containers/Tickets/TicketsPaidContainer';

import SearchContainer from 'containers/SearchContainer';
import FavoritesContainer from 'containers/FavoritesContainer';

import OnlineAdviserContainer from 'containers/OnlineAdviserContainer';
import CashBoxesContainer from 'containers/CashBoxesContainer';
import FAQContainer from 'containers/FAQContainer';
import TermsConditionsContainer from 'containers/TermsConditionsContainer';

import AuthContainer from 'containers/Auth/AuthContainer';
import RegisterContainer from 'containers/Auth/RegisterContainer';
import ChangePasswordContainer from 'containers/Auth/ChangePasswordContainer';
import EditPersonalInfoContainer from 'containers/PersonalCabinet/EditPersonalInfoContainer';
import CardListContainer from 'containers/PersonalCabinet/CardListContainer';

const ItemTouchable = styled(TouchableOpacity)`
  width: 100%;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px;
  border-bottom-color: grey;
  border-top-color: white;
`;

const ItemLabel = styled.Text`
  font-size: 17px;
  color: black;
`;

const BackBtnWrapper = styled.View`
  position: absolute;
  right: 0;
  top: 0;
  width: 50px;
  height: 50px;
  border-radius: 25px;
`;

const PC = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
`;


export default class FakeLinksContainer extends Component {
  state = {
    page: null,
  };

  setPage = page => this.setState({ page });

  renderPageBackBtn = () => (
    <BackBtnWrapper>
      <TouchableOpacity onPress={() => this.setState({ page: null })} style={{ width: '100%', height: '100%', borderRadius: 25 }}>
        <View style={{ width: 50, height: 50, backgroundColor: 'rgba(0,0,0,0.5)', borderRadius: 25 }} />
      </TouchableOpacity>
    </BackBtnWrapper>
  );

  renderPage = () => {
    switch (this.state.page) {
      case 'event-page': return <PC><EventPageContainer />{this.renderPageBackBtn()}</PC>;
      case 'event-page-schemeless': return <PC><EventBuyTicketsSchemelessContainer />{this.renderPageBackBtn()}</PC>;
      case 'event-page-withscheme': return <PC><EventBuyTicketsWithSchemeContainer />{this.renderPageBackBtn()}</PC>;
      case 'event-page-cart': return <PC><EventBuyTicketsCartContainer />{this.renderPageBackBtn()}</PC>;
      case 'hall-page': return <PC><HallPageContainer />{this.renderPageBackBtn()}</PC>;
      case 'ticket-info': return <PC><TicketInfoContainer />{this.renderPageBackBtn()}</PC>;
      case 'ticket-not-paid': return <PC><TicketsNotPaidContainer />{this.renderPageBackBtn()}</PC>;
      case 'ticket-paid': return <PC><TicketsPaidContainer />{this.renderPageBackBtn()}</PC>;
      case 'search': return <PC><SearchContainer />{this.renderPageBackBtn()}</PC>;
      case 'favorites': return <PC><FavoritesContainer />{this.renderPageBackBtn()}</PC>;
      case 'terms': return <PC><TermsConditionsContainer />{this.renderPageBackBtn()}</PC>;
      case 'faq': return <PC><FAQContainer />{this.renderPageBackBtn()}</PC>;
      case 'online': return <PC><OnlineAdviserContainer />{this.renderPageBackBtn()}</PC>;
      case 'cashboxes': return <PC><CashBoxesContainer />{this.renderPageBackBtn()}</PC>;
      case 'auth': return <PC><AuthContainer />{this.renderPageBackBtn()}</PC>;
      case 'register': return <PC><RegisterContainer />{this.renderPageBackBtn()}</PC>;
      case 'edit-info': return <PC><EditPersonalInfoContainer />{this.renderPageBackBtn()}</PC>;
      case 'card-list': return <PC><CardListContainer />{this.renderPageBackBtn()}</PC>;
      case 'change-password': return <PC><ChangePasswordContainer />{this.renderPageBackBtn()}</PC>;
      default: return this.renderLinks();
    }
  }

  renderLinks = () => (
    <ScrollView>
      <ItemTouchable onPress={() => this.setState({ page: 'event-page' })}>
        <ItemLabel>Страница мероприятия</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'event-page-schemeless' })}>
        <ItemLabel>Покупка билета: без выбора места</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'event-page-withscheme' })}>
        <ItemLabel>Покупка билета: выбор места</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'event-page-cart' })}>
        <ItemLabel>Покупка билета: корзина</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'hall-page' })}>
        <ItemLabel>Страница зала</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'ticket-info' })}>
        <ItemLabel>Билеты: полная информация</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'ticket-not-paid' })}>
        <ItemLabel>Билеты: неоплаченные</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'ticket-paid' })}>
        <ItemLabel>Билеты: оплаченные</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'search' })}>
        <ItemLabel>Поиск</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'favorites' })}>
        <ItemLabel>Избранное</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'terms' })}>
        <ItemLabel>Правила использования приложения</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'faq' })}>
        <ItemLabel>F.A.Q.</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'cashboxes' })}>
        <ItemLabel>Кассы в городе</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'online' })}>
        <ItemLabel>Online консультант</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'auth' })}>
        <ItemLabel>Авторизация</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'register' })}>
        <ItemLabel>Регистрация</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'change-password' })}>
        <ItemLabel>Изменить пароль</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'card-list' })}>
        <ItemLabel>Список банковских карт</ItemLabel>
      </ItemTouchable>
      <ItemTouchable onPress={() => this.setState({ page: 'edit-info' })}>
        <ItemLabel>Изменить личные данные</ItemLabel>
      </ItemTouchable>
    </ScrollView>
  );

  render() {
    return (
      <View>
        { this.renderPage() }
      </View>
    );
  }
}
