import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'react-native';
import styled from 'styled-components/native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import TopNavBar from 'components/TopNavBar';
import FAQCollapsableItem from 'components/FAQCollapsableItem';

const Wrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class FAQContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
  }

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="FAQ (Вопрос - Ответ)" key="header" />,
      <ScrollView key="body">
        <Wrapper>
          <FAQCollapsableItem
            label="Что мне делать, если купленный билет не пришел на почту e-mail?"
            text={[
              '',
              '',
            ]}
          />
        </Wrapper>
      </ScrollView>,
    ];
  }
}
