import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

// import { request } from 'actions/api/utils';

import TopNavBar from 'components/TopNavBar';
import Footer from 'components/EventBuyTickets/Footer';

const Wrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #F2F2F2;
  flex: 1;
`;

const NoWidgetLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  text-align: center;
  color: #4F4F4F;
  margin-top: 15px;
`;
// TODO Handle errors

export default class EventBuyTicketsWithSchemeContainer extends Component {
  static propTypes = {
    // eventID: PropTypes.number.isRequired,
    goBack: PropTypes.string,
  };

  static defaultProps = {
    goBack: '/',
  };

  // state = {
  //   tickets: {},
  //   isLoading: true,
  // };

  // componentWillMount() {
  //   request({
  //     url: 'event/widgetUrl',
  //     query: {
  //       eventId: this.props.eventID,
  //     },
  //   }).then(data => this.setState({ isLoading: false, widgetURL: data.url }));
  // }

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="The World Famous Glenn Miller" key="header" />,
      <Wrapper key="body">
        <ActivityIndicator size="large" color="#4F4F4F" />
        <NoWidgetLabel>Загрузка данных</NoWidgetLabel>
      </Wrapper>,
      <Footer key="footer" type="type2" />,
    ];
  }
}
