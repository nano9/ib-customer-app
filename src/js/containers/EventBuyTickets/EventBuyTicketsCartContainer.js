import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import TopNavBar from 'components/TopNavBar';
import CartBodyInfo from 'components/EventBuyTickets/CartBodyInfo';
import Footer from 'components/EventBuyTickets/Footer';

const Wrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #F2F2F2;
  flex: 1;
  z-index: 600;
  margin-top: -29px;
`;

export default class EventBuyTicketsCartContainer extends Component {
  static propTypes = {
    goBack: PropTypes.string,
  };

  static defaultProps = {
    goBack: '/',
  };

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="Оформить билет" key="header" />,
      <Wrapper key="body">
        <CartBodyInfo />
      </Wrapper>,
      <Footer key="footer" />,
    ];
  }
}
