import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components';

// import { request } from 'actions/api/utils';

import TopNavBar from 'components/TopNavBar';
import SchemelessBodyInfo from 'components/EventBuyTickets/SchemelessBodyInfo';
import Footer from 'components/EventBuyTickets/Footer';
import Tooltip from 'components/Tooltip';

const Wrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #F2F2F2;
`;

// TODO Handle errors
export default class EventBuyTicketsSchemelessContainer extends Component {
  static propTypes = {
    // eventID: PropTypes.number.isRequired,
    goBack: PropTypes.string,
  };

  static defaultProps = {
    goBack: '/',
  };

  state = {
    isShowTooltip: false,
  };

  // state = {
  //   tickets: {},
  //   pendingRequests: 1,
  // };

  // componentWillMount() {
  //   this.updateTicketList();
  // }

  // ticketOperation = (ticketID, reserve) => {
  //   this.setState((state) => {
  //     const changeAmount = reserve ? 1 : -1;
  //     if (state.tickets[ticketID] + changeAmount > state.data[ticketID].available) {
  //       return {};
  //     }

  //     request({
  //       url: reserve ? 'event/ns/reservePricedTicket' : 'event/ns/removePricedTicketReserve',
  //       query: {
  //         ticketId: ticketID,
  //       },
  //     });

  //     this.updateTicketList();

  //     return {
  //       pendingRequests: state.pendingRequests + 1,
  //       tickets: {
  //         ...state.tickets,
  //         [ticketID]: state.tickets[ticketID] + changeAmount,
  //       },
  //     };
  //   });
  // };

  // updateTicketList = () => {
  //   request({
  //     url: 'event/ns/quota',
  //     query: {
  //       eventId: this.props.eventID,
  //     },
  //   }).then(this.endLoadingQuota);
  // };

  // endLoadingQuota = data => this.setState(state => ({
  //   pendingRequests: state.pendingRequests - 1,
  //   data: data.reduce((acc, val) => ({
  //     ...acc,
  //     [val.id]: val,
  //   }), {}),
  // }));

  toggleTooltip = () => {
    this.setState({ isShowTooltip: !this.state.isShowTooltip });
  }

  renderInfoTooltip = () => (
    <Tooltip
      text="Данный билет дает право посещения мероприятия, а так же отдельный вход"
      right={10}
      bottom={125}
      key="tooltip"
      onToggle={this.toggleTooltip}
    />
  );

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="The World Famous Glenn Miller" key="header" />,
      <ScrollView key="body" style={{ backgroundColor: '#F2F2F2' }}>
        <Wrapper>
          <SchemelessBodyInfo />
        </Wrapper>
      </ScrollView>,
      <Footer key="footer" onToggleTooltip={this.toggleTooltip} />,
      this.state.isShowTooltip && this.renderInfoTooltip(),
    ];
  }
}
