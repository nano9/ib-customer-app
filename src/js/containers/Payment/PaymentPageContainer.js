import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import TopNavBar from 'components/TopNavBar';
import SideShadowSource from 'components/SideShadowSource';
import Menu, {
  METHOD_SELF_DELIVERY,
  METHOD_CITY_DELIVERY,
  METHOD_UA_DELIVERY,
  METHOD_ETICKET,
} from 'components/Payment/Menu';
import Footer from 'components/Payment/Footer';
import ETicketMethod from 'components/Payment/ETicketMethod';
import CityDeliveryMethod from 'components/Payment/CityDeliveryMethod';
import SelfDeliveryMethod from 'components/Payment/SelfDeliveryMethod';
import UkraineDeliveryMethod from 'components/Payment/UkraineDeliveryMethod';
import TicketInfo from 'components/Payment/TicketInfo';
import { ETIME } from 'constants';

const Wrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #fff;
  padding-bottom: 40px;
`;

export default class PaymentContainer extends Component {
  static propTypes = {
    goBack: PropTypes.string,
  };

  static defaultProps = {
    goBack: '/',
  };

  state = {
    activeDeliveryMethod: METHOD_ETICKET,
    rulesAccepted: false,
  };

  toggleRulesCheckbox = () => this.setState({ rulesAccepted: !this.state.rulesAccepted });

  changeDeliveryMethod = (method) => {
    if (this.state.activeDeliveryMethod !== method) {
      this.setState({ activeDeliveryMethod: method });
    }
  }

  renderDeliveryMethod = () => {
    const selectedCashBoxId = 123;

    switch (this.state.activeDeliveryMethod) {
      case METHOD_ETICKET:
        return (
          <ETicketMethod />
        );
      case METHOD_CITY_DELIVERY:
        return (
          <CityDeliveryMethod />
        );
      case METHOD_SELF_DELIVERY:
        return (
          <SelfDeliveryMethod
            selectedCashBoxId={selectedCashBoxId}
          />
        );
      case METHOD_UA_DELIVERY:
        return (
          <UkraineDeliveryMethod />
        );
      default:
        return null;
    }
  }

  render() {
    const { rulesAccepted } = this.state;
    return [
      <TopNavBar goBack={this.props.goBack} label="Оплата" key="header" />,
      <ScrollView key="bdy">
        <SideShadowSource goes="from-top" bgColor="#fff" />
        <Menu
          onDeliveryMethodChange={this.changeDeliveryMethod}
          activeDeliveryMethod={this.state.activeDeliveryMethod}
        />
        <Wrapper>
          <TicketInfo
            image={{ size1: 'http://via.placeholder.com/350x150', size2: 'http://via.placeholder.com/350x150' }}
            title="The World Famous Glenn Miller Orchestra (Глен Миллер)"
            city="Днепр"
            place="Днепропетровская филармония имени Л. Б. Когана"
            date="27 октября 23:00"
          />
          { this.renderDeliveryMethod() }
          <Footer
            rulesAccepted={rulesAccepted}
            onToggleRulesCheckbox={this.toggleRulesCheckbox}
          />
        </Wrapper>
      </ScrollView>,
    ];
  }
}
