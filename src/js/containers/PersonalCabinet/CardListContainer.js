import React, { Component } from 'react';
import { TouchableHighlight, ScrollView } from 'react-native';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import TopNavBar from 'components/TopNavBar';
import ShadowView from 'components/ShadowView';
import {
  CardItem,
  MasterCardImage,
  VisaImage,
  CardTextContainer,
  CardText,
} from 'components/PersonalCabinet/BankCard';

import visa from 'img/visa.png';
import mastercard from 'img/mastercard.png';
import CloseBtnIcon from 'components/Icons/CloseBtnIcon';

const CloseBtnContainer = styled.View`
  padding-left: 20px;
  padding-right: 20px;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class RegisterContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
  };

  render() {
    const cards = [
      { id: '1', type: 'visa', cardLastNumber: '1234' },
      { id: '2', type: 'mc', cardLastNumber: '1234' },
      { id: '1', type: 'visa', cardLastNumber: '1234' },
      { id: '2', type: 'mc', cardLastNumber: '1234' },
      { id: '1', type: 'visa', cardLastNumber: '1234' },
      { id: '2', type: 'mc', cardLastNumber: '1234' },
      { id: '1', type: 'visa', cardLastNumber: '1234' },
      { id: '2', type: 'mc', cardLastNumber: '1234' },
    ];
    return [
      <TopNavBar goBack={this.props.goBack} label="Управление картами" key="header" />,
      <ScrollView key="content" contentContainerStyle={{ paddingBottom: 40, paddingTop: 40 }}>
        {
          cards.map(({
            id,
            type,
            cardLastNumber,
          }) => (
            <ShadowView
              key="2"
              color={0x000000}
              colorAlpha={0x33}
              shadowRadius={7}
              bgColor={0xFFFFFF}
              bgColorAlpha={0xFF}
              cornerRadius={2}
            >
              <CardItem>
                <MasterCardImage source={type === 'visa' ? visa : mastercard} />
                <CardTextContainer>
                  <CardText>**** **** **** {cardLastNumber}</CardText>
                </CardTextContainer>
                <TouchableHighlight onPress={this.props.goBack} underlayColor="rgba(0,0,0,0.05)">
                  <CloseBtnContainer>
                    <CloseBtnIcon />
                  </CloseBtnContainer>
                </TouchableHighlight>
              </CardItem>
            </ShadowView>
          ))
        }
      </ScrollView>,
    ];
  }
}
