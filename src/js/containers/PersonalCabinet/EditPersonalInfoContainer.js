import React, { Component } from 'react';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import TopNavBar from 'components/TopNavBar';
import ViewWithShadow from 'components/ViewWithShadow';
import Row from 'components/Layouts/Row';
import ButtonMain from 'components/ButtonMain';

import PersonalInfoFormComponent from 'components/PersonalInfoFormComponent';

const AuthScrollView = styled.ScrollView`
  flex: 1;
`;

const AuthWrapper = styled.View`
  padding: 20px;
`;

const MainButtonWrapper = styled.View`
  margin-top: 1px;
  height: 49px;
  padding-left: 5px;
  padding-right: 5px;
`;

const ButtonRow = Row.extend`
  padding: 0px;
  margin: 0px;
  height: 49px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class EditPersonalInfoContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
  };

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="Редактирование профиля" key="header" />,
      <AuthScrollView key="context" contentContainerStyle={{ paddingBottom: 40 }}>
        <AuthWrapper>
          <PersonalInfoFormComponent />
        </AuthWrapper>
        <MainButtonWrapper>
          <ViewWithShadow
            shadowRadius={3}
            cornerRadius={3}
            color={0x000000}
            colorAlpha={80}
            bgColor={0xffffff}
            bgColorAlpha={0xff}
          >
            <ButtonRow>
              <ButtonMain
                label="Сохранить"
                height={49}
                onPressGetRef={this.onRequestClose}
              />
            </ButtonRow>
          </ViewWithShadow>
        </MainButtonWrapper>
      </AuthScrollView>,
    ];
  }
}
