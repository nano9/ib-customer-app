import React from 'react';
import { ScrollView, View } from 'react-native';
import styled from 'styled-components/native';

import { Link } from 'react-router-native';

import TopNavBar from 'components/TopNavBar';

const ScrollViewContainer = styled.View`
  padding-bottom: 40px;
`;

const MenuItem = styled(Link)`
  border-style: solid;
  border-bottom-width: 1px;
  border-bottom-color: #E0E0E0;
  height: 55px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding-left: 26px;
  padding-right: 26px;
  &:hover {
    color: rgba(0,0,0,0.05);
  }
`;

const MenuText = styled.Text`
  font-size: 16px;
  color: #767676;
`;

const UserProfileLabel = MenuText.extend`
  color: #006DF0;
`;

const ChangeAccountLabel = MenuText.extend`
  color: #EB5757;
`;

// TODO Add query for username from redux state

export default function MenuContainer() {
  const menu = [
    { text: 'История заказов', url: '/pc/history' },
    { text: 'Редактирование данных профиля', url: '/pc/edit' },
    { text: 'Управление подписками', url: '/pc/subs' },
    { text: 'Смена пароля', url: '/pc/changePassword' },
    { text: 'Управление картами', url: '/pc/cardManagement' },
    { text: 'FAQ', url: '/faq' },
    { text: 'Условия использования', url: '/terms' },
  ].map(
    ({ text, url }) => (
      <MenuItem
        underlayColor="rgba(0,0,0,0.05)"
        key={url}
        to={url}
      >
        <MenuText>{text}</MenuText>
      </MenuItem>
    )
  );
  return [
    <TopNavBar label="Личный кабинет" key="header" />,
    <ScrollView key="menu" contentContainerStyle={{ paddingBottom: 40 }}>

      <MenuItem
        underlayColor="rgba(0,0,0,0.05)"
        key="username"
      >
        <UserProfileLabel>Сиракова Марина Андреевна</UserProfileLabel>
      </MenuItem>

      {menu}

      <MenuItem
        underlayColor="rgba(0,0,0,0.05)"
        key="change-account"
      >
        <ChangeAccountLabel>Сменить аккаунт</ChangeAccountLabel>
      </MenuItem>

    </ScrollView>,
  ];
}
