import React, { Component } from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';

import { Svg, Defs, LinearGradient, Stop, Rect } from 'react-native-svg';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import ArrowIcon from 'components/Icons/ArrowIcon';
import TopNavBar from 'components/TopNavBar';
import ShadowView from 'components/ShadowView';
import { Checkbox } from 'components/FormComponents';

const SubItem = styled.View`
  flex-direction: row;
  padding: 25px 0;
  align-items: center;
`;

const SubCheckbox = styled.View`
  border-radius: 3px;
  border: 2px solid rgba(118, 118, 118, 0.4);
  margin: 0 33px;
  width: 36px;
  height: 36px;
`;

const SubCheckboxActive = styled.View`
  margin: 0 28px;
`;

const ItemText = styled.Text`
  flex: 1;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  line-height: 24px;
  font-size: 16px;
  
  color: #767676;
`;

const ArrowIconWrapper = styled.View`
  position: absolute;
  top: 10px;
  left: 8px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class PersonalSubscriptionContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
  };

  render() {
    return [
      <TopNavBar goBack={this.props.goBack} label="Управление подписками" key="header" />,
      <View style={{ paddingTop: 17 }}>
        <Checkbox
          key="email"
          label="Получать email рассылку"
        />
        <Checkbox
          key="sms"
          label="Получать SMS рассылку"
        />
      </View>
    ];
  }
}
