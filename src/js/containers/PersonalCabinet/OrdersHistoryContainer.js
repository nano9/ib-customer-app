import React, { Component } from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import PropTypes from 'prop-types';

import styled from 'styled-components/native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import ViewWithShadow from 'components/ViewWithShadow';
import TopNavBar from 'components/TopNavBar';

const TicketContainer = styled.View`
  flex-direction: row;
  padding: 14px;
  padding-left: 8px;
  padding-bottom: 17px;
  background-color: #fff;
  border-radius: 2px;
`;

const TicketInfo = styled.View`
  flex: 1;
  padding-left: 5px;
`;

const TicketInfoRow = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const TicketInfoMain = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  line-height: 24px;
  font-size: 12px;

  color: #4F4F4F;
`;
const TicketInfoSecondary = TicketInfoMain.extend`
  color: #767676;
`;
const DateText = TicketInfoSecondary;
const stateColors = {
  finished: '#27AE60',
  canceled: '#FF4D00',
};
const StateText = DateText.extend`
  color: ${({ state }) => stateColors[state]};
`;
const EventNameText = styled.Text`
  font-family: Roboto;
  font-size: 13px;
  color: #006DF0;
`;
const EventLocationText = DateText;

const TicketPrice = TicketInfoMain.extend`
  color: #006DF0;
`;

const ImageContainer = styled.View`
  height: 96px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class OrdersHistoryContainer extends Component {
  static propTypes = {
    goBack: PropTypes.func.isRequired,
  };

  render() {
    const orders = [
      {
        id: 0,
        event: {
          id: 17943,
          name: 'Александр Меламуд. "Кабаре"',
          date: '22 февраля, 19:00',
          city: 'Харьков',
          hallName: 'ТЕАТРАЛЬНО-КОНЦЕРТНИЙ ЦЕНТР',
          freeTicketsAmount: 272,
          priceRangeBounds: {
            hasPrices: true,
            hasMultiPrices: true,
            minPrice: '150',
            fromNotice: 'от 150 грн',
          },
          image: {
            size1: 'http://api.internetbilet.com.ua/images/events_header/size1/eh_1507037239_59d3903753243.jpg',
            size2: 'http://api.internetbilet.com.ua/images/events_header/size2/eh_1507037239_59d3903753243.jpg',
          },
          url: 'http://kharkov.api.internetbilet.com.ua/Aleksandr-Melamud',
        },
        location: {
          type: 'Партер',
          row: 7,
          col: 11,
        },
        price: 260,
        state: 'finished',
      },
    ];
    orders.push({ ...orders[0] });
    orders.push({ ...orders[0] });
    orders.push({ ...orders[0] });
    orders[1].id = 1; orders[2].id = 2; orders[3].id = 3;
    orders[2].state = 'canceled';

    return [
      <TopNavBar goBack={this.props.goBack} label="История заказов" key="header" />,
      <ScrollView key="content" contentContainerStyle={{ paddingBottom: 40, paddingTop: 7 }}>
        {
          orders.map(({
            id,
            event: {
              image,
              name,
              date,
              city,
              hallName,
            },
            location: { type, row, col },
            price,
            state,
          }) => (
            <ViewWithShadow
              key={id}
              color={0x000000}
              colorAlpha={0x33}
              shadowRadius={7}
              offsetY={4}
              bgColor={0xFFFFFF}
              bgColorAlpha={0xFF}
              cornerRadius={2}
            >
              <TicketContainer>
                <ImageContainer>
                  <ViewWithShadow
                    color={0x000000}
                    colorAlpha={0x50}
                    shadowRadius={3}
                    bgColor={0xFFFFFF}
                    bgColorAlpha={0xFF}
                    cornerRadius={2}
                  >
                    <Image source={{ uri: image.size2, width: 60, height: 86 }} />
                  </ViewWithShadow>
                </ImageContainer>
                <TicketInfo>
                  <TicketInfoRow>
                    <DateText>{date}</DateText>
                    <StateText state={state}>{state}</StateText>
                  </TicketInfoRow>
                  <TicketInfoRow>
                    <EventNameText>{name}</EventNameText>
                  </TicketInfoRow>
                  <TicketInfoRow>
                    <EventLocationText>{city}, {hallName}</EventLocationText>
                  </TicketInfoRow>
                  <TicketInfoRow>
                    <TicketInfoMain>{type}</TicketInfoMain>
                    <TicketInfoMain>
                      Ряд
                      <TicketInfoSecondary> {row}</TicketInfoSecondary>
                    </TicketInfoMain>
                    <TicketInfoMain>
                      Место
                      <TicketInfoSecondary> {col}</TicketInfoSecondary>
                    </TicketInfoMain>
                    <TicketPrice>
                      Цена {price}
                    </TicketPrice>
                  </TicketInfoRow>
                </TicketInfo>
              </TicketContainer>
            </ViewWithShadow>
          ))
        }
      </ScrollView>,
    ];
  }
}
