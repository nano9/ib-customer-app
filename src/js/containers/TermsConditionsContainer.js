import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import styled from 'styled-components/native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import TopNavBar from 'components/TopNavBar';
import ViewWithShadow from 'components/ViewWithShadow';
import FAQCollapsableItem from 'components/FAQCollapsableItem';
import Row from 'components/Layouts/Row';
import { H1, H2, P, B } from 'components/Markdown';

const Wrapper = styled.View`
  padding-top: 22px;
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: 22px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class TermsConditionsContainer extends Component {
  render() {
    return [
      <TopNavBar label="Условия использования" goBack={this.props.goBack} key="header" />,
      <ScrollView key="body">
        <Wrapper>
          <H1 withPadding>ДОГОВОР НА Р ЕАЛИЗАЦИЮ БИЛЕТОВ НА КУЛЬТУРНО-ЗРЕЛИЩНЫЕ МЕРОПРИЯТИЯ (ПУБЛИЧНАЯ ОФЕРТА), С ПОМОЩЬЮ «СИСТЕМА ОН-ЛАЙН ПРОДАЖИ «INTERNET-BILET»</H1>
          <H2 withPadding>ПОНЯТИЯ и ОПРЕДЕЛЕНИЯ.</H2>
          <P withPadding>Нижеприведенные термины и определения используются в следующих значениях:</P>
          <P withPadding>
            1.1.<B>«Оферта»</B> –предложение «Агента по реализации билетов» заключить с ним со глашение 
            по бронированию, оформлению и реализации Электронных Билетов на культурно-зрелищные 
            мероприятия с помощью «Системы».
          </P>
          <P withPadding>
            1.2. <B>«Система он-лайн продажи «INTERNET-BILET»</B> - программно-технический комплекс, 
            предназначенный для бронирования и продажи билетов на театрально-зрелищные, цирковые, 
            спортивные, культурно-просветительские, другие мероприятия организации досуга; расположена 
            по электронному адресу https://internet-bilet.ua, далее «Система»;
          </P>
          <P withPadding>
            1.3. <B>«Мероприятие»</B> – культурно-зрелищное или спортивное мероприятие, в 
            том числе театральный спектакль, цирковое представление, концерт в зале, 
            клубе или на открытой площадке, выставка, кинопоказ, фестиваль, шоу, показ мод, 
            спортивное соревнование, экскурсия, а также любое иное событие, посещение 
            которого осуществляется по предъявлению специального документа – бланка Электронного Билета;
          </P>
          <P>
            1.4. <B>«Покупатель»</B> - физическое или юридическое лицо (лица), которому на условиях, 
            установленных настоящей «Офертой», оказываются услуги по бронированию, оформлению и 
            реализации бланков Электронных Билетов на «Мероприятия», с помощью «Системы»;
          </P>
        </Wrapper>
      </ScrollView>,
    ];
  }
}
