import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import CashBoxCollapsableItem from 'components/CashBoxCollapsableItem';
import Row from 'components/Layouts/Row';
import ButtonMain from 'components/ButtonMain';

const Wrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Container = styled.View`
  width: 100%;
`;

const ButtonRow = Row.extend`
  padding: 0px;
  margin: 0px;
  height: 49px;
`;

export default class CashBoxesContainer extends Component {
  render() {
    return (
      <ScrollView>
        <Wrapper>
          <CashBoxCollapsableItem
            label='Test label'
            address='Some address here'
            date='some date plz here'
            number='838388383'
          />
          <CashBoxCollapsableItem
            label='Test label'
            address='Some address here'
            date='some date plz here'
            number='838388383'
          />
          <CashBoxCollapsableItem
            label='Test label'
            address='Some address here'
            date='some date plz here'
            number='838388383'
          />
        </Wrapper>
      </ScrollView>
    );
  }
}
