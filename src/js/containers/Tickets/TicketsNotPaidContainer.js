import React, { PureComponent } from 'react';
import { View, ScrollView } from 'react-native';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import ButtonMain from 'components/ButtonMain';
import Row from 'components/Layouts/Row';
import TicketCardSmall from 'components/Tickets/TicketCardSmall';
import TicketsTopNavBar from 'components/Tickets/TicketsTopNavBar';

import { Wrapper, TitleLabel } from './TicketsPaidContainer';

export default class TicketsNotPaidContainer extends PureComponent {
  render() {
    const tickets = {
      notFinishedOrder: [
        {
          image: {
            size1: 'http://api.internetbilet.com.ua/images/events_header/size1/eh_1507037239_59d3903753243.jpg',
            size2: 'http://api.internetbilet.com.ua/images/events_header/size2/eh_1507037239_59d3903753243.jpg',
          },
          title: 'Курган и Агрегат',
          place: 'Киев, Кинопанорама',
          time: '12:12',
          date: {
            number: 15,
            month: 'ноября',
          },
          price: '400',
          orderData: {
            number: 12345,
            status: 'not-finished',
            canBeCanceled: false,
          },
        },
        {
          image: {
            size1: 'http://api.internetbilet.com.ua/images/events_header/size1/eh_1507037239_59d3903753243.jpg',
            size2: 'http://api.internetbilet.com.ua/images/events_header/size2/eh_1507037239_59d3903753243.jpg',
          },
          title: 'Курган и Агрегат',
          place: 'Киев, Кинопанорама',
          time: '12:12',
          date: {
            number: 15,
            month: 'ноября',
          },
          price: '400',
          orderData: {
            number: 12345,
            status: 'not-finished',
            canBeCanceled: false,
          },
        },
      ],
      pendingPayment: [
        {
          image: {
            size1: 'http://api.internetbilet.com.ua/images/events_header/size1/eh_1507037239_59d3903753243.jpg',
            size2: 'http://api.internetbilet.com.ua/images/events_header/size2/eh_1507037239_59d3903753243.jpg',
          },
          title: 'Курган и Агрегат',
          place: 'Киев, Кинопанорама',
          time: '12:12',
          date: {
            number: 15,
            month: 'ноября',
          },
          price: '400',
          orderData: {
            number: 12345,
            status: 'pending-payment',
            canBeCanceled: true,
          },
        },
        {
          image: {
            size1: 'http://api.internetbilet.com.ua/images/events_header/size1/eh_1507037239_59d3903753243.jpg',
            size2: 'http://api.internetbilet.com.ua/images/events_header/size2/eh_1507037239_59d3903753243.jpg',
          },
          title: 'Курган и Агрегат',
          place: 'Киев, Кинопанорама',
          time: '12:12',
          date: {
            number: 15,
            month: 'ноября',
          },
          price: '400',
          orderData: {
            number: 12345,
            status: 'pending-payment',
            canBeCanceled: false,
          },
        },
      ],
    };
    return [
      <TicketsTopNavBar type='not-paid' key='header' />,
      <ScrollView key='body'>
        <Wrapper>
          <TitleLabel>Не оформленные</TitleLabel>
          { tickets.notFinishedOrder.map((data) => <TicketCardSmall key={`k${data.orderData.number}`} ticketData={data} />) }
          <View style={{ height: 21 }} />
          <TitleLabel>В ожидании оплаты</TitleLabel>
          { tickets.pendingPayment.map((data) => <TicketCardSmall key={`k${data.orderData.number}`} ticketData={data} />) }
        </Wrapper>
      </ScrollView>,
    ];
  }
}
