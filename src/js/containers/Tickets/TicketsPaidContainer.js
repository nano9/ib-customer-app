import React, { PureComponent } from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import styled from 'styled-components/native';

import ViewWithShadow from 'components/ViewWithShadow';
import ButtonMain from 'components/ButtonMain';
import Row from 'components/Layouts/Row';
import TicketCardSmall from 'components/Tickets/TicketCardSmall';
import TicketsTopNavBar from 'components/Tickets/TicketsTopNavBar';

export const Wrapper = styled.View`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: stretch;
  padding-left: 1px;
  padding-right: 1px;
  padding-top: 21px;
  padding-bottom: 40px;
`;

export const TitleLabel = styled.Text`
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  text-align: center;
  color: #767676;
  margin-bottom: 10px;
`;

export default class TicketsPaidContainer extends PureComponent {
  render() {
    const tickets = [
      {
        image: {
          size1: 'http://api.internetbilet.com.ua/images/events_header/size1/eh_1507037239_59d3903753243.jpg',
          size2: 'http://api.internetbilet.com.ua/images/events_header/size2/eh_1507037239_59d3903753243.jpg',
        },
        title: 'Курган и Агрегат',
        place: 'Киев, Кинопанорама',
        time: '12:12',
        date: {
          number: 15,
          month: 'ноября',
        },
        price: '400',
        orderData: {
          number: 12345,
          status: 'finished',
          canBeCanceled: false,
        },
      },
      {
        image: {
          size1: 'http://api.internetbilet.com.ua/images/events_header/size1/eh_1507037239_59d3903753243.jpg',
          size2: 'http://api.internetbilet.com.ua/images/events_header/size2/eh_1507037239_59d3903753243.jpg',
        },
        title: 'Курган и Агрегат',
        place: 'Киев, Кинопанорама',
        time: '12:12',
        date: {
          number: 15,
          month: 'ноября',
        },
        price: '400',
        orderData: {
          number: 12345,
          status: 'finished',
          canBeCanceled: false,
        },
      },
      {
        image: {
          size1: 'http://api.internetbilet.com.ua/images/events_header/size1/eh_1507037239_59d3903753243.jpg',
          size2: 'http://api.internetbilet.com.ua/images/events_header/size2/eh_1507037239_59d3903753243.jpg',
        },
        title: 'Курган и Агрегат',
        place: 'Киев, Кинопанорама',
        time: '12:12',
        date: {
          number: 15,
          month: 'ноября',
        },
        price: '400',
        orderData: {
          number: 12345,
          status: 'finished',
          canBeCanceled: false,
        },
      },
      {
        image: {
          size1: 'http://api.internetbilet.com.ua/images/events_header/size1/eh_1507037239_59d3903753243.jpg',
          size2: 'http://api.internetbilet.com.ua/images/events_header/size2/eh_1507037239_59d3903753243.jpg',
        },
        title: 'Курган и Агрегат',
        place: 'Киев, Кинопанорама',
        time: '12:12',
        date: {
          number: 15,
          month: 'ноября',
        },
        price: '400',
        orderData: {
          number: 12345,
          status: 'finished',
          canBeCanceled: false,
        },
      },
    ];
    return [
      <TicketsTopNavBar type='paid' key='header' />,
      <ScrollView key='body'>
        <Wrapper>
          <TitleLabel>Купленные билеты</TitleLabel>
          { tickets.map((data) => <TicketCardSmall key={`k${data.orderData.number}`} ticketData={data} />) }
        </Wrapper>
      </ScrollView>,
    ];
  }
}
