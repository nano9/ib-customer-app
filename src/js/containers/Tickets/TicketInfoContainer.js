import React, { PureComponent } from 'react';
import { View, ScrollView, Image } from 'react-native';
import styled from 'styled-components/native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import Carousel from 'react-native-snap-carousel';

import { screenDimensions } from 'constants';
import TopNavBar from 'components/TopNavBar';
import TicketCardBig from 'components/Tickets/TicketCardBig';

import bgImg from 'img/ticket-info-bg.png';

const BgImage = styled(Image)`
  position: absolute;
  left: 0px;
  right: 0px;
  top: 0px;
  bottom: 0px;
  width: 100%;
  height: 100%;
`;

const TicketScrolledWrapper = styled.ScrollView`
  height: 100%;
  padding-top: 20px;
  padding-bottom: 40px;
`;

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    goBack,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class TicketInfoContainer extends PureComponent {
  renderTicketCard = (data, index) => (
    <TicketScrolledWrapper key={`k${index}`}>
      <TicketCardBig ticketData={data} />
    </TicketScrolledWrapper>
  );

  render() {
    const { WND_WIDTH, WND_HEIGHT } = screenDimensions.get();
    const tickets = [
      {
        codeData: {
          isBarCode: true,
          img: '',
          number: 1234,
        },
      },
      {
        codeData: {
          isBarCode: true,
          img: '',
          number: 1234,
        },
      },
      {
        codeData: {
          isBarCode: true,
          img: '',
          number: 1234,
        },
      },
    ];
    return [
        <BgImage source={bgImg} key='bg' />,
        <TopNavBar goBack={this.props.goBack} label="Цирк 'Super men на арені'" key="header" />,
        <Carousel
          layout={'default'}
          ref={(c) => { this._carousel = c; }}
          data={tickets}
          renderItem={this.renderTicketCard}
          sliderWidth={WND_WIDTH}
          itemWidth={320}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
          key='hor-scroll'
        />
    ];
  }
}
