import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import styled from 'styled-components/native';

import NavBar from 'components/NavBar';
import SideBarContainer from 'containers/SideBarContainer';

const Footer = styled.View`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 75px;
`;

export default class NavContainer extends PureComponent {
  static propTypes = {
    style: View.propTypes && View.propTypes.style,
    setGlobalClickListener: PropTypes.func.isRequired,
  };
  static defaultProps = {
    style: undefined,
  };

  state = {
    sideBarOpen: false,
  };

  setSideBarState = (to) => {
    this.setState({ sideBarOpen: to });
  };

  render() {
    const { sideBarOpen } = this.state;
    return (
      <View style={this.props.style}>
        <SideBarContainer
          open={sideBarOpen}
          setGlobalClickListener={this.props.setGlobalClickListener}
          setSideBarState={this.setSideBarState}
        />
        <Footer>
          <NavBar
            setSideBarState={this.setSideBarState}
            isSidebarOpen={sideBarOpen}
            ticketsNotices={2}
            favoritesNotices={1}
            accountNotices={3}
          />
        </Footer>
      </View>
    );
  }
}
