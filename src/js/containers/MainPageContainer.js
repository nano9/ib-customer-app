import React, { PureComponent } from 'react';
import { FlatList, View, RefreshControl, Dimensions } from 'react-native';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components/native';

import { requestEvents, requestTours, requestPosters } from 'actions/api/mainPage';

import { getComponentFromNativeID } from 'js/utils';

import EventCard, { EventCardModal } from 'components/EventCard';
import {
  screenDimensions,
  EVENT_CARD_BOTTOM_HEIGHT,
  EVENT_CARD_MARGIN_BOTTOM,
} from 'constants';
import TourCard from 'components/TourCard';
import BannerSlider from 'components/BannerSlider';
import MainPageTabs from 'components/MainPageTabs';
import LoadingSpinner from 'components/LoadingSpinner';

function mapStateToProps(state) {
  const prepareArray = (indexes, data) => Array.from(indexes, id => data.byID[id]);

  return {
    posters: state.uiData.posters,
    allEvents: prepareArray(state.events.allEvents, state.events),
    topEvents: prepareArray(state.events.topEvents, state.events),
    tours: state.tours.allTours.map(tourID => ({
      ...state.tours.byID[tourID],
      events: state.tours.byID[tourID].events.map(eventID => state.events.byID[eventID]),
      key: tourID,
    })),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    requestEvents,
    requestTours,
    requestPosters,
  }, dispatch);
}

const Container = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
  z-index: 898;
  elevation: 898;
  padding-bottom: 26px;
  background: #FFF;
  height: 500px;
`;

const StyledList = styled(({ style, ...otherProps }) => <FlatList contentContainerStyle={style} {...otherProps} />)`
  padding-bottom: 20px;
  background-color: #FFF;
`;

@connect(mapStateToProps, mapDispatchToProps)
export default class MainPageContainer extends PureComponent {
  static propTypes = {
    allEvents: PropTypes.arrayOf(PropTypes.any).isRequired,
    topEvents: PropTypes.arrayOf(PropTypes.any).isRequired,
    tours: PropTypes.arrayOf(PropTypes.any).isRequired,
    posters: PropTypes.arrayOf(PropTypes.any).isRequired,
  };

  state = {
    page: 'topEvents',
    activePage: 'loading',
    isLoading: true,
    eventBigCardData: {
      isVisible: false,
    },
    activeSlide: 0,
  };

  componentWillMount() {
    this.props.requestPosters();
    this.setPage(this.state.page);
  }

  setPage = (page, refresh = false) => {
    if (!refresh && this.getData(page).length > 0) {
      this.setState({ page, activePage: page, isLoading: false });
      return;
    }

    let request;
    switch (page) {
      default:
        throw new Error(`Unknown page ${page}`);
      case 'allEvents':
        request = this.props.requestEvents({ mode: 'all', refresh: true });
        break;
      case 'topEvents':
        request = this.props.requestEvents({ mode: 'top', refresh: true });
        break;
      case 'tours':
        request = this.props.requestTours();
        break;
    }
    if (request) {
      this.setState({ isLoading: true, page, activePage: refresh ? 'refresh' : 'loading' });
      request.then(() => this.setState({ activePage: page, isLoading: false }));
    }
  };

  onEventPress = (event) => {
    let nativeNode = getComponentFromNativeID(event.nativeEvent.target);
    while (nativeNode !== null && nativeNode.type !== EventCard) {
      nativeNode = nativeNode.return;
    }
    if (!nativeNode) return;

    const card = nativeNode.stateNode;
    while (nativeNode.type !== View) {
      nativeNode = nativeNode.child;
    }

    const { eventData } = card.props;
    const { height, width } = Dimensions.get('window');

    nativeNode.stateNode.measureInWindow((x, y, w, h) => {
      const p = width / 3;
      let align;
      if (x < p) {
        align = 'left';
      } else if (x < p * 2) {
        align = 'center';
      } else {
        align = 'right';
      }
      this.setState({
        hideEventCardId: eventData.id,
        eventBigCardData: {
          isVisible: true,
          data: eventData,
          align,
          x,
          y,
        },
      });
    });
  };

  getData(page = undefined) {
    switch (page || this.state.activePage) {
      case 'allEvents': return this.props.allEvents;
      case 'topEvents': return this.props.topEvents;
      case 'tours': return this.props.tours;
      default: return [{ id: 'loading' }];
    }
  }

  getItemLayout = (data, index) => {
    const { EVENT_CARD_IMG_HEIGHT } = screenDimensions.get();
    const eventCardHeight =
      EVENT_CARD_IMG_HEIGHT + EVENT_CARD_BOTTOM_HEIGHT + EVENT_CARD_MARGIN_BOTTOM;
    return {
      length: eventCardHeight,
      offset: eventCardHeight * index,
      index,
    };
  };

  getToursLayout = (data, index) => ({
    length: 200,
    offset: 200 * index,
    index,
  });

  loadMoreData = () => {
    let request;
    switch (this.state.activePage) {
      case 'allEvents':
        request = this.props.requestEvents({ mode: 'all', refresh: false });
        break;
      case 'topEvents':
        request = this.props.requestEvents({ mode: 'top', refresh: false });
        break;
      default:
        return;
    }
    this.setState({ isLoading: true });
    request.then(() => this.setState({ isLoading: false }));
  };

  keyExtractor = it => it.id;

  // Kludge: We shouldn't access state and make update depending on it.
  // But there is no real other way to do this, only by changing how setPage works
  refresh = () => {
    this.props.requestPosters();
    if (this.state.activePage === this.state.page) {
      this.setPage(this.state.page, true);
    }
  };

  closeEventBigCardModal = () => {
    this.setState({
      eventBigCardData: {
        isVisible: false,
      },
    });
  };

  renderItem = ({ item }) => {
    switch (this.state.activePage) {
      case 'allEvents': return this.renderEventRow(item);
      case 'topEvents': return this.renderEventRow(item);
      case 'tours': return this.renderTourRow(item);
      default: return false;
    }
  };
  renderLoadingSpinner = () => this.state.isLoading && <LoadingSpinner marginBottom={50} />;
  renderEventRow = (it) => {
    const visible = this.state.hideEventCardId !== it.id;
    return <EventCard eventData={it} onPress={this.onEventPress} visible={visible} />;
  };
  renderTourRow = it => <TourCard tour={it} />;

  setSlide = index => this.setState({ activeSlide: index });

  renderPageHeader = () => (
    <Container>
      <BannerSlider
        key="slider"
        onSlideChange={this.setSlide}
        activeSlide={this.state.activeSlide}
        posters={this.props.posters}
      />
      <MainPageTabs
        key="tabs"
        selectedTabId={this.state.page}
        onSelectTab={this.setPage}
      />
    </Container>
  );

  renderEventBigCard = () => {
    const {
      isVisible,
      data, align,
      x, y,
    } = this.state.eventBigCardData;
    const {
      name,
      image,
      city,
      priceRangeBounds,
      date,
      freeTicketsAmount,
    } = data;
    return (
      <EventCardModal
        visible={isVisible}
        align={align}
        x={x}
        y={y}
        title={name}
        imgUri={image.size2}
        city={city}
        priceStarts={priceRangeBounds.minPrice}
        date={date}
        freeTicketsAmount={freeTicketsAmount}
        onRequestClose={this.closeEventBigCardModal}
        onShowEventCard={() => {
          this.setState({
            hideEventCardId: -1,
          });
        }}
      />
    );
  };

  render() {
    return (
      <View>
        <StyledList
          key={this.state.page}
          ListHeaderComponent={this.renderPageHeader}
          data={this.getData()}
          renderItem={this.renderItem}
          numColumns={this.state.page === 'tours' ? 1 : 3}
          keyExtractor={this.keyExtractor}
          initialNumToRender={this.state.page === 'tours' ? 3 : 9}
          getItemLayout={this.state.page === 'tours' ? this.getToursLayout : this.getItemLayout}
          onEndReached={this.loadMoreData}
          onEndReachedThreshold={0.1}
          ListFooterComponent={this.renderLoadingSpinner}
          refreshControl={
            <RefreshControl
              refreshing={this.state.activePage === 'refresh'}
              onRefresh={this.refresh}
            />
          }
          removeClippedSubviews
        />
        {this.state.eventBigCardData.isVisible && this.renderEventBigCard()}
      </View>
    );
  }
}
