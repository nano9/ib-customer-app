import React, { PureComponent } from 'react';
import { ScrollView, Animated } from 'react-native';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components/native';

import { requestHallData } from 'actions/api/hallPage';

import PageLoading from 'components/PageLoading';
import TopNavBar from 'components/TopNavBar';
import Header from 'components/HallPage/Header';
import Events from 'components/HallPage/Events';

const ScrollContainer = styled(ScrollView)`
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
`;

function mapStateToProps(state) {
  return {
    data: state.hall.data,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    requestHallData,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
export default class HallPageContainer extends PureComponent {
  state = {
    topNavBarOffsetY: new Animated.Value(-100),
  };

  componentWillMount() {
    this.props.requestHallData();
  }

  isShowTopNavBar = false;

  handleScroll = (event) => {
    if (event.nativeEvent.contentOffset.y > 100 && !this.isShowTopNavBar) {
      this.toggleNavBarState(true);
      this.isShowTopNavBar = true;
    }
    if (event.nativeEvent.contentOffset.y < 100 && this.isShowTopNavBar) {
      this.toggleNavBarState(false);
      this.isShowTopNavBar = false;
    }
  }

  toggleNavBarState = (isShow) => {
    const { topNavBarOffsetY } = this.state;
    topNavBarOffsetY.setValue(isShow ? -100 : 0);

    Animated.timing(
      topNavBarOffsetY,
      {
        toValue: isShow ? 0 : -100,
        duration: 400,
        useNativeDriver: true,
      },
    ).start();
  }

  render() {
    // TODO: fix go back button
    const goBackFunc = () => console.log('going back');
    if (!this.props.data) {
      return <PageLoading />;
    }

    const {
      name: title, address,
      image,
      descrMd: description,
      events,
    } = this.props.data;

    return [
      <ScrollContainer key="body" onScroll={this.handleScroll}>
        <Header
          goBack={goBackFunc}
          title={title}
          image={image}
          address={address}
          description={description}
        />
        <Events events={events} />
      </ScrollContainer>,
      <Animated.View
        key="header"
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          transform: [
            { translateY: this.state.topNavBarOffsetY },
          ],
        }}
      >
        <TopNavBar goBack={goBackFunc} label={title} />
      </Animated.View>,
    ];
  }
}

HallPageContainer.propTypes = {
  requestHallData: PropTypes.func,
  data: PropTypes.shape({
    image: PropTypes.string,
    name: PropTypes.string,
    descrMd: PropTypes.string,
    address: PropTypes.string,
    events: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      date: PropTypes.string,
      freeTicketsAmount: PropTypes.number,
      hallName: PropTypes.string,
      hasScheme: PropTypes.bool,
      city: PropTypes.string,
      image: PropTypes.shape({
        size1: PropTypes.string,
        size2: PropTypes.strings,
      }),
      priceRangeBounds: PropTypes.shape({
        hasPrices: PropTypes.bool,
        hasMultiPrices: PropTypes.bool,
        minPrice: PropTypes.string,
        fromNotice: PropTypes.string,
      }),
      url: PropTypes.string,
    })),
  }),
};

HallPageContainer.defaultProps = {
  requestHallData: undefined,
  data: null,
};
