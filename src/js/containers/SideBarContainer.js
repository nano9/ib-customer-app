import React, { Component } from 'react';
import { Animated } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import { getComponentFromNativeID } from 'js/utils';

import SideBar from 'components/SideBar/SideBar';
import { SidebarToggle } from 'components/NavBar/NavBar';

const SideBarWrapper = styled(Animated.View)`
  position: absolute;
  width: 250px;
  height: 100%;
  background-color: #151E26;
`;

function mapStateToProps(state) {
  return {
    city: state.cities.byID[state.profile.settings.personal.city],
    allCities: Object.values(state.cities.byID),
    phones: state.uiData.phones,
  };
}

@connect(mapStateToProps)
export default class SideBarContainer extends Component {
  static OPEN_X = 0;
  static CLOSED_X = -250;

  constructor(props) {
    super(props);
    this.state = {
      xOffset: new Animated.Value(props.open ? SideBarContainer.OPEN_X : SideBarContainer.CLOSED_X),
    };
  }

  componentWillReceiveProps({ open, setGlobalClickListener }) {
    this.setState((prevState) => {
      if (prevState.isOpen === open) return prevState;

      this.updateListener(setGlobalClickListener, open);

      Animated.timing(
        this.state.xOffset,
        {
          toValue: open ? SideBarContainer.OPEN_X : SideBarContainer.CLOSED_X,
          duration: 500,
        },
      ).start();
      return {
        isOpen: open,
      };
    });
  }

  updateListener(setGlobalClickListener, open) {
    if (open) {
      setGlobalClickListener(this.clickListener);
    } else {
      setGlobalClickListener(undefined);
    }
  }

  clickListener = ({ nativeEvent: { target } }) => {
    let nativeNode = getComponentFromNativeID(target);
    while (
      nativeNode !== null &&
      nativeNode.stateNode !== this &&
      nativeNode.type !== SidebarToggle
    ) {
      nativeNode = nativeNode.return;
    }
    if (nativeNode === null) this.props.setSideBarState(false);
  };

  render() {
    const { city, allCities } = this.props;
    return (
      <SideBarWrapper
        style={{
          right: this.state.xOffset,
        }}
      >
        <SideBar city={city} allCities={allCities} />
      </SideBarWrapper>
    );
  }
}

const cityShape = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
};

SideBarContainer.propTypes = {
  city: PropTypes.shape(cityShape),
  allCities: PropTypes.arrayOf(PropTypes.shape(cityShape)),
  open: PropTypes.bool,
  setSideBarState: PropTypes.func.isRequired,
};

SideBarContainer.defaultProps = {
  city: undefined,
  allCities: undefined,
  open: false,
};
