import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';

import styled from 'styled-components/native';

import { Provider } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-native';
import { ConnectedRouter } from 'react-router-redux';

import store, { history } from 'store';

import BuyTicketsRouter from 'routers/BuyTicketsRouter';
import PersonalCabinetRouter from 'routers/PersonalCabinetRouter';
import TicketsRouter from 'routers/TicketsRouter';

import SplashScreenContainer from 'containers/SplashScreenContainer';
import MainPageContainer from 'containers/MainPageContainer';
import NavContainer from 'containers/NavContainer';
// import TicketsInfoContainer from 'containers/TicketsInfoContainer';
import AuthContainer from 'containers/Auth/AuthContainer';
import RegisterContainer from 'containers/Auth/RegisterContainer';
import ChangePasswordContainer from 'containers/Auth/ChangePasswordContainer';
import OnlineAdviserContainer from 'containers/OnlineAdviserContainer';
import CashBoxesContainer from 'containers/CashBoxesContainer';
import FAQContainer from 'containers/FAQContainer';
import TermsConditionsContainer from 'containers/TermsConditionsContainer';
import EditPersonalInfoContainer from 'containers/PersonalCabinet/EditPersonalInfoContainer';
import CardListContainer from 'containers/PersonalCabinet/CardListContainer';
import FavoritesContainer from 'containers/FavoritesContainer';

import SearchContainer from 'containers/SearchContainer';
import EventPageContainer from 'containers/EventPageContainer';

import DelayedRoute from 'components/DelayedRoute';

import TicketInfoContainer from 'containers/Tickets/TicketInfoContainer';
import EventBuyTicketsSchemelessContainer from 'containers/EventBuyTickets/EventBuyTicketsSchemelessContainer';
import EventBuyTicketsWithSchemeContainer from 'containers/EventBuyTickets/EventBuyTicketsWithSchemeContainer';
import EventBuyTicketsCartContainer from 'containers/EventBuyTickets/EventBuyTicketsCartContainer';
import HallPageContainer from 'containers/HallPageContainer';
import PaymentPageContainer from 'containers/Payment/PaymentPageContainer';

import FakeLinksContainer from 'containers/FakeLinksContainer';

import { screenDimensions, SCREEN_MODE_LANDSCAPE } from 'constants';

const AppContainerView = styled.View`
  flex: 1;
  align-items: stretch;
  background-color: #FFF;
`;

const AppContent = styled.View`
  position: absolute;
  top: 0;
  bottom: 54px;
  background-color: #FFF;
  width: 100%;
  flex: 1;
  padding-top: 3px;
`;

const StyledNavContainer = styled(NavContainer)`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;

function TestComponent() {
  return <View></View>;
}

const TicketsInfoWrapper = styled.View`
  width: 100%;
  height: 100%;
  z-index: 999;
`;

class LandscapeTicketsInfo extends Component {
  componentWillMount() {
    Dimensions.addEventListener('change', () => this.forceUpdate());
  }

  render() {
    return (
      screenDimensions.get().ORIENTATION === SCREEN_MODE_LANDSCAPE &&
        <TicketsInfoWrapper>
          <TicketInfoContainer />
        </TicketsInfoWrapper>
    );
  }
}

class AppContainer extends Component {
  componentWillMount() {
    Dimensions.addEventListener('change', this.screenResizeHandler);
    const window = Dimensions.get('window');
    screenDimensions.update(window);
  }

  componentWillUnmount() {
    Dimensions.removeEventListener('change', this.screenResizeHandler);
  }

  setGlobalClickListener = (listener) => { this.globalClickListener = listener; };
  globalClickListener = undefined;
  handleTouchCapture = (event) => {
    if (this.globalClickListener) this.globalClickListener(event);
    return false;
  };

  screenResizeHandler = event => screenDimensions.update(event.window);

  render() {
    return (
      <Switch>
        <Route exact path="/auth" component={AuthContainer} />
        <Route exact path="/register" component={RegisterContainer} />
        <Route
          render={() => (
            <AppContainerView onStartShouldSetResponderCapture={this.handleTouchCapture}>
              <AppContent>
                <Switch>
                  <DelayedRoute path="/main" component={MainPageContainer} />
                  <DelayedRoute path="/pc/favorites" component={FavoritesContainer} />
                  <DelayedRoute path="/pc/tickets" component={TicketsRouter} />
                  <DelayedRoute path="/search" component={SearchContainer} />

                  <DelayedRoute path="/onlineAdviser" component={OnlineAdviserContainer} />
                  <DelayedRoute path="/cashboxes" component={CashBoxesContainer} />

                  <Route path="/pc" component={PersonalCabinetRouter} />

                  <Route path="/faq" component={FAQContainer} />
                  <Route path="/terms" component={TermsConditionsContainer} />

                  <Route path="/event/:eventID" component={BuyTicketsRouter} />
                  <Redirect from="/" to="/main" />
                </Switch>
                <LandscapeTicketsInfo />
              </AppContent>
              <StyledNavContainer setGlobalClickListener={this.setGlobalClickListener} />
            </AppContainerView>
          )}
        />
      </Switch>
    );
  }
}

const TestWrapper = styled.View`
  width: 100px;
  height: 100px;
  border: 1px solid blue;
`;

export default function App() {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Switch>
          <Route exact path="/" component={SplashScreenContainer} />
          <Route component={AppContainer} />
        </Switch>
      </ConnectedRouter>
    </Provider>
  );
}
